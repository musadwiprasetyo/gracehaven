-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.38-MariaDB - Source distribution
-- Server OS:                    Linux
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for gracehaven
CREATE DATABASE IF NOT EXISTS `trindrdb_grace` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `trindrdb_grace`;

-- Dumping structure for table gracehaven.attendance_resident
CREATE TABLE IF NOT EXISTS `attendance_resident` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_class_staff` int(11) DEFAULT NULL,
  `id_scs` int(11) DEFAULT NULL COMMENT 'relation with schedule_class_staff',
  `id_resident` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `status` enum('present','not present') DEFAULT 'present',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.attendance_resident: 2 rows
/*!40000 ALTER TABLE `attendance_resident` DISABLE KEYS */;
INSERT INTO `attendance_resident` (`id`, `id_class_staff`, `id_scs`, `id_resident`, `date`, `time`, `status`) VALUES
	(2, 1, 1, 1, '2019-04-08', '16:24:05', 'present'),
	(3, 1, 1, 2, '2019-04-08', '16:31:11', 'present');
/*!40000 ALTER TABLE `attendance_resident` ENABLE KEYS */;

-- Dumping structure for table gracehaven.category_routine
CREATE TABLE IF NOT EXISTS `category_routine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.category_routine: 5 rows
/*!40000 ALTER TABLE `category_routine` DISABLE KEYS */;
INSERT INTO `category_routine` (`id`, `name`) VALUES
	(1, 'Good Morning!'),
	(2, 'Study time!'),
	(3, 'Good afternoon!'),
	(4, 'Evening!'),
	(5, 'Good night!');
/*!40000 ALTER TABLE `category_routine` ENABLE KEYS */;

-- Dumping structure for table gracehaven.class
CREATE TABLE IF NOT EXISTS `class` (
  `class_id` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`class_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.class: 4 rows
/*!40000 ALTER TABLE `class` DISABLE KEYS */;
INSERT INTO `class` (`class_id`, `class_name`) VALUES
	(1, 'Bless class'),
	(2, 'Holy Blessing'),
	(3, 'Miracle'),
	(4, 'new class');
/*!40000 ALTER TABLE `class` ENABLE KEYS */;

-- Dumping structure for table gracehaven.class_resident
CREATE TABLE IF NOT EXISTS `class_resident` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_class_staff` int(11) DEFAULT NULL,
  `id_resident` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.class_resident: 3 rows
/*!40000 ALTER TABLE `class_resident` DISABLE KEYS */;
INSERT INTO `class_resident` (`id`, `id_class_staff`, `id_resident`) VALUES
	(1, 1, 1),
	(2, 1, 2),
	(3, 2, 1);
/*!40000 ALTER TABLE `class_resident` ENABLE KEYS */;

-- Dumping structure for table gracehaven.class_staff
CREATE TABLE IF NOT EXISTS `class_staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_class` int(11) DEFAULT NULL,
  `id_staff` int(11) DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `clock` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '1' COMMENT '1:Active;0;Disable',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.class_staff: 5 rows
/*!40000 ALTER TABLE `class_staff` DISABLE KEYS */;
INSERT INTO `class_staff` (`id`, `id_class`, `id_staff`, `day`, `clock`, `status`) VALUES
	(1, 1, 1, 4, '12.00-14.00', 1),
	(2, 2, 1, 2, '11.00-12.00', 1),
	(5, 1, 1, 4, '10:00-12:00', 1),
	(4, 1, 1, 4, '10:00-11:00', 1),
	(6, 2, 2, 7, '12:00', 1);
/*!40000 ALTER TABLE `class_staff` ENABLE KEYS */;

-- Dumping structure for table gracehaven.day
CREATE TABLE IF NOT EXISTS `day` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.day: 7 rows
/*!40000 ALTER TABLE `day` DISABLE KEYS */;
INSERT INTO `day` (`id`, `day_name`) VALUES
	(1, 'Monday'),
	(2, 'Tuesday'),
	(3, 'Wednesday'),
	(4, 'Thursday'),
	(5, 'Friday'),
	(6, 'Saturday'),
	(7, 'Sunday');
/*!40000 ALTER TABLE `day` ENABLE KEYS */;

-- Dumping structure for table gracehaven.group
CREATE TABLE IF NOT EXISTS `group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.group: 3 rows
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
INSERT INTO `group` (`group_id`, `group_name`) VALUES
	(1, 'Admin'),
	(2, 'Staff'),
	(3, 'Resident');
/*!40000 ALTER TABLE `group` ENABLE KEYS */;

-- Dumping structure for table gracehaven.resident
CREATE TABLE IF NOT EXISTS `resident` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `resident_id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.resident: 3 rows
/*!40000 ALTER TABLE `resident` DISABLE KEYS */;
INSERT INTO `resident` (`id`, `resident_id`, `name`) VALUES
	(1, 1, 'Arga'),
	(2, 2, 'Aris'),
	(3, 3, 'Andre');
/*!40000 ALTER TABLE `resident` ENABLE KEYS */;

-- Dumping structure for table gracehaven.routine
CREATE TABLE IF NOT EXISTS `routine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_routine_id` int(11) NOT NULL,
  `quantities` int(11) DEFAULT NULL,
  `expectations` varchar(100) DEFAULT NULL,
  `routine_name` text,
  `default_star` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.routine: 13 rows
/*!40000 ALTER TABLE `routine` DISABLE KEYS */;
INSERT INTO `routine` (`id`, `category_routine_id`, `quantities`, `expectations`, `routine_name`, `default_star`) VALUES
	(1, 1, NULL, NULL, 'a. Wake up on time by yourself ', 5),
	(2, 1, NULL, NULL, 'b. Greetings "Good Morning/Bye Aunty/Uncle" ', 5),
	(3, 1, NULL, NULL, 'c. Made my bed (fold blanket)', 5),
	(4, 2, NULL, NULL, 'a. Attend school/activities with no complaints received OR Homework/ Revise time >60 mins (non-schoolers)', 5),
	(5, 3, NULL, NULL, 'a. Come back on time', 5),
	(6, 3, NULL, NULL, 'b. Greetings "Good Afternoon Aunty/Uncle"   ', 5),
	(7, 3, NULL, NULL, 'c. Sign Blue Book, Surrender phone  ', 5),
	(8, 4, NULL, NULL, 'a. Complete my duties properly', 5),
	(9, 4, NULL, NULL, 'b. Personal hygience (shower)', 5),
	(10, 4, NULL, NULL, 'c. Surrender phone or exit privilege room on time  ', 5),
	(11, 5, NULL, NULL, 'a. Prepare for school (uniform)', 5),
	(12, 5, NULL, NULL, 'b. Be in room/bed by 9PM', 5),
	(13, 5, NULL, NULL, 'c. Greetings "Good Night Aunty/Uncle"', 5);
/*!40000 ALTER TABLE `routine` ENABLE KEYS */;

-- Dumping structure for table gracehaven.routine_record
CREATE TABLE IF NOT EXISTS `routine_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `routine_id` int(11) DEFAULT NULL,
  `resident_id` int(11) DEFAULT NULL,
  `day` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `description` text,
  `star` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.routine_record: 0 rows
/*!40000 ALTER TABLE `routine_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `routine_record` ENABLE KEYS */;

-- Dumping structure for table gracehaven.schedule_class_staff
CREATE TABLE IF NOT EXISTS `schedule_class_staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_class_staff` int(11) DEFAULT NULL,
  `schedule` text,
  `date` date DEFAULT NULL,
  `note` text,
  `status` int(11) DEFAULT '1' COMMENT '1;Active;0:Close',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.schedule_class_staff: 3 rows
/*!40000 ALTER TABLE `schedule_class_staff` DISABLE KEYS */;
INSERT INTO `schedule_class_staff` (`id`, `id_class_staff`, `schedule`, `date`, `note`, `status`) VALUES
	(1, 1, 'Meeting 12', '2019-04-08', 'Note Meeting 1', 1),
	(2, 2, 'Meeting 2', '2019-04-11', 'Note Meeting 2', 1),
	(3, 1, 'Meeting 3', '2019-04-11', 'Note Meeting 3', 1);
/*!40000 ALTER TABLE `schedule_class_staff` ENABLE KEYS */;

-- Dumping structure for table gracehaven.staff
CREATE TABLE IF NOT EXISTS `staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.staff: 2 rows
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
INSERT INTO `staff` (`id`, `name`, `group_id`) VALUES
	(1, 'pras', 1),
	(2, 'naka', 2),
	(3, 'admin', 1);
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;

-- Dumping structure for table gracehaven.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT 'Relation With Staff and Resident',
  `name` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `type` int(11) NOT NULL COMMENT '1:Admin;2:Staff:3:Resident',
  `token` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.users: 5 rows
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `user_id`, `name`, `username`, `password`, `type`, `token`) VALUES
	(1, 1, 'Pras', 'pras', 'pras', 1, NULL),
	(2, 2, 'Naka', 'naka', 'naka', 2, NULL),
	(3, 1, 'Arga', 'arga', 'arga', 3, NULL),
	(4, 2, 'Aris', 'aris', 'aris', 3, NULL),
	(5, 3, 'Andre', 'andre', 'andre', 3, NULL),
	(6, 3, 'Admin', 'admin', 'admin', 1, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

import React, { Component } from 'react';
import { View, Text ,Alert, Button} from 'react-native';

export default class DetailClassScreen extends Component {
   
  componentWillMount() {
        // Alert.alert(JSON.stringify(this.props.navigation.state.params.classid.class_id));
  }
  deleteProcess(classid){
    fetch('http://gracehaven.tri-niche.com/gracenatalie/api/deleteclass', {
           method: 'POST',
           headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
          },
          body: JSON.stringify({
                classid:classid
          })
          }).then((response) => response.json())
            .then((responseJson) => {
              this.setState({
                      loading: false
                  }, ()=>{
              if (responseJson.success=='TRUE') {
                  Alert.alert(responseJson.info);
                  this.props.navigation.navigate('MasterClassScreen')
              } else {
                 this.setState({ spinner: false });
                      setTimeout(() => {
                        Alert.alert('Warning',responseJson.info);
                      }, 100);
              }
              }
                );
          }).catch((error) => {
            console.error(error);
    }); 

  }
  render() {
    const { navigation } = this.props;
    const classid = navigation.getParam('classid');
    // Alert.alert(JSON.stringify(classid));
    return (
      <View style={{backgroundColor: '#eee', flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
        <Text style={{textAlign: 'center'}}>DetailClassScreen {classid.class_name}</Text>
        <Button
          onPress={() => {
            Alert.alert(
              'Are You Sure',
              'Delete This Class?',
              [
                {text: 'OK', onPress: () => this.deleteProcess(classid.class_id)},
                {
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel',
                },
              ],
              {cancelable: false},
            );
          }}
          title="Delete"
        />
      </View>
    );
  }
}

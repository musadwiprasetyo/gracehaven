'use strict';

import React, { Component } from 'react'
import QRCode from 'react-native-qrcode';
import { View, Text, AppRegistry,StyleSheet,TextInput,AsyncStorage} from 'react-native';

export default class QRScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userid:''
    };
    AsyncStorage.getItem('userid', (error, result) => {
        if (result) {
            this.setState({
                userid: result
            });
        }
    });
  }
  render() {
    return (
      <View style={styles.container}>
        <QRCode
          value={this.state.userid}
          size={350}
          bgColor='black'
          fgColor='white'/>
        <Text style={styles.input}>{this.state.userid}</Text>
      </View>
    );
  };
} 
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
 
    input: {
        fontSize:20
    }
});


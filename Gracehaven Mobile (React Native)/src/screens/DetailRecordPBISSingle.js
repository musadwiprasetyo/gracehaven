import React, { Component } from 'react';
import {AppRegistry,Picker
,TouchableHighlight,AsyncStorage,StyleSheet,TextInput,Button,ActivityIndicator, ListView, Text, View, Alert,Image, Platform,TouchableOpacity} from 'react-native';

import { NavigationActions } from 'react-navigation';
import Select from 'react-native-select-plus';
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from "react-native-modal";
import DatePicker from 'react-native-datepicker';
var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

export default class DetailRecordPBISSingle extends Component {
  constructor(props) {
   super(props);
   this.state = {
     isLoading: true,
     dataSource:ds,
     dataSourceSelect:[],
     isModalVisible: false,
     classname:'',
     idclass:'',
     userid:'',
     value:'',
     routineid:'',
     valueResident : '',
     valueClass : '',
     valueDay : '',
     date:'',
     Default_Rating: 1,
     Max_Rating : 5
   }
 }
  componentWillMount() {
      // Alert.alert(JSON.stringify(this.props.navigation.state.params.pbisdata.date));
      this.datapbis(this.props.navigation.state.params.pbisdata.date,this.props.navigation.state.params.pbisdata.resident_id)
  }
  datapbis(date,resident){
        fetch('http://gracehaven.tri-niche.com/gracenatalie/api/detailrecordPBISperResident/'+date+'/'+resident)
        .then((response) => response.json())
        .then((responseJson) => {
        this.setState({
            isLoading: false,
            dataSource: ds.cloneWithRows(responseJson.data),
        }, function() {
            // In this block you can do something with new state.
        });
        })
        .catch((error) => {
        console.error(error);
        });
   }
   ListViewItemSeparator = () => {
   return (
     <View
       style={{
         height: .5,
         width: "100%"
       }}
     />
   );
}

  render() {
    const { navigation } = this.props;
    const pbis = navigation.getParam('pbisdata');
    if (this.state.isLoading) {
     return (
       <View style={{flex: 1,justifyContent:'center'}}>
         <ActivityIndicator />
       </View>
     );
   }
   return (
     <View style={styles.MainContainer}>
        
       <Text style={styles.textTitle} >List Record PBIS Resident</Text>
       <View>
          <View style={{flexDirection:'row'}}>
            <Text style={styles.textResident} >Date</Text>
            <Text style={styles.textResidentDetail} >{pbis.day_name}, {pbis.date}</Text>
          </View>
          <View style={{flexDirection:'row'}}>
            <Text style={styles.textResident} >Resident</Text>
            <Text style={styles.textResidentDetail} >{pbis.name} </Text>
          </View>
        </View>
       <ListView
         dataSource={this.state.dataSource}
         renderSeparator= {this.ListViewItemSeparator}
         renderRow={(rowData) =>
        <View style={{flex:1, flexDirection: 'row',backgroundColor:'skyblue',marginBottom:2}}>
          <View style={{flex:1,width:'100%'}}>
            <Text style={styles.textViewContainer} >
            {rowData.routine_name}
            </Text>
             <Text style={styles.textViewStaff} >
             {rowData.description}
            </Text>
          </View>
          <Text style={styles.textViewContainerRight} >
            {rowData.star}/{rowData.default_star} by {rowData.staffname}
          </Text>
         
        </View>
         }
       />
      <View>
   </View>
     </View>
     
   );
 }
}
const styles = StyleSheet.create({

MainContainer :{

// Setting up View inside content in Vertically center.
justifyContent: 'center',
flex:1,
margin: 5,
paddingTop: (Platform.OS === 'ios') ? 20 : 0,

},

imageViewContainer: {
    width: '70%',
    height: 100 ,
    margin: 10,
    borderRadius : 10
},
textViewContainerRight: {
  textAlign:'right',
  width:'30%', 
  color:'#FF0000'
},
textViewContainer: {
  textAlignVertical:'center',
  width:'100%', 
  color:'#FFFFFF'
},
textViewStaff: {
  width:'100%', 
  color:'#FFFF00',
},
modalContent: {
  height: 400,
  backgroundColor: "#FFFFFF",
  padding: 22,
  justifyContent: "center",
  borderRadius: 4,
  borderColor: "rgba(0, 0, 0, 0.1)"
},
textTitle: {
    textAlignVertical:'center',
    color:'#000000',
    padding:7
},
textResident: {
    textAlignVertical:'center',
    color:'#000000',
    padding:7,
    width:'30%'
},
textResidentDetail: {
    textAlignVertical:'center',
    color:'#000000',
    padding:7,
    width:'70%'
},
TouchableOpacityStyle:{
  position: 'absolute',
  width: 50,
  height: 50,
  alignSelf:'flex-end',
  alignItems: 'center',
  justifyContent: 'center',
  right: 10,
  bottom: 20,
},
buttonContainer: {
  flex: 1,
},
FloatStyle:{
  position: 'absolute',
  width: 50,
  height: 50,
  alignSelf:'flex-end',
  alignItems: 'center',
  justifyContent: 'center',
  right: 30,
  bottom: 10,
},
FloatingButtonStyle: {
  resizeMode: 'contain',
  width: 50,
  height: 50,
},
container: {
  paddingTop: 10
},
input: {
  margin: 15,
  height: 40,
  borderColor: '#7a42f4',
  borderWidth: 1
},
textheader:{
  textAlign:'left',
  color:'#FFFFFF',
  fontSize:14
},
textinput:{
  textAlign:'left'
},
submitButton: {
  backgroundColor: '#7a42f4',
  padding: 10,
  margin: 15,
  height: 20,
},
submitBtnTxts:{
  color: 'white'
},
inputBox: {
  backgroundColor:'rgba(255, 255,255,0.2)',
  fontSize:16,
  color:'#000000',
},
button: {
  backgroundColor:'#0077c2',
    marginVertical: 2,
    paddingVertical: 2
},
buttonCancel: {
  backgroundColor:'#9e9e9e',
    marginVertical: 2,
    paddingVertical: 2
},
btnTxts: {
  fontSize:14,
  fontWeight:'500',
  color:'#ffffff',
  textAlign:'center'
},
 childView:{
        justifyContent: 'center',
        flexDirection: 'row',
},
StarImage:{
    width: 40,
    height: 40,
    resizeMode: 'cover'
},
textStyle:{
    textAlign: 'center',
    fontSize: 23,
    color: '#000',
    marginTop: 15
}
});

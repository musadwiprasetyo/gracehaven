import React, { Component } from 'react';
import { View,ActivityIndicator,TouchableOpacity,Text,StyleSheet,Platform,Alert,ListView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
export default class DetailScheduleScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
          isLoading: true,
          dataSource:ds,
          isModalVisible: false,
        }
    }
    componentWillMount() {
        this.dataattendance(this.props.navigation.state.params.schedule.id)
    }
    dataattendance(id){
        fetch('http://gracehaven.tri-niche.com/gracenatalie/api/dataattendance/'+id)
        .then((response) => response.json())
        .then((responseJson) => {
        // let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.setState({
            isLoading: false,
            dataSource: ds.cloneWithRows(responseJson.attendance),
        }, function() {
            // In this block you can do something with new state.
        });
        })
        .catch((error) => {
        console.error(error);
        });
   }
   ListViewItemSeparator = () => {
    return (
      <View
        style={{
          height: .3,
          width: "100%"
        }}
      />
    );
 } 
 Attendance(dataschedule){
        this.props.navigation.navigate('AttendanceScreenDetail', {
         dataschedule: dataschedule
       });
}
listData()
{
  return (
    <ListView
     dataSource={this.state.dataSource}
     renderSeparator= {this.ListViewItemSeparator}
     renderRow={(rowData,sectionID,rowID)=>
      
        <View style={{flex:1, flexDirection: 'row',backgroundColor:'skyblue',marginBottom:2}}>
        <Text style={styles.textViewContainer} >{rowData.name}</Text>
        <Text style={styles.textViewContainer2} >{rowData.date}-{rowData.time}</Text>
        </View> 
       
        
     }/>
  );
}
render() {
    const { navigation } = this.props;
    const schedule = navigation.getParam('schedule');
    const listViewProportion = this.state.dataSource.getRowCount() == 0 ? 0 : 1
    if (this.state.isLoading) {
     return (
       <View style={{flex: 1,justifyContent:'center'}}>
         <ActivityIndicator />
       </View>
     );
   }else{
    return (
    <View style={styles.MainContainer}>
        <View>
        <Text style={styles.textResident} >Class Schedule Detail</Text>
        </View>
        <View>
          <View style={{flexDirection:'row'}}>
            <Text style={styles.textHeader} >Schedule</Text>
            <Text style={styles.textTitle} >{schedule.schedule}</Text>
          </View>
          <View style={{flexDirection:'row'}}>
            <Text style={styles.textHeader} >Date</Text>
            <Text style={styles.textTitle} >{schedule.date} </Text>
          </View>
          <View style={{flexDirection:'row'}}>
            <Text style={styles.textHeader} >Note</Text>
            <Text style={styles.textTitle} >{schedule.note} </Text>
          </View>
        </View>
        <Text style={styles.textResident} >List Resident Attendance</Text>
        
         {listViewProportion > 0
                ?
                this.listData()
                :
                <Text style={styles.nodata}>No Data Attendance</Text>
          }
       <TouchableOpacity style={styles.TouchableOpacityStyle} >
          <Icon
            onPress={this.Attendance.bind(this, schedule)}
            name="qrcode"
            size={40}
            color="#FF0000"
          />
        </TouchableOpacity>
    </View>

    );
  }
  }
}
const styles = StyleSheet.create({
    MainContainer :{
    flex:1,
    marginTop: 5,
    paddingTop: (Platform.OS === 'ios') ? 20 : 0,
    },
    textViewContainer: {
      textAlignVertical:'center',
      width:'50%', 
      color:'#000000',
      padding:7,
      color:'blue'
    },
    textViewContainer2: {
        width:'50%', 
        color:'#000000',
        padding:7,
        textAlign:'right'
      },
      textViewContainer3: {
        width:'50%', 
        color:'#000000',
        padding:7,
        textAlign:'right'
      },
    modalContent: {
      height: 200,
      backgroundColor: "#FFFFFF",
      padding: 22,
      justifyContent: "center",
      borderRadius: 4,
      borderColor: "rgba(0, 0, 0, 0.1)"
    },
    btnmytext: {
      fontSize:12,
      fontWeight:'400',
      color:'#ffffff',
      textAlign:'center'
    },
    textHeader: {
        textAlignVertical:'center',
        color:'#000000',
        padding:7,
        width:'30%'
    },
    textTitle: {
        textAlignVertical:'center',
        color:'#000000',
        padding:7,
        width:'70%'
    },
    textResident: {
        textAlignVertical:'center',
        color:'#000000',
        padding:7
    },
    TouchableOpacityStyle:{
      position: 'absolute',
      width: 50,
      height: 50,
      alignSelf:'flex-end',
      alignItems: 'center',
      justifyContent: 'center',
      right: 10,
      bottom: 20,
    },
    buttonContainer: {
      flex: 1,
    },
    FloatStyle:{
      position: 'absolute',
      width: 50,
      height: 50,
      alignSelf:'flex-end',
      alignItems: 'center',
      justifyContent: 'center',
      right: 30,
      bottom: 10,
    },
    FloatingButtonStyle: {
      resizeMode: 'contain',
      width: 50,
      height: 50,
    },
    container: {
      paddingTop: 10
    },
    input: {
      margin: 15,
      height: 40,
      borderColor: '#7a42f4',
      borderWidth: 1
    },
    textinput:{
      textAlign:'left'
    },
    submitButton: {
      backgroundColor: '#7a42f4',
      padding: 10,
      margin: 15,
      height: 20,
    },
    submitButtonText2:{
      color: 'white'
    },
    inputBox: {
      backgroundColor:'rgba(255, 255,255,0.2)',
      fontSize:16,
      color:'#000000',
    },
    button: {
      backgroundColor:'#0077c2',
        marginVertical: 2,
        paddingVertical: 2
    },
    buttonCancel: {
      backgroundColor:'#9e9e9e',
        marginVertical: 2,
        paddingVertical: 2
    },
    btnmytext: {
      fontSize:14,
      fontWeight:'500',
      color:'#ffffff',
      textAlign:'center'
    },
    nodata:{
      marginLeft:8
    }
    });
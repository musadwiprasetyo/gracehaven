import React, { Component } from 'react';
import { View,StyleSheet,Text,AsyncStorage,Alert,TouchableOpacity } from 'react-native';

export default class StaffScreen extends Component<{}> {
   constructor(props) {
        super(props);
        this.state = {
	      token: '',
	      type:'',
	      data:'',
	      userid:'',
	      loading: false
	    };
	    
    }
    componentWillMount(){
        AsyncStorage.getItem('userid').then((value) => {
	       this.setState({
	            userid: value
	     	});
			this.fetchdata()
		});
        
    }

    fetchdata(){
    	fetch('http://gracehaven.tri-niche.com/gracenatalie/api/profilstaff', {
           method: 'POST',
           headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
          },
          body: JSON.stringify({
                userid :this.state.userid
          })
          }).then((response) => response.json())
            .then((responseJson) => {
              	this.setState({
             		data:responseJson.data
          		})
          }).catch((error) => {
            console.error(error);
          });
    }
    logout(userid){
    fetch('http://gracehaven.tri-niche.com/gracenatalie/api/logout', {
     method: 'POST',
     headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    },
    body: JSON.stringify({
          userid   : userid
    })
    }).then((response) => response.json())
      .then((responseJson) => { 
        if (responseJson.success=='TRUE') {
           AsyncStorage.clear()
           Alert.alert('Success',responseJson.info);
       	   this.props.navigation.navigate('SignInStack');
        } else {
          Alert.alert('Warning',responseJson.info);
        }
    }).catch((error) => {
      console.error(error);
    }); 
  }
  render() {
    return (
      <View style={{backgroundColor: '#eee', flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
        <Text style={{textAlign: 'center'}}>Name: {this.state.data.name}</Text>
        <TouchableOpacity onPress={() => {this.logout(this.state.data.id)}}><Text style={styles.LogoutText}> Logout</Text></TouchableOpacity>

      </View>
    );
  }
}
const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#FFFFFF'
  },
  MainContainer :{
 
   flex:1,
   justifyContent: 'center',
   alignItems: 'center',
   backgroundColor : '#fff'
 
 },
 
 LinearGradientStyleCard: {
   width: '100%',
   height: '75%'
 },
 LinearGradientStyle: {
   height: '100%',
   width: '100%'
 },
 
 ChildViewStyle:{
 
   borderWidth: 1, 
   borderColor: '#028b53',
   width: '100%',
   height: 50,
   borderRadius: 10,
 
 },
 TextInputStyleClass:{
  textAlign: 'center',
  height: 50,
  width: '90%'
 },
  containerloading : {
    flex: 1,
    alignItems:'center',
    justifyContent :'center'
  },
  header:{
    height: 100, 
    backgroundColor: '#FFFFFF',
    alignItems:'center'
  },
  HeaderText: {
    color:"#FFFFFF",
    fontSize:20,
    marginTop:10,
    marginLeft: 2,
    marginRight:2,
    textAlign:'center'
  },
  HomeText: {
    color:"#000000",
    fontSize:18,
    marginTop: 1,
    height: 50,
    marginLeft: 2,
    marginRight:2,
    textAlign:'center',
    fontWeight : "bold",
    backgroundColor: '#FFFFFF'
  },
  LogoutText: {
    color:"#000000",
    fontSize:18,
    height: 30,
    marginLeft: 2,
    marginRight:2,
    textAlign:'center',
    fontWeight : "bold",
    backgroundColor: '#FFFFFF'
  },
  list: {
    paddingHorizontal: 10,
    marginTop:20
  },
  listContainer:{
    alignItems:'center'
  },

  card:{
    marginHorizontal:5,
    marginVertical:5,
    flexBasis: '32%',
    borderRadius: 20
  },
  cardHeader: {
    paddingVertical: 17,
    paddingHorizontal: 16,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 1,
    flexDirection: 'row',
    alignItems:"center", 
    justifyContent:"center"
  },
  cardContent: {
    paddingVertical: 12.5,
    paddingHorizontal: 16,
  },
  cardFooter:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 12.5,
    paddingBottom: 25,
    paddingHorizontal: 16,
    borderBottomLeftRadius: 1,
    borderBottomRightRadius: 1,
  },
  cardImage:{
    height: 60,
    width: 60,
    alignSelf:'center',
    borderRadius: 40,
    marginTop:20
  },
  Title:{
    fontSize:12,
    flex:1,
    textAlign:'center',
    color:"#000000",
    fontWeight : "bold"
  },
  icon:{
    height: 40,
    width: 40, 
  }
}); 

import React, { Component } from 'react';
import {AppRegistry,Picker
,TouchableHighlight,StyleSheet,TextInput,Button,ActivityIndicator, ListView, Text, View, Alert,Image, Platform,TouchableOpacity} from 'react-native';
import Modal from "react-native-modal";
import { NavigationActions } from 'react-navigation';
import Select from 'react-native-select-plus';
var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

export default class ScheduleScreen extends Component {
	constructor(props) {
   super(props);
   this.state = {
     isLoading: true,
     dataSource:ds,
     dataSourceSelect:[],
     dataSourceClass:[],
     dataSourceDay:[],
     isModalVisible: false,
     classname:'',
     idclass:'',
     value:'',
     valueStaff : '',
     valueClass : '',
     valueDay : '',
     clock:''
   }
   
 }
 modalOk(classdata){
  this.setState({
     idclass:classdata.class_id,
     classname:classdata.class_name,
     isModalVisible: !this.state.isModalVisible
  });
 }
 _ModalAdd = () =>
 this.setState({  
   idclass:'',
   clock:'',
   isModalVisible: !this.state.isModalVisible });

GetClass(classdata) {
    this.props.navigation.navigate('DetailScheduleClassScreen', {
        class: classdata
    });
}
componentDidMount() {
   this.dataclass();
   this.datamasterclass(); 
   this.datastaff(); 
   this.dataday(); 
 }
 datamasterclass(){
  fetch('http://gracehaven.tri-niche.com/gracenatalie/api/masterclass')
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            isLoading: false,
            dataSourceClass: responseJson.data,
            valueClass:responseJson.default
          }, function() {
            
          });
        })
        .catch((error) => {
          console.error(error);
        });
 }
 dataday(){
  fetch('http://gracehaven.tri-niche.com/gracenatalie/api/masterday')
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            isLoading: false,
            dataSourceDay: responseJson.data,
            valueDay:responseJson.default
          }, function() {
            
          });
        })
        .catch((error) => {
          console.error(error);
        });
 }
 datastaff(){
  fetch('http://gracehaven.tri-niche.com/gracenatalie/api/masterstaff')
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            isLoading: false,
            dataSourceSelect: responseJson.data,
            valueStaff:responseJson.default
          }, function() {
            // In this block you can do something with new state.
          });
        })
        .catch((error) => {
          console.error(error);
        });
 }
 dataclass(){
  fetch('http://gracehaven.tri-niche.com/gracenatalie/api/getclassstaff')
  .then((response) => response.json())
  .then((responseJson) => {
    this.setState({
      isLoading: false,
      dataSource: ds.cloneWithRows(responseJson.class),
    }, function() {
      // In this block you can do something with new state.
    });
  })
  .catch((error) => {
    console.error(error);
  });
 }
 processSubmit(idclass){
  if (this.state.clock=='') {
      Alert.alert(
        'Warning',
        'Please Input Clock');
  }
  else{
    this.setState({loading: true})
        fetch('http://gracehaven.tri-niche.com/gracenatalie/api/processclassstaff', {
           method: 'POST',
           headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
          },
          body: JSON.stringify({
                clock : this.state.clock,
                classstaffid : '',
                staffid : this.state.valueStaff,
                classid : this.state.valueClass,
                dayid : this.state.valueDay,
          })
          }).then((response) => response.json())
            .then((responseJson) => {
              this.setState({
                      loading: false
                  }, ()=>{
              if (responseJson.success=='TRUE') {
                  Alert.alert(responseJson.info);
                  this._ModalAdd(!this.state.isModalVisible);
                  this.dataclass();
              } else {
                 this.setState({ spinner: false });
                      setTimeout(() => {
                        Alert.alert('Warning',responseJson.info);
                      }, 100);
              }
              }
                );
          }).catch((error) => {
            console.error(error);
          }); 
  }
}
ListViewItemSeparator = () => {
   return (
     <View
       style={{
         height: .3,
         width: "100%"
       }}
     />
   );
} 
GetPickerSelectedItemValue=()=>{
 
      Alert.alert(this.state.PickerValueHolder);
 
    }
render() {
   
   if (this.state.isLoading) {
     return (
       <View style={{flex: 1,justifyContent:'center'}}>
         <ActivityIndicator />
       </View>
     );
   }
   return (
     <View style={styles.MainContainer}>
        
       <Text style={styles.textResident} >List Class Schedule</Text>
       <ListView
         dataSource={this.state.dataSource}
         renderSeparator= {this.ListViewItemSeparator}
         renderRow={(rowData) =>
        <View style={{flex:1, flexDirection: 'row',backgroundColor:'skyblue',marginBottom:2}}>
          <View style={{flex:1,width:'100%'}}>
          <Text onPress={this.GetClass.bind(this, rowData)} style={styles.textViewContainer} >
          {rowData.id}-{rowData.class_name}
          </Text>
          <Text onPress={this.GetClass.bind(this, rowData)} style={styles.textViewStaff} >
           {rowData.staffname}
          </Text>
          </View>
          <Text style={styles.textViewContainerRight} >
           {rowData.day_name},{rowData.clock}
          </Text>
        </View>
         }
       />
       <TouchableOpacity onPress={this._ModalAdd} style={styles.TouchableOpacityStyle} >
          <Image source={require('../images/fav.png')} 
          style={styles.FloatingButtonStyle} />
        </TouchableOpacity>
        <View>
    
        <Modal isVisible={this.state.isModalVisible}>
        <View style={styles.modalContent}>
          <Text style={styles.textheader}>Class Schedule</Text>
          <Text style={styles.textinput}>Class</Text>
          <Picker
            selectedValue={this.state.valueClass}
            onValueChange={(classValue, classIndex) => this.setState({valueClass: classValue})} >
            { this.state.dataSourceClass.map((valclass, key)=>(
            <Picker.Item label={valclass.class_name} value={valclass.class_id} key={key} />)
            )}
          </Picker>
          <Text style={styles.textinput}>Staff</Text>
          <Picker
      
            selectedValue={this.state.valueStaff}
            placeholder="Select Type"
            onValueChange={(itemValue, itemIndex) => this.setState({valueStaff: itemValue})} >
            { this.state.dataSourceSelect.map((item, key)=>(
            <Picker.Item label={item.name} value={item.id} key={key} />)
            )}
          </Picker>
          <Text style={styles.textinput}>Day</Text>
          <Picker
            selectedValue={this.state.valueDay}
            onValueChange={(dayValue, dayIndex) => this.setState({valueDay: dayValue})} >
            { this.state.dataSourceDay.map((day, key)=>(
            <Picker.Item label={day.day_name} value={day.id} key={key} />)
            )}
          </Picker>
          <Text style={styles.textinput}>Clock</Text>
           <TextInput style={styles.inputBox} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholder="12.00-14.00"
              placeholderTextColor = "#000000"
              selectionColor="#0077c2"
              borderColor="#000000"
              value={this.state.clock}
              onChangeText={clock => this.setState({clock})}
              />  
           <TouchableOpacity style={styles.button} onPress={() => {this.processSubmit()}}>
             <Text style={styles.btnTxts}>Submit</Text>
           </TouchableOpacity>
           <TouchableOpacity style={styles.buttonCancel} onPress={this._ModalAdd}>
             <Text style={styles.btnTxts}>Cancel</Text>
           </TouchableOpacity>  
        </View>
      </Modal>
   </View>
     </View>
     
   );
 }
}

const styles = StyleSheet.create({

MainContainer :{

// Setting up View inside content in Vertically center.
justifyContent: 'center',
flex:1,
margin: 5,
paddingTop: (Platform.OS === 'ios') ? 20 : 0,

},

imageViewContainer: {
    width: '50%',
    height: 100 ,
    margin: 10,
    borderRadius : 10
},
textViewContainerRight: {
  textAlign:'right',
  width:'50%', 
  color:'#FF0000'
},
textViewContainer: {
  textAlignVertical:'center',
  width:'100%', 
  color:'#FFFFFF'
},
textViewStaff: {
  width:'100%', 
  color:'#FFFF00',
  marginLeft:10
},
modalContent: {
  height: 400,
  backgroundColor: "#FFFFFF",
  padding: 22,
  justifyContent: "center",
  borderRadius: 4,
  borderColor: "rgba(0, 0, 0, 0.1)"
},
btnTxts: {
  fontSize:12,
  fontWeight:'400',
  color:'#ffffff',
  textAlign:'center'
},
textResident: {
    textAlignVertical:'center',
    color:'#000000',
    padding:7
},
TouchableOpacityStyle:{
  position: 'absolute',
  width: 50,
  height: 50,
  alignSelf:'flex-end',
  alignItems: 'center',
  justifyContent: 'center',
  right: 10,
  bottom: 20,
},
buttonContainer: {
  flex: 1,
},
FloatStyle:{
  position: 'absolute',
  width: 50,
  height: 50,
  alignSelf:'flex-end',
  alignItems: 'center',
  justifyContent: 'center',
  right: 30,
  bottom: 10,
},
FloatingButtonStyle: {
  resizeMode: 'contain',
  width: 50,
  height: 50,
},
container: {
  paddingTop: 10
},
input: {
  margin: 15,
  height: 40,
  borderColor: '#7a42f4',
  borderWidth: 1
},
textheader:{
  textAlign:'left',
  color:'#FFFFFF',
  fontSize:14
},
textinput:{
  textAlign:'left'
},
submitButton: {
  backgroundColor: '#7a42f4',
  padding: 10,
  margin: 15,
  height: 20,
},
submitBtnTxts:{
  color: 'white'
},
inputBox: {
  backgroundColor:'rgba(255, 255,255,0.2)',
  fontSize:16,
  color:'#000000',
},
button: {
  backgroundColor:'#0077c2',
    marginVertical: 2,
    paddingVertical: 2
},
buttonCancel: {
  backgroundColor:'#9e9e9e',
    marginVertical: 2,
    paddingVertical: 2
},
btnTxts: {
  fontSize:14,
  fontWeight:'500',
  color:'#ffffff',
  textAlign:'center'
}
});

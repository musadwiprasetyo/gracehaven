'use strict';
import React, { Component } from 'react';
import {StyleSheet,Text,View,StatusBar,TouchableOpacity,Linking,Vibration,Alert,AsyncStorage} from 'react-native';

import QRCodeScanner from 'react-native-qrcode-scanner';

export default class AttendanceScreenDetail extends Component {
  componentWillMount() {
    // Alert.alert(JSON.stringify(this.props.navigation.state.params.classid.id));
  }
  goBack(dataschedule){
    this.props.navigation.navigate('DetailScheduleScreen', {
      dataschedule: dataschedule
    });
  }
  refresh(){
    this.props.navigation.navigate('AttendanceScreenDetail', {
                  dataschedule: this.props.navigation.state.params.dataschedule,
                });
  }
  onSuccess(e) {
    fetch('http://gracehaven.tri-niche.com/gracenatalie/api/attendancetaking', {
           method: 'POST',
           headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
          },
          body: JSON.stringify({
                userid        : e.data,
                idclasstaff   : this.props.navigation.state.params.dataschedule.id_class_staff,
                idscs        : this.props.navigation.state.params.dataschedule.id
          })
          }).then((response) => response.json())
            .then((responseJson) => {
             
              if (responseJson.success=='TRUE') {
                Alert.alert(
                        'Success',
                        responseJson.info
                );
                  this.props.navigation.navigate('DetailScheduleScreen', {
                    classid: this.props.navigation.state.params.dataschedule
                  });
              } else {
                Alert.alert('Warning', responseJson.info);
                this.scanner.reactivate()
              }
          }).catch((error) => {
            console.error(error);
          }); 
    
  }
  
  
  render() {
    const { navigation } = this.props;
    const schedule = navigation.getParam('dataschedule');
    

    // Alert.alert(JSON.stringify(classid));
    return(
      <QRCodeScanner
        onRead={this.onSuccess.bind(this)}
        ref={(node) => { this.scanner = node }}
        topContent={
          <Text style={styles.centerText}>
            <Text style={styles.textBold}>Scan QR Code Resident Here!</Text>
          </Text>
        }
        bottomContent={
          <TouchableOpacity  style={styles.button} onPress={() => {this.goBack(schedule)}}><Text style={styles.forgotButton}> Back</Text></TouchableOpacity>
        }
      /> 
      );
  }
}
const styles = StyleSheet.create({
  container : {
    backgroundColor:'#7494d8',
    flex: 1,
    alignItems:'center',
    justifyContent :'center'
  },
  button: {
    width:80,
    backgroundColor:'#23cfc2',
     borderRadius: 50,
      marginVertical: 4,
      paddingVertical: 13
  },
  btnText: {
    fontSize:16,
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  },
  ForgotHeader : {
    paddingHorizontal:16,
    fontSize:18,
    color:'rgba(255, 255, 255, 0.7)'
  },
  forgotTextCont : {
    flexGrow: 1,
    alignItems:'flex-end',
    justifyContent :'center',
    paddingVertical:16,
    flexDirection:'row'
  },
  forgotText: {
    color:'rgba(255,255,255,0.6)',
    fontSize:16
  },
  forgotButton: {
    color:'#ffffff',
    fontSize:16,
    fontWeight:'500',
    marginHorizontal:16,
  },
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonTouchable: {
    padding: 16,
  }
});


import React, { Component } from 'react';
import { AppRegistry,TouchableHighlight,StyleSheet,TextInput,Button,ActivityIndicator, ListView, Text, View, Alert,Image, Platform,TouchableOpacity} from 'react-native';
import Modal from "react-native-modal";
import { NavigationActions } from 'react-navigation';


export default class MasterClassScreen extends Component {
	constructor(props) {
   super(props);
   this.state = {
     isLoading: true,
     dataSource:[],
     isModalVisible: false,
     classname:'',
     idclass:'',
   }
 }
 modalOk(classdata){
  this.setState({
     idclass:classdata.class_id,
     classname:classdata.class_name,
     isModalVisible: !this.state.isModalVisible
  });
 }
 _ModalAdd = () =>
 this.setState({  
   idclass:'',
   classname:'',
   isModalVisible: !this.state.isModalVisible });
GetClass(classdata) {
  Alert.alert(
    'Action',
    'Choice Action',
    [
      {text: 'Detail', onPress: () => this.detailClass(classdata)},
      {text: 'Edit', onPress: () => this.modalOk(classdata)},
      {
        text: 'Close',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
    ],
    {cancelable: false},
  );
}
detailClass(classdata){
   this.props.navigation.navigate('DetailClassScreen', {
    classid: classdata
  });
}
componentDidMount() {
   this.dataclass();
 }
 dataclass(){
  fetch('http://gracehaven.tri-niche.com/gracenatalie/api/getclass')
  .then((response) => response.json())
  .then((responseJson) => {
    let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.setState({
      isLoading: false,
      dataSource: ds.cloneWithRows(responseJson.class),
    }, function() {
      // In this block you can do something with new state.
    });
  })
  .catch((error) => {
    console.error(error);
  });
 }
 processSubmit(idclass){
  if (this.state.classname=='') {
      Alert.alert(
        'Warning',
        'Please Input Class Name');
  }
  else{
    this.setState({loading: true})
        fetch('http://gracehaven.tri-niche.com/gracenatalie/api/processclass', {
           method: 'POST',
           headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
          },
          body: JSON.stringify({
                classname : this.state.classname,
                classid:this.state.idclass
          })
          }).then((response) => response.json())
            .then((responseJson) => {
              this.setState({
                      loading: false
                  }, ()=>{
              if (responseJson.success=='TRUE') {
                  Alert.alert(responseJson.info);
                  this._ModalAdd(!this.state.isModalVisible);
                  this.dataclass();
              } else {
                 this.setState({ spinner: false });
                      setTimeout(() => {
                        Alert.alert('Warning',responseJson.info);
                      }, 100);
              }
              }
                );
          }).catch((error) => {
            console.error(error);
          }); 
      }
}
ListViewItemSeparator = () => {
   return (
     <View
       style={{
         height: .3,
         width: "100%",
         // backgroundColor: "#000",
       }}
     />
   );
} 
render() {
   if (this.state.isLoading) {
     return (
       <View style={{flex: 1,justifyContent:'center'}}>
         <ActivityIndicator />
       </View>
     );
   }
   
   return (

     <View style={styles.MainContainer}>
       <Text style={styles.textResident} >List Class</Text>
       <ListView
         dataSource={this.state.dataSource}
         renderSeparator= {this.ListViewItemSeparator}
         renderRow={(rowData) =>
        <View style={{flex:1, flexDirection: 'row',backgroundColor:'skyblue',marginBottom:2}}>
          <Text onPress={this.GetClass.bind(this, rowData)} style={styles.textViewContainer} >{rowData.class_id}-{rowData.class_name}</Text>
        </View>
         }
       />
       <TouchableOpacity onPress={this._ModalAdd} style={styles.TouchableOpacityStyle} >
          <Image source={require('../images/fav.png')} 
          style={styles.FloatingButtonStyle} />
        </TouchableOpacity>
        <View>
       
        <Modal isVisible={this.state.isModalVisible}>
        <View style={styles.modalContent}>
          <Text style={styles.textinput}>Class</Text>
          <TextInput style={styles.inputBox} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholder="class name"
              placeholderTextColor = "#000000"
              selectionColor="#0077c2"
              borderColor="#000000"
              value={this.state.classname}
              onChangeText={classname => this.setState({classname})}
              />  
           <TouchableOpacity style={styles.button} onPress={() => {this.processSubmit(this.state.idclass)}}>
             <Text style={styles.buttonText2}>Submit</Text>
           </TouchableOpacity>
           <TouchableOpacity style={styles.buttonCancel} onPress={this._ModalAdd}>
             <Text style={styles.buttonText2}>Cancel</Text>
           </TouchableOpacity>  
        </View>
      </Modal>
   </View>
     </View>
     
   );
 }
}

const styles = StyleSheet.create({

MainContainer :{

// Setting up View inside content in Vertically center.
justifyContent: 'center',
flex:1,
margin: 5,
paddingTop: (Platform.OS === 'ios') ? 20 : 0,

},

imageViewContainer: {
    width: '50%',
    height: 100 ,
    margin: 10,
    borderRadius : 10
},
textViewContainer: {
  textAlignVertical:'center',
  width:'50%', 
  color:'#FFFFFF',
  padding:7
},
modalContent: {
  height: 200,
  backgroundColor: "#FFFFFF",
  padding: 22,
  justifyContent: "center",
  borderRadius: 4,
  borderColor: "rgba(0, 0, 0, 0.1)"
},
buttonText2: {
  fontSize:12,
  fontWeight:'400',
  color:'#ffffff',
  textAlign:'center'
},
textResident: {
    textAlignVertical:'center',
    color:'#000000',
    padding:7
},
TouchableOpacityStyle:{
  position: 'absolute',
  width: 50,
  height: 50,
  alignSelf:'flex-end',
  alignItems: 'center',
  justifyContent: 'center',
  right: 10,
  bottom: 20,
},
buttonContainer: {
  flex: 1,
},
FloatStyle:{
  position: 'absolute',
  width: 50,
  height: 50,
  alignSelf:'flex-end',
  alignItems: 'center',
  justifyContent: 'center',
  right: 30,
  bottom: 10,
},
FloatingButtonStyle: {
  resizeMode: 'contain',
  width: 50,
  height: 50,
},
container: {
  paddingTop: 10
},
input: {
  margin: 15,
  height: 40,
  borderColor: '#7a42f4',
  borderWidth: 1
},
textinput:{
  textAlign:'left'
},
submitButton: {
  backgroundColor: '#7a42f4',
  padding: 10,
  margin: 15,
  height: 20,
},
submitButtonText2:{
  color: 'white'
},
inputBox: {
  backgroundColor:'rgba(255, 255,255,0.2)',
  fontSize:16,
  color:'#000000',
},
button: {
  backgroundColor:'#0077c2',
    marginVertical: 2,
    paddingVertical: 2
},
buttonCancel: {
  backgroundColor:'#9e9e9e',
    marginVertical: 2,
    paddingVertical: 2
},
buttonText2: {
  fontSize:14,
  fontWeight:'500',
  color:'#ffffff',
  textAlign:'center'
}
});

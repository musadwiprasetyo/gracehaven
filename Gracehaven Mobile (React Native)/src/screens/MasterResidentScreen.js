import React, { Component } from 'react';
import { AppRegistry, StyleSheet, ActivityIndicator, ListView, Text, View, Alert,Image, Platform} from 'react-native';

export default class MasterResidentScreen extends Component {
	constructor(props) {
   super(props);
   this.state = {
     isLoading: true,
     dataSource:[]
   }
 }
 
GetResident (residentid) {
 // Alert.alert(residentid);
}
componentDidMount() {
   return fetch('http://gracehaven.tri-niche.com/gracenatalie/api/getresident')
     .then((response) => response.json())
     .then((responseJson) => {
       let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
       this.setState({
         isLoading: false,
         dataSource: ds.cloneWithRows(responseJson.resident),
       }, function() {
         // In this block you can do something with new state.
       });
     })
     .catch((error) => {
       console.error(error);
     });
 }

 ListViewItemSeparator = () => {
   return (
     <View
       style={{
         height: .5,
         width: "100%",
         backgroundColor: "#000",
       }}
     />
   );
 }


 render() {
   if (this.state.isLoading) {
     return (
       <View style={{flex: 1, paddingTop: 20}}>
         <ActivityIndicator />
       </View>
     );
   }

   return (

     <View style={styles.MainContainer}>
       <Text style={styles.textResident} >List Resident</Text>
       <ListView
         dataSource={this.state.dataSource}
         renderSeparator= {this.ListViewItemSeparator}
         renderRow={(rowData) =>
        <View style={{flex:1, flexDirection: 'row'}}>
          <Text onPress={this.GetResident.bind(this, rowData.resident_id)} style={styles.textViewContainer} >{rowData.resident_id}-{rowData.name}</Text>
        </View>
         }
       />
     </View>
   );
 }
}

const styles = StyleSheet.create({

MainContainer :{

// Setting up View inside content in Vertically center.
justifyContent: 'center',
flex:1,
margin: 5,
paddingTop: (Platform.OS === 'ios') ? 20 : 0,

},

imageViewContainer: {
    width: '50%',
    height: 100 ,
    margin: 10,
    borderRadius : 10
},
textViewContainer: {
  textAlignVertical:'center',
  width:'50%', 
  color:'#000000',
  padding:7
},
textResident: {
    textAlignVertical:'center',
    color:'#000000',
    padding:7
}
});

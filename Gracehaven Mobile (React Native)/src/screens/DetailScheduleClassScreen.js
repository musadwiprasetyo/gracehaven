import React, { Component } from 'react';
import {AppRegistry,Picker
,TouchableHighlight,StyleSheet,TextInput,Button,ActivityIndicator, ListView, Text, View, Alert,Image, Platform,TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from "react-native-modal";
import DatePicker from 'react-native-datepicker';
var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
export default class DetailScheduleClassScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
          isLoading: true,
          dataSource:ds,
          isModalVisible: false,
          classname:'',
          idclass:'',
          idclassstaff:'',
          idschedule:'',
          schedule:'',
          date:'',
          note:'',
          listViewProportion:'',
        }
    }
    componentWillMount() {
        var date = new Date().getDate(); //Current Date
        var month = new Date().getMonth() + 1; //Current Month
        var year = new Date().getFullYear(); //Current Year
        this.setState({
          date:year + '-' + month + '-' + date,
          idclassstaff:this.props.navigation.state.params.class.id
        });
        this.dataschedule(this.props.navigation.state.params.class.id)
    }
    modalOk(schedule){
      // Alert.alert(JSON.stringify(schedule));
      this.setState({
         schedule    :schedule.schedule,
         idschedule  :schedule.id,
         note        :schedule.note,
         idclassstaff:schedule.id_class_staff,
         date        :schedule.date,
         isModalVisible: !this.state.isModalVisible
      });
     }
  _ModalAdd = () =>
    this.setState({  
     idschedule:'',
     schedule:'',
     note:'',
     isModalVisible: !this.state.isModalVisible });

    GetSchedule(schedule)
    {
        Alert.alert(
            'Action',
            'Choice Action',
            [
              {text: 'Detail',  onPress: () => this.detailSchedule(schedule)},
              {text: 'Edit', onPress: () => this.modalOk(schedule)},
              {
                text: 'Close',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
            ],
            {cancelable: false},
          );
    }
    detailSchedule(schedule){
      this.props.navigation.navigate('DetailScheduleScreen', {
        schedule: schedule
     });
   }
    Attendance(dataschedule,classid){
        this.props.navigation.navigate('AttendanceScreen', {
         dataschedule: dataschedule,
         classid:classid
       });
     }
    dataschedule(id){
        fetch('http://gracehaven.tri-niche.com/gracenatalie/api/getclassstaffschedule/'+id)
        .then((response) => response.json())
        .then((responseJson) => {
        // let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.setState({
            isLoading: false,
            dataSource: ds.cloneWithRows(responseJson.schedule),
        }, function() {
            // In this block you can do something with new state.
        });
        })
        .catch((error) => {
        console.error(error);
        });
   }
   ListViewItemSeparator = () => {
    return (
      <View
        style={{
          height: .3,
          width: "100%",
        }}
      />
    );
 } 
 processSubmit(idschedule){
  if (this.state.schedule=='') {
      Alert.alert(
        'Warning',
        'Please Input Schedule');
  }else if (this.state.note=='') {
      Alert.alert(
        'Warning',
        'Please Input Note');
  }
  else{
    this.setState({loading: true})
        fetch('http://gracehaven.tri-niche.com/gracenatalie/api/processchedule', {
           method: 'POST',
           headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
          },
          body: JSON.stringify({
                schedule      : this.state.schedule,
                idschedule    : this.state.idschedule,
                note          : this.state.note,
                scheduledate  : this.state.date,
                idclassstaff  : this.state.idclassstaff,
          })
          }).then((response) => response.json())
            .then((responseJson) => {
              this.setState({
                      loading: false
                  }, ()=>{
              if (responseJson.success=='TRUE') {
                  Alert.alert(responseJson.info);
                  this._ModalAdd(!this.state.isModalVisible);
                  this.dataschedule(this.state.idclassstaff);
              } else {
                 this.setState({ spinner: false });
                      setTimeout(() => {
                        Alert.alert('Warning',responseJson.info);
                      }, 100);
              }
              }
                );
          }).catch((error) => {
            console.error(error);
          }); 
      }
}
render() {
    const { navigation } = this.props;
    const classid = navigation.getParam('class');
    const listViewProportion = this.state.dataSource.getRowCount() == 0 ? 0 : 1
    // Alert.alert(JSON.stringify(classid));
    if (this.state.isLoading) {
     return (
       <View style={{flex: 1,justifyContent:'center'}}>
         <ActivityIndicator />
       </View>
     );
   }else{
    return (

    <View style={styles.MainContainer}>
        <View>
        <Text style={styles.textResident} >Class Schedule Detail</Text>
        </View>
        <View>
          <View style={{flexDirection:'row'}}>
            <Text style={styles.textResident} >Class Name</Text>
            <Text style={styles.textResident} >{classid.id} | {classid.class_name}</Text>
          </View>
          <View style={{flexDirection:'row'}}>
            <Text style={styles.textResident} >Staff Name</Text>
            <Text style={styles.textResident} >{classid.staffid} | {classid.staffname} </Text>
          </View>
        </View>
        <Text style={styles.textResident} >List Schedule</Text>
         {listViewProportion > 0
                ?
                <ListView
                 dataSource={this.state.dataSource}
                 renderSeparator= {this.ListViewItemSeparator}
                 renderRow={(rowData,sectionID,rowID)=>
                <View style={{flex:1, flexDirection: 'row',backgroundColor:'skyblue',marginBottom:2}}>
                  <Text onPress={this.GetSchedule.bind(this, rowData)} style={styles.textViewContainer} >({rowData.date})-{rowData.schedule}</Text>
                  <Icon
                    onPress={this.Attendance.bind(this, rowData,classid)}
                    name="qrcode"
                    size={30}
                    color="#FF0000"
                    style={styles.textViewContainer2}
                  />

                </View>
                 }
               />
                : // if false
                <Text style={styles.nodata}>No Data</Text>
            }
       <TouchableOpacity onPress={this._ModalAdd} style={styles.TouchableOpacityStyle} >
          <Image source={require('../images/fav.png')} 
          style={styles.FloatingButtonStyle} />
        </TouchableOpacity>
        <Modal isVisible={this.state.isModalVisible}>
        <View style={styles.modalContent}>
          <Text style={styles.textinput}>Schedule</Text>
           <TextInput style={styles.inputBox} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholderTextColor = "#000000"
              selectionColor="#0077c2"
              borderColor="#000000"
              value={this.state.schedule}
              onChangeText={schedule => this.setState({schedule})}
              />
          <Text style={styles.textinput}>Date</Text>
           <DatePicker
          style={{width: 200}}
          date={this.state.date} //initial date from state
          mode="date" //The enum of date, datetime and time
          placeholder="select date"
          format="YYYY-MM-DD"
          minDate="2019-01-01"
          maxDate="2029-01-01"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          onDateChange={(date) => {this.setState({date: date})}}
        />

          <Text style={styles.textinput}>Note</Text>
           <TextInput style={styles.inputBox} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholderTextColor = "#000000"
              selectionColor="#0077c2"
              borderColor="#000000"
              value={this.state.note}
              onChangeText={note => this.setState({note})}
              />  
           <TouchableOpacity style={styles.button} onPress={() => {this.processSubmit()}}>
             <Text style={styles.btnTxts}>Submit</Text>
           </TouchableOpacity>
           <TouchableOpacity style={styles.buttonCancel} onPress={this._ModalAdd}>
             <Text style={styles.btnTxts}>Cancel</Text>
           </TouchableOpacity>  
        </View>
      </Modal>
    </View>

    );
  }
  }
}
const styles = StyleSheet.create({
    MainContainer :{
    flex:1,
    marginTop: 5,
    paddingTop: (Platform.OS === 'ios') ? 20 : 0,
    },
    textViewContainer: {
      textAlignVertical:'center',
      width:'50%', 
      color:'#000000',
      padding:7,
      color:'blue'
    },
    textViewContainer2: {
        width:'50%', 
        color:'#FF0000',
        padding:7,
        textAlign:'right'
      },
      textViewContainer3: {
        width:'50%', 
        color:'#000000',
        padding:7,
        textAlign:'right'
      },
    modalContent: {
      height: 400,
      backgroundColor: "#FFFFFF",
      padding: 22,
      justifyContent: "center",
      borderRadius: 4,
      borderColor: "rgba(0, 0, 0, 0.1)"
    },
    btnmytext: {
      fontSize:12,
      fontWeight:'400',
      color:'#ffffff',
      textAlign:'center'
    },
    textResident: {
        textAlignVertical:'center',
        color:'#000000',
        padding:7,
        width:'50%'
    },
    TouchableOpacityStyle:{
      position: 'absolute',
      width: 50,
      height: 50,
      alignSelf:'flex-end',
      alignItems: 'center',
      justifyContent: 'center',
      right: 10,
      bottom: 20,
    },
    buttonContainer: {
      flex: 1,
    },
    FloatStyle:{
  position: 'absolute',
  width: 50,
  height: 50,
  alignSelf:'flex-end',
  alignItems: 'center',
  justifyContent: 'center',
  right: 30,
  bottom: 10,
},
FloatingButtonStyle: {
  resizeMode: 'contain',
  width: 50,
  height: 50,
},
container: {
  paddingTop: 10
},
input: {
  margin: 15,
  height: 40,
  borderColor: '#7a42f4',
  borderWidth: 1
},
textheader:{
  textAlign:'left',
  color:'#FFFFFF',
  fontSize:14
},
textinput:{
  textAlign:'left'
},
submitButton: {
  backgroundColor: '#7a42f4',
  padding: 10,
  margin: 15,
  height: 20,
},
submitBtnTxts:{
  color: 'white'
},
inputBox: {
  backgroundColor:'#FFFFFF',
  fontSize:16,
  color:'#000000',
  borderColor: '#7a42f4',
  borderWidth: 1
},
button: {
  backgroundColor:'#0077c2',
    marginVertical: 2,
    paddingVertical: 2
},
buttonCancel: {
  backgroundColor:'#9e9e9e',
    marginVertical: 2,
    paddingVertical: 2
},
btnTxts: {
  fontSize:14,
  fontWeight:'500',
  color:'#ffffff',
  textAlign:'center'
},
nodata:{
  marginLeft:8
}
});
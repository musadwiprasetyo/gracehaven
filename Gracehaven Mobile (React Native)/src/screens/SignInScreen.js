import React, { Component } from 'react';
import {StyleSheet,View,TextInput,Button,TouchableOpacity,Text,Alert,ActivityIndicator,AsyncStorage,ScrollView,FlatList,Image} from 'react-native';

export default class SignInScreen extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      token: '',
      type:'',
      username: '',
      password: '',
      loading: true
    };
  }
  componentWillMount() { 
   AsyncStorage.getItem('token').then( (value) => {
     if(value) {
        this.setState({
                token: value
        });
       AsyncStorage.getItem('type').then( (valuetype) => {
         if(valuetype) {
            this.setState({
                    type: valuetype
                });
          if (valuetype=='1') {
            this.props.navigation.navigate('MainStack')
          }else if(valuetype=='2'){
            this.props.navigation.navigate('MainStaffStack')
          }else {
            this.props.navigation.navigate('MainResidentStack')
          }
           
         } 
       })
     } else {
       this.props.navigation.navigate('SignInStack');
       console.log('login')
    }
   })
   setTimeout(()=>{
      this.setState({loading:false
      })
    },3000)
 } 
 login(){
      if (this.state.username=='') {
          Alert.alert(
            'Warning',
            'Please Input Username');
      }
      else if (this.state.password=='') {
          Alert.alert(
            'Warning',
            'Please Input Password');
      }else{
        this.setState({loading: true})
        fetch('http://gracehaven.tri-niche.com/gracenatalie/api/login', {
           method: 'POST',
           headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
          },
          body: JSON.stringify({
                username : this.state.username,
                password : this.state.password
          })
          }).then((response) => response.json())
            .then((responseJson) => {
              this.setState({
                      loading: false
                  }, ()=>{
              if (responseJson.success=='TRUE') {
                
                  AsyncStorage.setItem('username', responseJson.username);
                  AsyncStorage.setItem('userid', responseJson.id);
                  AsyncStorage.setItem('token', responseJson.token);
                  AsyncStorage.setItem('type', responseJson.type);
                  AsyncStorage.setItem('name', responseJson.name);
                  if (responseJson.type=='1') {
                    this.props.navigation.navigate('MainStack')
                  }else if(responseJson.type=='2'){
                    this.props.navigation.navigate('MainStaffStack')
                  }else {
                    this.props.navigation.navigate('MainResidentStack')
                  }
              } else {
                 this.setState({ spinner: false });
                      setTimeout(() => {
                        Alert.alert('Warning',responseJson.info);
                      }, 100);      

              }
              }
                );
          }).catch((error) => {
            console.error(error);
          }); 
      }
  }
  render() {
     if(this.state.loading){
           return(
            <View style={styles.container}>
               <ActivityIndicator size="large" color="#FFFFFF"/>
            </View>
           )
    }else{
    return (
      <View style={styles.container}>
        <Image style={styles.images} source={require('../images/sa-logotext.png')}/>    
        <TextInput style={styles.inputBox} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholder="Username"
              placeholderTextColor = "#000000"
              selectionColor="#0077c2"
              onChangeText={username => this.setState({username})}
              />
          <TextInput style={styles.inputBox} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholder="Password"
              secureTextEntry={true}
              placeholderTextColor = "#000000"
              selectionColor="#0077c2"
              onChangeText={password => this.setState({password})}
              ref={(input) => this.password = input}
              />  
           <TouchableOpacity style={styles.button} onPress={() => {this.login()}}>
             <Text style={styles.btnTxt}>Login</Text>
           </TouchableOpacity>    
      </View>
    );
  }
  }
}
const styles = StyleSheet.create({
  container:{
    backgroundColor:'#FFFFFF',
    flex: 1,
    alignItems:'center',
    justifyContent :'center'
  },
  images:{
    marginTop:-100,
    marginBottom:30,
    width:300, 
    height:70
  },
  inputBox: {
    width:300,
    backgroundColor:'#FFFFFF',
    paddingHorizontal:16,
    fontSize:16,
    color:'#0077c2',
    marginVertical: 10
  },
  button: {
    width:300,
    backgroundColor:'#0077c2',
      marginVertical: 10,
      paddingVertical: 13
  },
  btnTxt: {
    fontSize:16,
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  }
}); 
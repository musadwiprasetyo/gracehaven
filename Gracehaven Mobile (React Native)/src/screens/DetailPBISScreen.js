import React, { Component } from 'react';
import {AppRegistry,Picker
,TouchableHighlight,AsyncStorage,StyleSheet,TextInput,Button,ActivityIndicator, ListView, Text, View, Alert,Image, Platform,TouchableOpacity} from 'react-native';

import { NavigationActions } from 'react-navigation';
import Select from 'react-native-select-plus';
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from "react-native-modal";
import DatePicker from 'react-native-datepicker';
var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
export default class DetailPBISScreen extends Component {
  constructor(props) {
   super(props);
   this.state = {
     isLoading: true,
     dataSource:ds,
     dataSourceSelect:[],
     isModalVisible: false,
     classname:'',
     idclass:'',
     userid:'',
     value:'',
     routineid:'',
     valueResident : '',
     valueClass : '',
     valueDay : '',
     date:'',
     Default_Rating: 1,
     Max_Rating : 5
   }
    this.Star = 'https://reactnativecode.com/wp-content/uploads/2018/01/full_star.png';
    this.Star_With_Border = 'https://reactnativecode.com/wp-content/uploads/2018/01/border_star.png';
   
 }
  UpdateRating(key)
  {
      this.setState({ Default_Rating: key });
  }
  componentWillMount() {
        // Alert.alert(JSON.stringify(this.props.navigation.state.params.pbisid.id));
      this.datapbis(this.props.navigation.state.params.pbisid.id)
      this.dataresident()
       AsyncStorage.getItem('userid').then((value) => {
         this.setState({
              userid: value
        });
      });
      var date = new Date().getDate(); //Current Date
      var month = new Date().getMonth() + 1; //Current Month
      var year = new Date().getFullYear(); //Current Year
      this.setState({
          date:year + '-' + month + '-' + date,
      });
  }
  dataresident(){
  fetch('http://gracehaven.tri-niche.com/gracenatalie/api/masterresident')
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            isLoading: false,
            dataSourceSelect: responseJson.data,
            valueResident:responseJson.default
          }, function() {
            // In this block you can do something with new state.
          });
        })
        .catch((error) => {
          console.error(error);
        });
  }
  datapbis(id){
        fetch('http://gracehaven.tri-niche.com/gracenatalie/api/routine/'+id)
        .then((response) => response.json())
        .then((responseJson) => {
        this.setState({
            isLoading: false,
            dataSource: ds.cloneWithRows(responseJson.routine),
        }, function() {
            // In this block you can do something with new state.
        });
        })
        .catch((error) => {
        console.error(error);
        });
   }
   ListViewItemSeparator = () => {
   return (
     <View
       style={{
         height: .5,
         width: "100%"
       }}
     />
   );
}
processSubmit(routineid){
  if (this.state.date=='') {
      Alert.alert(
        'Warning',
        'Please Input Date');
  }else if(this.state.note==''){
      Alert.alert(
        'Warning',
        'Please Input Description');
  }
  else{
    this.setState({loading: true})
        fetch('http://gracehaven.tri-niche.com/gracenatalie/api/processpbis', {
           method: 'POST',
           headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
          },
          body: JSON.stringify({
                routine_id    : this.state.routineid,
                resident      : this.state.valueResident,
                date          : this.state.date,
                description   : this.state.note,
                star          : this.state.Default_Rating,
                staffid       : this.state.userid,
          })
          }).then((response) => response.json())
            .then((responseJson) => {
              this.setState({
                      loading: false
                  }, ()=>{
              if (responseJson.success=='TRUE') {
                  Alert.alert(responseJson.info);
                  this._ModalAdd(!this.state.isModalVisible);
              } else {
                 this.setState({ spinner: false });
                      setTimeout(() => {
                        Alert.alert('Warning',responseJson.info);
                      }, 100);
              }
              }
                );
          }).catch((error) => {
            console.error(error);
          }); 
      }
}

_ModalAdd = (routine) =>
    // Alert.alert(JSON.stringify(routine.id));
    this.setState({  
     routineid:routine.id,
     note:'',
     isModalVisible: !this.state.isModalVisible });

  render() {
    const { navigation } = this.props;
    const pbis = navigation.getParam('pbisid');
    let React_Native_Rating_Bar = [];
 
        for( var i = 1; i <= this.state.Max_Rating; i++ )
        {
          React_Native_Rating_Bar.push(
 
                <TouchableOpacity 
                  activeOpacity = { 0.7 } 
                  key = { i } 
                  onPress = { this.UpdateRating.bind( this, i ) }>
 
                  
                    <Image 
                      style = { styles.StarImage } 
                      source = { ( i <= this.state.Default_Rating ) ? { uri: this.Star } : { uri: this.Star_With_Border } } />
                
                
                </TouchableOpacity>
            
          );
        }

    if (this.state.isLoading) {
     return (
       <View style={{flex: 1,justifyContent:'center'}}>
         <ActivityIndicator />
       </View>
     );
   }
   return (
     <View style={styles.MainContainer}>
        
       <Text style={styles.textResident} >List Routine</Text>
       <ListView
         dataSource={this.state.dataSource}
         renderSeparator= {this.ListViewItemSeparator}
         renderRow={(rowData) =>
        <View style={{flex:1, flexDirection: 'row',backgroundColor:'skyblue',marginBottom:2}}>
          <View style={{flex:1,width:'100%'}}>
          <Text onPress={this._ModalAdd.bind(this, rowData)} style={styles.textViewContainer} >
          {rowData.routine_name}
          </Text>
          </View>
          <Text style={styles.textViewContainerRight} >
           
          </Text>
        </View>
         }
       />
      <View>
   </View>
    <Modal isVisible={this.state.isModalVisible}>
        <View style={styles.modalContent}>
          <Text style={styles.textinput}>Resident</Text>
           <Picker
            selectedValue={this.state.valueResident}
            onValueChange={(classResident, classIndex) => this.setState({valueResident: classResident})} >
            { this.state.dataSourceSelect.map((valresident, key)=>(
            <Picker.Item label={valresident.name} value={valresident.resident_id} key={key} />)
            )}
          </Picker>
          <Text style={styles.textinput}>Date</Text>
           <DatePicker
            style={{width: 200}}
            date={this.state.date} //initial date from state
            mode="date" //The enum of date, datetime and time
            placeholder="select date"
            format="YYYY-MM-DD"
            minDate="2019-01-01"
            maxDate="2029-01-01"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            onDateChange={(date) => {this.setState({date: date})}}
          />
          <Text style={styles.textinput}>Star Rating</Text>
          <View style = { styles.childView }>
                {React_Native_Rating_Bar}
          </View>
          <Text style={styles.textinput}>Description</Text>
           <TextInput style={styles.inputBox} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholderTextColor = "#000000"
              selectionColor="#0077c2"
              borderColor="#000000"
              value={this.state.note}
              onChangeText={note => this.setState({note})}
              />  
           <TouchableOpacity style={styles.button} onPress={() => {this.processSubmit(this.state.routineid)}}>
             <Text style={styles.btnTxts}>Submit</Text>
           </TouchableOpacity>
           <TouchableOpacity style={styles.buttonCancel} onPress={this._ModalAdd}>
             <Text style={styles.btnTxts}>Cancel</Text>
           </TouchableOpacity>  
        </View>
      </Modal>
     </View>
     
   );
 }
}
const styles = StyleSheet.create({

MainContainer :{

// Setting up View inside content in Vertically center.
justifyContent: 'center',
flex:1,
margin: 5,
paddingTop: (Platform.OS === 'ios') ? 20 : 0,

},

imageViewContainer: {
    width: '70%',
    height: 100 ,
    margin: 10,
    borderRadius : 10
},
textViewContainerRight: {
  textAlign:'right',
  width:'30%', 
  color:'#FF0000'
},
textViewContainer: {
  textAlignVertical:'center',
  width:'100%', 
  color:'#FFFFFF'
},
textViewStaff: {
  width:'100%', 
  color:'#FFFF00',
  marginLeft:10
},
modalContent: {
  height: 400,
  backgroundColor: "#FFFFFF",
  padding: 22,
  justifyContent: "center",
  borderRadius: 4,
  borderColor: "rgba(0, 0, 0, 0.1)"
},
textResident: {
    textAlignVertical:'center',
    color:'#000000',
    padding:7
},
TouchableOpacityStyle:{
  position: 'absolute',
  width: 50,
  height: 50,
  alignSelf:'flex-end',
  alignItems: 'center',
  justifyContent: 'center',
  right: 10,
  bottom: 20,
},
buttonContainer: {
  flex: 1,
},
FloatStyle:{
  position: 'absolute',
  width: 50,
  height: 50,
  alignSelf:'flex-end',
  alignItems: 'center',
  justifyContent: 'center',
  right: 30,
  bottom: 10,
},
FloatingButtonStyle: {
  resizeMode: 'contain',
  width: 50,
  height: 50,
},
container: {
  paddingTop: 10
},
input: {
  margin: 15,
  height: 40,
  borderColor: '#7a42f4',
  borderWidth: 1
},
textheader:{
  textAlign:'left',
  color:'#FFFFFF',
  fontSize:14
},
textinput:{
  textAlign:'left'
},
submitButton: {
  backgroundColor: '#7a42f4',
  padding: 10,
  margin: 15,
  height: 20,
},
submitBtnTxts:{
  color: 'white'
},
inputBox: {
  backgroundColor:'rgba(255, 255,255,0.2)',
  fontSize:16,
  color:'#000000',
},
button: {
  backgroundColor:'#0077c2',
    marginVertical: 2,
    paddingVertical: 2
},
buttonCancel: {
  backgroundColor:'#9e9e9e',
    marginVertical: 2,
    paddingVertical: 2
},
btnTxts: {
  fontSize:14,
  fontWeight:'500',
  color:'#ffffff',
  textAlign:'center'
},
 childView:{
        justifyContent: 'center',
        flexDirection: 'row',
},
StarImage:{
    width: 40,
    height: 40,
    resizeMode: 'cover'
},
textStyle:{
    textAlign: 'center',
    fontSize: 23,
    color: '#000',
    marginTop: 15
}
});

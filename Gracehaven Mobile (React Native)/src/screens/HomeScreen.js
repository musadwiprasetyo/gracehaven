import React, { Component } from 'react';
import {AppRegistry,Picker,AsyncStorage
,TouchableHighlight,StyleSheet,TextInput,Button,ActivityIndicator, ListView, Text, View, Alert,Image, Platform,TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from "react-native-modal";
import DatePicker from 'react-native-datepicker';
var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

export default class HomeScreen extends Component {
	constructor(props) {
	    super(props);
	    this.state = {
	      token: '',
	      type:'',
	      data:'',
	      userid:'',
	      loading: true,
        dataSource:ds,
	    };
	    
	}
	componentWillMount(){
	    AsyncStorage.getItem('userid').then((value) => {
	       this.setState({
	            userid: value
	     	});
			this.fetchdata()
      this.schedule()
     
		});
		setTimeout(()=>{
      this.setState({loading:false
      })
    },3000)
	    
	}
  schedule(){
   fetch('http://gracehaven.tri-niche.com/gracenatalie/api/getclassstafftoday/'+this.state.userid)
  .then((response) => response.json())
  .then((responseJson) => {
    this.setState({
      loading: false,
      dataSource: ds.cloneWithRows(responseJson.class),
    }, function() {
    });
  })
  .catch((error) => {
    console.error(error);
  });
  }
	fetchdata(){
		fetch('http://gracehaven.tri-niche.com/gracenatalie/api/profilstaff', {
	       method: 'POST',
	       headers: {
	          'Accept': 'application/json',
	          'Content-Type': 'application/json',
	      },
	      body: JSON.stringify({
	            userid :this.state.userid
	      })
	      }).then((response) => response.json())
	        .then((responseJson) => {
	          	this.setState({
	         		data:responseJson.data
	      		})
	      }).catch((error) => {
	        console.error(error);
	      });
	}
	biodata()
	{
		this.props.navigation.navigate('StaffScreen');	
	}
  GetClass(classdata) {
    this.props.navigation.navigate('DetailScheduleClassScreen', {
        class: classdata
    });
  }
  GetPBIS() {
    this.props.navigation.navigate('PBISScreen');
  }
  ListViewItemSeparator = () => {
   return (
     <View
       style={{
         height: .3,
         width: "100%",
         // backgroundColor: "#000",
       }}
     />
   );
} 
  render() {
    const listViewProportion = this.state.dataSource.getRowCount() == 0 ? 0 : 1
  	
    if(this.state.loading){
           return(
            <View style={{flex: 1,justifyContent:'center'}}>
               <ActivityIndicator size="large" color="#FFFFFF"/>
            </View>
           )
    }else{
    return (
      <View style={{flex: 1}}>
       <View style={{flex:1,backgroundColor:'#FFFFFF'}}>
       	<View>
       		<Text style={{textAlign: 'left',marginLeft:10,color:'#000000',marginTop:20,fontSize:16}}>Hello,</Text>
       	</View>
       	<View>
       		<View style={{flexDirection:'row'}}>
       			<Text style={styles.textViewContainer} >
           			{this.state.data.name}
        		</Text>
        		<Text onPress={() => {this.biodata()}} style={styles.textViewRight} >
           			Profile
        		</Text>
       		</View>
       	</View>
       	<View>
       		<View>
       			<Text style={{textAlign: 'left',marginLeft:10,color:'#000000',marginTop:20,fontSize:16}}>Bible Today</Text>
       		</View>
       		<View style={{backgroundColor: 'skyblue'}}>
       			<Text style={{color:'#FF0000',textAlign:'left',marginLeft:10}}>Matthew 6:33</Text>
       			<Text style={{color:'#FFFFFF',marginBottom:10,textAlign:'left',marginLeft:10}}>But let your first care be for his kingdom and his righteousness; and all these other things will be given to you in addition</Text>
       		</View>
       		
       	</View>
       	<View>
       		<View>
       			<Text style={{textAlign: 'left',marginLeft:10,color:'#000000',marginTop:20,fontSize:16}}>Schedule Today</Text>
       		</View>
          {listViewProportion > 0
                ?
          <ListView
           dataSource={this.state.dataSource}
           renderSeparator= {this.ListViewItemSeparator}
           renderRow={(rowData) =>
          <View style={{flex:1, flexDirection: 'row',backgroundColor:'skyblue',marginBottom:2}}>
            <View style={{flex:1,width:'100%'}}>
            <Text onPress={this.GetClass.bind(this, rowData)} style={styles.textViewSchedule} >
            {rowData.class_name}
            </Text>
            </View>
            <Text style={styles.textViewRightSchedule} >
             {rowData.clock}
            </Text>
          </View>
           }
         />
         :
          <Text style={styles.nodata}>No Schedule</Text>
      }
       		
       		
       	</View>
       	<View>
       		<Text style={{textAlign: 'left',marginLeft:10,color:'#000000',marginTop:20,fontSize:16}}>PBIS</Text>
       		<Text onPress={() => {this.GetPBIS()}} style={{textAlign:'center',height: 30, justifyContent:'center',backgroundColor: 'steelblue',color:'#FFFFFF',marginTop:20,fontSize:16}}>Click to Manage PBIS </Text>
       	</View>
       </View>
       <View style={{height:44,backgroundColor:'#000000'}}></View>
      	
      </View>

    );
  }
  }
}
const styles = StyleSheet.create({
textViewContainer: {
  textAlign:'left',
  width:'50%', 
  color:'#FF0000',
  marginLeft:10,
  fontSize:16,
  textTransform: 'uppercase'
},
textViewSchedule: {
  textAlign:'left',
  width:'60%', 
  color:'#FFFFFF',
  marginLeft:10
},
title:{
	textAlign: 'left',
	marginLeft:10,
	color:'#000000',
	fontSize:16,
	marginTop:100

},
textViewRight:{
  textAlign:'left',
  width:'20%', 
  color:'#0077c2',
  marginLeft:120,
  fontSize:16
},
textViewRightSchedule:{
  textAlign:'right',
  width:'40%', 
  color:'#FF0000',
  marginLeft:70
},
nodata:{
  marginLeft:8
}
});

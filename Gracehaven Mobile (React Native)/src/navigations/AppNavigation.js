import React from 'react';
import { StackNavigator, DrawerNavigator } from 'react-navigation';
import { View, TouchableOpacity, TextInput, StyleSheet, Text, Alert,ActivityIndicator,AsyncStorage,} from 'react-native';
import SignInStack from './SignInStack';
import Icon from 'react-native-vector-icons/FontAwesome';
import DrawerContent from './DrawerContent';
import DrawerContentResident from './DrawerContentResident';
import DrawerContentStaff from './DrawerContentStaff';
import HomeScreen from '../screens/HomeScreen';
import HomeScreenResident from '../screens/HomeScreenResident';

import QRScreen from '../screens/QRScreen';
import ResidentScreen from '../screens/ResidentScreen';
import StaffScreen from '../screens/StaffScreen';
import MasterResidentScreen from '../screens/MasterResidentScreen';
import MasterClassScreen from '../screens/MasterClassScreen';
import DetailClassScreen from '../screens/DetailClassScreen';
import DetailScheduleScreen from '../screens/DetailScheduleScreen';


import ScheduleScreen from '../screens/ScheduleScreen';
import DetailScheduleClassScreen from '../screens/DetailScheduleClassScreen';
import AttendanceScreen from '../screens/AttendanceScreen';
import AttendanceScreenDetail from '../screens/AttendanceScreenDetail';

import ScheduleScreenByUser from '../screens/ScheduleScreenByUser';
import PBISScreen from '../screens/PBISScreen';
import DetailPBISScreen from '../screens/DetailPBISScreen';
import RecordPBIS from '../screens/RecordPBIS';
import DetailRecordPBIS from '../screens/DetailRecordPBIS';
import DetailRecordPBISSingle from '../screens/DetailRecordPBISSingle';


const DrawerNavigationx = DrawerNavigator(
  {
    HomeScreen                : { screen: HomeScreen },
    StaffScreen               : { screen: StaffScreen },
    MasterResidentScreen      : { screen: MasterResidentScreen },
    MasterClassScreen         : { screen: MasterClassScreen },
    DetailClassScreen         : { screen: DetailClassScreen },
    DetailScheduleClassScreen : { screen: DetailScheduleClassScreen },
    ScheduleScreen            : {screen: ScheduleScreen},
    AttendanceScreen          : {screen: AttendanceScreen},
    AttendanceScreenDetail    : {screen: AttendanceScreenDetail},
    DetailScheduleScreen      : {screen: DetailScheduleScreen},
    PBISScreen                : {screen: PBISScreen},
    DetailPBISScreen          : {screen: DetailPBISScreen},
    RecordPBIS                : {screen: RecordPBIS},
    DetailRecordPBIS          : {screen: DetailRecordPBIS},
    DetailRecordPBISSingle    : {screen: DetailRecordPBISSingle},

  },
  {
    contentComponent: DrawerContent,
    drawerPosition: 'left',
    drawerWidth: 200,
  }
);
const DrawerNavigationResident = DrawerNavigator(
  {
    HomeScreenResident: { screen: HomeScreenResident },
    QRScreen: { screen: QRScreen },
    ResidentScreen: { screen: ResidentScreen },
  },
  {
    contentComponent: DrawerContentResident,
    drawerPosition: 'left',
    drawerWidth: 200,
  }
);
const DrawerNavigationStaff = DrawerNavigator(
  {
    HomeScreen: { screen: HomeScreen },
    StaffScreen: { screen: StaffScreen },
    MasterResidentScreen: { screen: MasterResidentScreen },
    DetailScheduleClassScreen: { screen: DetailScheduleClassScreen },
    ScheduleScreenByUser : {screen: ScheduleScreenByUser},
    AttendanceScreen : {screen: AttendanceScreen},
    DetailScheduleScreen : {screen: DetailScheduleScreen},
    PBISScreen                : {screen: PBISScreen},
    DetailPBISScreen          : {screen: DetailPBISScreen},
    RecordPBIS                : {screen: RecordPBIS},
    DetailRecordPBIS          : {screen: DetailRecordPBIS},
    DetailRecordPBISSingle    : {screen: DetailRecordPBISSingle},
  },
  {
    contentComponent: DrawerContentStaff,
    drawerPosition: 'left',
    drawerWidth: 200,
  }
);

const MainStack = StackNavigator(
  {
    DrawerNavigationx: DrawerNavigationx,
  },
  {
    navigationOptions: navigator => ({
      headerStyle: {
        backgroundColor: '#0077c2'
      },
      headerLeft: (
        <TouchableOpacity
          onPress={() => {
            navigator.navigation.toggleDrawer();
          }}
        >
          <Icon
            name="bars"
            size={20}
            color="#aaa"
            style={styles.headerLeftIconStyle}
          />
        </TouchableOpacity>
      ),
      headerTitle: (
        <View color="#FFFFFF">
          <Text style={styles.textWhite}>Gracehaven</Text>
        </View>
      ),
      drawerLockMode: 'locked-open',
    }),
  }
);
const MainResidentStack = StackNavigator(
  {
    DrawerNavigationResident: DrawerNavigationResident,
  },
  {
    navigationOptions: navigator => ({
      headerStyle: {
        backgroundColor: '#0077c2'
      },
      headerLeft: (
        <TouchableOpacity
          onPress={() => {
            navigator.navigation.toggleDrawer();
          }}
        >
          <Icon
            name="bars"
            size={20}
            color="#aaa"
            style={styles.headerLeftIconStyle}
          />
        </TouchableOpacity>
      ),
      headerTitle: (
        <View color="#FFFFFF">
          <Text style={styles.textWhite}>Gracehaven</Text>
        </View>
      ),
      drawerLockMode: 'locked-open',
    }),
  }
);
const MainStaffStack = StackNavigator(
  {
    DrawerNavigationStaff: DrawerNavigationStaff,
  },
  {
    navigationOptions: navigator => ({
      headerStyle: {
        backgroundColor: '#0077c2'
      },
      headerLeft: (
        <TouchableOpacity
          onPress={() => {
            navigator.navigation.toggleDrawer();
          }}
        >
          <Icon
            name="bars"
            size={20}
            color="#aaa"
            style={styles.headerLeftIconStyle}
          />
        </TouchableOpacity>
      ),
      headerTitle: (
        <View color="#FFFFFF" >
          <Text style={styles.textWhite}>Gracehaven</Text>
        </View>
      ),
      drawerLockMode: 'locked-open',
    }),
  }
);

let TransitionConfig = () => {
  return {
    screenInterpolator: ({ position, scene }) => {
      const opacity = position.interpolate({
        inputRange: [scene.index - 1, scene.index],
        outputRange: [0, 1],
      });
      return {
        opacity: opacity,
      };
    },
  };
};

const RootStackNavigator = StackNavigator(
  {
    SignInStack: { screen: SignInStack },
    MainStack: { screen: MainStack },
    MainResidentStack: { screen: MainResidentStack },
    MainStaffStack: { screen: MainStaffStack },
  },
  {
    headerMode: 'none',
    transitionConfig: TransitionConfig,
  }
);

const styles = StyleSheet.create({
  headerLeftIconStyle: {
    marginLeft: 15,
  },
  searchInputContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
    color:'#000000'
  },
  searchInputIconStyle: {
    padding: 5,
  },
  searchInputStyle: {
    flex: 1,
    paddingRight: 10,
    textAlign: 'left',
    color:'#000000'
  },
  textWhite:{
    paddingRight: 10,
    textAlign: 'left',
    color:'white'
  }
});

export default RootStackNavigator;

import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import { ScrollView, TouchableOpacity, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/Octicons';

class DrawerContentResident extends Component {
  state = {
    channels: [
      { screen: 'HomeScreenResident', title: 'Home', icon: 'home' },
      { screen: 'QRScreen', title: 'QR Code', icon: 'screen-normal' },
      { screen: 'ResidentScreen', title: 'Biodata', icon: 'person' },
    ],
  };
  navigateToScreen = route => () => {
    const navigate = NavigationActions.navigate({
      routeName: route,
    });
    this.props.navigation.dispatch(navigate);
  };
  renderChannelButtons() {
    return this.state.channels.map(({ screen, title, icon }) => (
      <TouchableOpacity
        key={screen + title}
        onPress={this.navigateToScreen(screen)}
      >
        <View style={{ flexDirection: 'row' }}>
          <Icon
            name={icon}
            size={20}
            color="black"
            style={{ margin: 15, width: 20 }}
          />
          <Text style={{ color: 'black', marginTop: 17 }}>{title}</Text>
        </View>
      </TouchableOpacity>
    ));
  }
  render() {
    return (
      <View style={styles.containerStyle}>
        <ScrollView>{this.renderChannelButtons()}</ScrollView>
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
};

export default DrawerContentResident;

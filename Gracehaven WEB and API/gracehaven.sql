-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.38-MariaDB - Source distribution
-- Server OS:                    Linux
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for gracehaven
CREATE DATABASE IF NOT EXISTS `gracehaven` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `gracehaven`;

-- Dumping structure for table gracehaven.attendance_resident
CREATE TABLE IF NOT EXISTS `attendance_resident` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_class_staff` int(11) DEFAULT NULL,
  `id_scs` int(11) DEFAULT NULL COMMENT 'relation with schedule_class_staff',
  `id_resident` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `status` enum('present','not present') DEFAULT 'present',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.attendance_resident: 6 rows
/*!40000 ALTER TABLE `attendance_resident` DISABLE KEYS */;
INSERT INTO `attendance_resident` (`id`, `id_class_staff`, `id_scs`, `id_resident`, `date`, `time`, `status`) VALUES
	(33, 1, 3, 2, '2019-04-30', '14:11:00', 'not present'),
	(32, 1, 3, 1, '2019-04-30', '14:11:00', 'not present'),
	(31, 1, 1, 3, '2019-04-30', '14:09:00', 'not present'),
	(30, 1, 1, 2, '2019-04-30', '14:08:00', 'not present'),
	(29, 1, 1, 1, '2019-04-30', '14:08:00', 'present'),
	(28, 2, 2, 1, '2019-04-30', '14:01:00', 'not present');
/*!40000 ALTER TABLE `attendance_resident` ENABLE KEYS */;

-- Dumping structure for table gracehaven.category_routine
CREATE TABLE IF NOT EXISTS `category_routine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.category_routine: 5 rows
/*!40000 ALTER TABLE `category_routine` DISABLE KEYS */;
INSERT INTO `category_routine` (`id`, `name`) VALUES
	(1, 'Good Morning!'),
	(2, 'Study time!'),
	(3, 'Good afternoon!'),
	(4, 'Evening!'),
	(5, 'Good night!');
/*!40000 ALTER TABLE `category_routine` ENABLE KEYS */;

-- Dumping structure for table gracehaven.class
CREATE TABLE IF NOT EXISTS `class` (
  `class_id` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`class_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.class: 5 rows
/*!40000 ALTER TABLE `class` DISABLE KEYS */;
INSERT INTO `class` (`class_id`, `class_name`) VALUES
	(1, 'Bless class'),
	(2, 'Holy Blessing'),
	(3, 'Miracle'),
	(4, 'new class'),
	(5, 'Spirit');
/*!40000 ALTER TABLE `class` ENABLE KEYS */;

-- Dumping structure for table gracehaven.class_resident
CREATE TABLE IF NOT EXISTS `class_resident` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_class_staff` int(11) DEFAULT NULL,
  `id_resident` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.class_resident: 11 rows
/*!40000 ALTER TABLE `class_resident` DISABLE KEYS */;
INSERT INTO `class_resident` (`id`, `id_class_staff`, `id_resident`) VALUES
	(11, 5, 2),
	(2, 1, 2),
	(3, 2, 1),
	(10, 5, 1),
	(9, 2, 2),
	(7, 1, 3),
	(8, 1, 1),
	(12, 5, 3),
	(13, 4, 1),
	(14, 4, 2),
	(15, 4, 3);
/*!40000 ALTER TABLE `class_resident` ENABLE KEYS */;

-- Dumping structure for table gracehaven.class_staff
CREATE TABLE IF NOT EXISTS `class_staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_class` int(11) DEFAULT NULL,
  `id_staff` int(11) DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `clock` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '1' COMMENT '1:Active;0;Disable',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.class_staff: 5 rows
/*!40000 ALTER TABLE `class_staff` DISABLE KEYS */;
INSERT INTO `class_staff` (`id`, `id_class`, `id_staff`, `day`, `clock`, `status`) VALUES
	(1, 1, 1, 4, '12.00-14.00', 1),
	(2, 2, 1, 2, '11.00-12.00', 1),
	(5, 1, 1, 4, '10:00-12:00', 1),
	(4, 1, 1, 4, '10:00-11:00', 1),
	(6, 2, 2, 7, '12:00', 1);
/*!40000 ALTER TABLE `class_staff` ENABLE KEYS */;

-- Dumping structure for table gracehaven.day
CREATE TABLE IF NOT EXISTS `day` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.day: 7 rows
/*!40000 ALTER TABLE `day` DISABLE KEYS */;
INSERT INTO `day` (`id`, `day_name`) VALUES
	(1, 'Monday'),
	(2, 'Tuesday'),
	(3, 'Wednesday'),
	(4, 'Thursday'),
	(5, 'Friday'),
	(6, 'Saturday'),
	(7, 'Sunday');
/*!40000 ALTER TABLE `day` ENABLE KEYS */;

-- Dumping structure for table gracehaven.group
CREATE TABLE IF NOT EXISTS `group` (
  `group_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.group: 3 rows
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
INSERT INTO `group` (`group_id`, `group_name`) VALUES
	(1, 'Admin'),
	(2, 'Staff'),
	(3, 'Resident');
/*!40000 ALTER TABLE `group` ENABLE KEYS */;

-- Dumping structure for table gracehaven.login_attempts
CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `ip_address` varchar(200) DEFAULT NULL,
  `time_stamp` datetime DEFAULT NULL,
  `success` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.login_attempts: ~26 rows (approximately)
/*!40000 ALTER TABLE `login_attempts` DISABLE KEYS */;
INSERT INTO `login_attempts` (`id`, `username`, `ip_address`, `time_stamp`, `success`) VALUES
	(19, 'admin', '127.0.0.1', '2019-04-25 10:49:14', '1'),
	(20, 'pras', '127.0.0.1', '2019-04-25 11:12:28', '1'),
	(21, 'pras', '::1', '2019-04-25 11:58:24', '1'),
	(22, 'naka', '::1', '2019-04-25 15:23:29', '0'),
	(23, 'naka', '::1', '2019-04-25 15:23:34', '0'),
	(24, 'nakamura', '::1', '2019-04-25 15:23:43', '1'),
	(25, 'pras', '::1', '2019-04-25 15:46:35', '1'),
	(26, 'pras', '::1', '2019-04-26 09:13:01', '1'),
	(27, 'pras', '::1', '2019-04-26 09:19:05', '1'),
	(28, 'admin', '::1', '2019-04-27 07:50:07', '1'),
	(29, 'pras', '::1', '2019-04-27 07:50:27', '1'),
	(30, 'pras', '::1', '2019-04-29 11:14:24', '1'),
	(31, 'pras', '::1', '2019-04-29 13:24:25', '1'),
	(32, '', '::1', '2019-04-29 15:38:56', '0'),
	(33, 'pras', '::1', '2019-04-29 15:39:07', '1'),
	(34, 'pras', '::1', '2019-04-30 11:24:41', '1'),
	(35, 'admin', '::1', '2019-04-30 14:29:55', '1'),
	(36, 'pras', '::1', '2019-04-30 14:30:16', '1'),
	(37, 'admin', '::1', '2019-04-30 14:35:31', '1'),
	(38, 'admin', '::1', '2019-04-30 14:42:31', '1'),
	(39, 'admin', '::1', '2019-04-30 14:44:16', '1'),
	(40, 'arga', '::1', '2019-04-30 14:44:48', '0'),
	(41, 'admin', '::1', '2019-04-30 14:44:54', '1'),
	(42, 'pras', '::1', '2019-04-30 14:51:48', '1'),
	(43, 'pras', '::1', '2019-04-30 15:08:49', '1'),
	(44, 'pras', '::1', '2019-04-30 15:08:57', '1'),
	(45, 'naka', '::1', '2019-04-30 16:04:33', '1'),
	(46, 'pras', '::1', '2019-04-30 21:58:32', '1');
/*!40000 ALTER TABLE `login_attempts` ENABLE KEYS */;

-- Dumping structure for table gracehaven.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `group` varchar(255) NOT NULL DEFAULT 'Unknown',
  `order` int(11) NOT NULL DEFAULT '1',
  `statuspermission` varchar(1) NOT NULL DEFAULT '1' COMMENT '1:Active;0:Disable',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- Dumping data for table gracehaven.permissions: ~32 rows (approximately)
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `key`, `name`, `description`, `group`, `order`, `statuspermission`) VALUES
	(1, 'groups', 'Group', 'Manage Group', 'group', 1, '1'),
	(2, 'group-add', 'Group Add', 'Manage Group Add', 'group', 2, '1'),
	(3, 'group-edit', 'Group Edit', 'Manage Group Edit', 'group', 3, '1'),
	(4, 'group-delete', 'Group Delete', 'Manage Group Delete', 'group', 4, '1'),
	(5, 'group-detail', 'Group Detail', 'Manage Group Detail', 'group', 5, '1'),
	(6, 'staffs', 'Staff', 'Manage Staff', 'staff', 1, '1'),
	(7, 'staff-add', 'Staff Add', 'Manage Staff Add', 'staff', 2, '1'),
	(8, 'staff-edit', 'Staff Delete', 'Manage Staff Edit', 'staff', 3, '1'),
	(9, 'staff-delete', 'Staff Delete', 'Manage Staff Delete', 'staff', 4, '1'),
	(10, 'class', 'Class', 'Manage Classs', 'class', 1, '1'),
	(11, 'class-add', 'Class Add', 'Manage Class Add', 'class', 2, '1'),
	(12, 'class-edit', 'Class Edit', 'Manage Class Edit', 'class', 3, '1'),
	(13, 'class-delete', 'Class Delete', 'Manage Class Delete', 'class', 4, '1'),
	(14, 'resident', 'Resident', 'Manage Resident', 'resident', 1, '1'),
	(15, 'resident-add', 'Resident Add', 'Manage Resident Add', 'resident', 2, '1'),
	(16, 'resident-edit', 'Resident Edit', 'Manage Resident Edit', 'resident', 3, '1'),
	(17, 'resident-delete', 'Resident Delete', 'Manage Resident Delete', 'resident', 4, '1'),
	(18, 'modules', 'Modules', 'Manage Module', 'module', 1, '1'),
	(19, 'module-add', 'Modules Add', 'Manage Module Add', 'module', 2, '1'),
	(20, 'module-edit', 'Modules Edit', 'Manage Module Edit', 'module', 3, '1'),
	(21, 'module-detail', 'Modules detail', 'Manage Module Detail', 'module', 4, '1'),
	(22, 'classstaff', 'Class Staff', 'Manage Class Staff', 'classstaff', 1, '1'),
	(23, 'classstaff-add', 'Class Staff Add', 'Manage Class Staff Add', 'classstaff', 2, '1'),
	(24, 'classstaff-edit', 'Class Staff Edit', 'Manage Class Staff Edit', 'classstaff', 3, '1'),
	(25, 'classstaff-delete', 'Class Staff Delete', 'Manage Class Staff Delete', 'classstaff', 4, '1'),
	(26, 'classstaff-detail', 'Class Staff Detail', 'Manage Class Staff Detail', 'classstaff', 5, '1'),
	(27, 'classstaff-schedule', 'Class Staff Schedule', 'Manage Class Staff Schedule', 'classstaff', 6, '1'),
	(28, 'schedule-add', 'Schedule Add', 'Manage Schedule Add', 'schedule', 1, '1'),
	(29, 'schedule-edit', 'Schedule Edit', 'Manage Schedule Edit', 'schedule', 2, '1'),
	(30, 'schedule-delete', 'Schedule Delete', 'Manage Schedule Delete', 'schedule', 3, '1'),
	(31, 'schedule-attendance', 'Schedule Attendance', 'Manage Schedule Attendance', 'schedule', 4, '1'),
	(32, 'attendance-add', 'Attendance Add', 'Manage Attendance Add', 'attendance', 1, '1');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Dumping structure for table gracehaven.resident
CREATE TABLE IF NOT EXISTS `resident` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `resident_id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.resident: 3 rows
/*!40000 ALTER TABLE `resident` DISABLE KEYS */;
INSERT INTO `resident` (`id`, `resident_id`, `name`) VALUES
	(1, 1, 'Arga'),
	(2, 2, 'Aris'),
	(3, 3, 'Andre');
/*!40000 ALTER TABLE `resident` ENABLE KEYS */;

-- Dumping structure for table gracehaven.role_permissions
CREATE TABLE IF NOT EXISTS `role_permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `grup_id` int(11) unsigned NOT NULL,
  `permission_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- Dumping data for table gracehaven.role_permissions: 32 rows
/*!40000 ALTER TABLE `role_permissions` DISABLE KEYS */;
INSERT INTO `role_permissions` (`id`, `grup_id`, `permission_id`) VALUES
	(20, 1, 1),
	(21, 1, 2),
	(22, 1, 3),
	(23, 1, 4),
	(24, 1, 5),
	(25, 1, 6),
	(26, 1, 7),
	(27, 1, 8),
	(28, 1, 9),
	(29, 1, 10),
	(30, 1, 11),
	(31, 1, 12),
	(32, 1, 13),
	(33, 1, 14),
	(34, 1, 15),
	(35, 1, 16),
	(36, 1, 17),
	(37, 1, 18),
	(38, 1, 19),
	(39, 1, 20),
	(40, 1, 21),
	(41, 1, 22),
	(42, 1, 23),
	(43, 1, 24),
	(44, 1, 25),
	(45, 1, 26),
	(46, 1, 27),
	(47, 1, 28),
	(48, 1, 29),
	(49, 1, 30),
	(50, 1, 31),
	(51, 1, 32);
/*!40000 ALTER TABLE `role_permissions` ENABLE KEYS */;

-- Dumping structure for table gracehaven.routine
CREATE TABLE IF NOT EXISTS `routine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_routine_id` int(11) NOT NULL,
  `quantities` int(11) DEFAULT NULL,
  `expectations` varchar(100) DEFAULT NULL,
  `routine_name` text,
  `default_star` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.routine: 13 rows
/*!40000 ALTER TABLE `routine` DISABLE KEYS */;
INSERT INTO `routine` (`id`, `category_routine_id`, `quantities`, `expectations`, `routine_name`, `default_star`) VALUES
	(1, 1, NULL, NULL, 'a. Wake up on time by yourself ', 5),
	(2, 1, NULL, NULL, 'b. Greetings "Good Morning/Bye Aunty/Uncle" ', 5),
	(3, 1, NULL, NULL, 'c. Made my bed (fold blanket)', 5),
	(4, 2, NULL, NULL, 'a. Attend school/activities with no complaints received OR Homework/ Revise time >60 mins (non-schoolers)', 5),
	(5, 3, NULL, NULL, 'a. Come back on time', 5),
	(6, 3, NULL, NULL, 'b. Greetings "Good Afternoon Aunty/Uncle"   ', 5),
	(7, 3, NULL, NULL, 'c. Sign Blue Book, Surrender phone  ', 5),
	(8, 4, NULL, NULL, 'a. Complete my duties properly', 5),
	(9, 4, NULL, NULL, 'b. Personal hygience (shower)', 5),
	(10, 4, NULL, NULL, 'c. Surrender phone or exit privilege room on time  ', 5),
	(11, 5, NULL, NULL, 'a. Prepare for school (uniform)', 5),
	(12, 5, NULL, NULL, 'b. Be in room/bed by 9PM', 5),
	(13, 5, NULL, NULL, 'c. Greetings "Good Night Aunty/Uncle"', 5);
/*!40000 ALTER TABLE `routine` ENABLE KEYS */;

-- Dumping structure for table gracehaven.routine_record
CREATE TABLE IF NOT EXISTS `routine_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `routine_id` int(11) DEFAULT NULL,
  `resident_id` int(11) DEFAULT NULL,
  `day` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `description` text,
  `star` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.routine_record: 4 rows
/*!40000 ALTER TABLE `routine_record` DISABLE KEYS */;
INSERT INTO `routine_record` (`id`, `routine_id`, `resident_id`, `day`, `date`, `description`, `star`, `staff_id`) VALUES
	(6, 1, 1, '2', '2019-04-23', 'good', 3, 1),
	(7, 1, 3, '2', '2019-04-23', 'good', 5, 1),
	(8, 2, 1, '3', '2019-04-24', 'good', 5, 1),
	(11, 0, 1, '2', '2019-04-30', '3', 5, 1);
/*!40000 ALTER TABLE `routine_record` ENABLE KEYS */;

-- Dumping structure for table gracehaven.schedule_class_staff
CREATE TABLE IF NOT EXISTS `schedule_class_staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_class_staff` int(11) DEFAULT NULL,
  `schedule` text,
  `date` date DEFAULT NULL,
  `note` text,
  `status` int(11) DEFAULT '1' COMMENT '1;Active;0:Close',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.schedule_class_staff: 5 rows
/*!40000 ALTER TABLE `schedule_class_staff` DISABLE KEYS */;
INSERT INTO `schedule_class_staff` (`id`, `id_class_staff`, `schedule`, `date`, `note`, `status`) VALUES
	(1, 1, 'Meeting 12', '2019-04-08', 'Note Meeting 1', 1),
	(2, 2, 'Meeting 2', '2019-04-11', 'Note Meeting 2', 1),
	(3, 1, 'Meeting 3', '2019-04-11', 'Note Meeting 3', 1),
	(5, 5, 'Meeting 1', '2019-04-29', 'Introduction', 1),
	(7, 1, 'Meeting 4', '2019-04-30', 'Meeting 4: Outbond', 1);
/*!40000 ALTER TABLE `schedule_class_staff` ENABLE KEYS */;

-- Dumping structure for table gracehaven.staff
CREATE TABLE IF NOT EXISTS `staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `group_id` int(11) NOT NULL,
  `staff` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.staff: 4 rows
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
INSERT INTO `staff` (`id`, `name`, `group_id`, `staff`) VALUES
	(1, 'pras', 1, 1),
	(2, 'Nakamura Setijo', 2, 1),
	(3, 'admin', 1, 1),
	(4, 'deva', 2, 1);
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;

-- Dumping structure for table gracehaven.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT 'Relation With Staff and Resident',
  `name` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `type` int(11) NOT NULL COMMENT '1:Admin;2:Staff:3:Resident',
  `token` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table gracehaven.users: 9 rows
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `user_id`, `name`, `username`, `password`, `type`, `token`) VALUES
	(1, 1, 'pras', 'pras', 'pras', 1, NULL),
	(2, 2, 'Nakamura Setijo', 'naka', 'naka', 2, NULL),
	(3, 1, 'Arga', 'arga', 'arga', 3, NULL),
	(4, 2, 'aris', 'aris', 'aris', 3, NULL),
	(5, 3, 'Andre', 'andre', 'andre', 3, NULL),
	(6, 3, 'admin', 'admin', 'admin', 1, NULL),
	(7, 4, 'deva', 'deva', 'deva', 2, NULL),
	(8, 5, 'toni', 'toni', 'toni', 2, NULL),
	(9, 4, 'Spirits', 'spirit', 'ddd', 3, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

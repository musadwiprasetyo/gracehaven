<?php 

class Template {
	protected $theme="blessing";
	protected $_ci;
	
	function __construct() 
	{
		$this->_ci=&get_instance();	
	}
	function safe_encode($string) {
	
        $data = str_replace(array('+','/','='),array('-','_',''),$string);
        return $data;
    }
	
	function display($template, $data=null)
	{		

		$data['_header']=$this->_ci->load->view($this->theme.'/header', $data, TRUE);
		$data['_sidebar']=$this->_ci->load->view($this->theme.'/sidebar', $data, TRUE);
		$data['_konten']=$this->_ci->load->view($template, $data, TRUE);
		$data['_footer']=$this->_ci->load->view($this->theme.'/footer', $data, TRUE);
		$this->_ci->load->view($this->theme.'/index', $data);
	}
	
	function custom_table($id,$jenis) {
	$custom_table = array (
						'table_open'			=> '<table style="text-align:left" id="'.$id.'" class="table '.$jenis.'">',

						'thead_open'			=> '<thead style="background-color:#367fa9;color:#fff;">',
						'thead_close'			=> '</thead>',

						'heading_row_start'		=> '<tr>',
						'heading_row_end'		=> '</tr>',
						'heading_cell_start'	=> '<th>',
						'heading_cell_end'		=> '</th>',

						'tbody_open'			=> '<tbody>',
						'tbody_close'			=> '</tbody>',

						'row_start'				=> '<tr>',
						'row_end'				=> '</tr>',
						'cell_start'			=> '<td>',
						'cell_end'				=> '</td>',

						'row_alt_start'			=> '<tr>',
						'row_alt_end'			=> '</tr>',
						'cell_alt_start'		=> '<td>',
						'cell_alt_end'			=> '</td>',

						'table_close'			=> '</table>'
					);
					return $custom_table;
	}
	
	function custom_table_standard($id,$jenis) {
		// jenis : [blank], table-responsive, table-striped, table-condensed, table-bordered
		$custom_table = array (
		'table_open'			=> '<table style="text-align:left" cellspacing="0" width="100%" id="'.$id.'" class="table '.$jenis.'">',
		
		'thead_open'			=> '<thead>',
		'thead_close'			=> '</thead>',
		
		'heading_row_start'		=> '<tr>',
		'heading_row_end'		=> '</tr>',
		'heading_cell_start'	=> '<th>',
		'heading_cell_end'		=> '</th>',
		
		'tbody_open'			=> '<tbody>',
		'tbody_close'			=> '</tbody>',
		
		'row_start'				=> '<tr>',
		'row_end'				=> '</tr>',
		'cell_start'			=> '<td>',
		'cell_end'				=> '</td>',
		
		'row_alt_start'			=> '<tr>',
		'row_alt_end'			=> '</tr>',
		'cell_alt_start'		=> '<td>',
		'cell_alt_end'			=> '</td>',
		
		'table_close'			=> '</table>'
		);
		return $custom_table;
	}
	
	function custom_table_standard_withfoot($id,$jenis) {
		// jenis : [blank], table-responsive, table-striped, table-condensed, table-bordered
		$custom_table = array (
		'table_open'			=> '<table style="text-align:left" cellspacing="0" width="100%" id="'.$id.'" class="table '.$jenis.'">',
		
		'thead_open'			=> '<thead>',
		'thead_close'			=> '</thead>',
		
		'heading_row_start'		=> '<tr>',
		'heading_row_end'		=> '</tr>',
		'heading_cell_start'	=> '<th>',
		'heading_cell_end'		=> '</th>',
		
		'tfoot_open'            => '<tfoot>',
        'footer_row_start'      => '<tr>',
        'footer_row_end'        => '</tr>',
        'footer_cell_start'     => '<th>',
        'footer_cell_end'       => '</th>',
        'tfoot_close'           => '</tfoot>',
		
		'tbody_open'			=> '<tbody>',
		'tbody_close'			=> '</tbody>',
		
		'row_start'				=> '<tr>',
		'row_end'				=> '</tr>',
		'cell_start'			=> '<td>',
		'cell_end'				=> '</td>',
		
		'row_alt_start'			=> '<tr>',
		'row_alt_end'			=> '</tr>',
		'cell_alt_start'		=> '<td>',
		'cell_alt_end'			=> '</td>',
		
		'table_close'			=> '</table>'
		);
		return $custom_table;
	}
	
	function custom_table_print($id,$jenis) {
		// jenis : [blank], table-responsive, table-striped, table-condensed, table-bordered
		$custom_table = array (
		'table_open'			=> '<table style="text-align:left" cellspacing="0" width="100%" id="'.$id.'" class="table '.$jenis.'">',
		
		'thead_open'			=> '<thead style="border-top:1px solid grey;border-bottom:1px dashed grey!important">',
		'thead_close'			=> '</thead>',
		
		'heading_row_start'		=> '<tr>',
		'heading_row_end'		=> '</tr>',
		'heading_cell_start'	=> '<th>',
		'heading_cell_end'		=> '</th>',
		
		'tbody_open'			=> '<tbody style="border-bottom:1px solid grey;border-top:1px dashed grey!important">',
		'tbody_close'			=> '</tbody>',
		
		'row_start'				=> '<tr style="border-bottom:1px dashed grey">',
		'row_end'				=> '</tr>',
		'cell_start'			=> '<td>',
		'cell_end'				=> '</td>',
		
		'row_alt_start'			=> '<tr style="border-bottom:1px dashed grey">',
		'row_alt_end'			=> '</tr>',
		'cell_alt_start'		=> '<td>',
		'cell_alt_end'			=> '</td>',
		
		'table_close'			=> '</table>'
		);
		return $custom_table;
	}

}
?>
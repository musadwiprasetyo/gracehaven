 <header>
    <div class="headerwrapper">
        <div class="header-left">
            <a href="" class="logo">
                <img src="<?php echo base_url();?>assets/backend/images/logo-2.png" alt="" width="150">
            </a>
            <div class="pull-right hidden-sm hidden-md hidden-lg">
                <a href="#" class="menu-collapse">
                    <i class="fa fa-bars"></i>
                </a>
            </div>
        </div>
        
        <div class="header-right">
            <div class="pull-right">     
                <div class="btn-group btn-group-option">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                     <?php echo $this->session->userdata('name');?> <i class="fa fa-caret-down"></i> 
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                      <li><a href="<?php echo base_url();?>profil"><i class="glyphicon glyphicon-user"></i> My Profile</a></li>
                      <li class="divider"></li>
                      <li><a href="<?php echo base_url();?>logout"><i class="glyphicon glyphicon-log-out"></i>Sign Out</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo $judul;?></title>
        <link href="<?php echo base_url();?>assets/backend/css/style.default.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/backend/css/morris.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/backend/css/select2.css" rel="stylesheet" />
        <link href="<?php echo base_url();?>assets/backend/css/style.datatables.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/backend/css/dataTables.responsive.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/backend/css/dataTables.bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/backend/css/toggles.css" rel="stylesheet" />
        <link href="<?php echo base_url();?>assets/backend/css/bootstrap-timepicker.min.css" rel="stylesheet" />
        <link href="<?php echo base_url();?>assets/backend/css/colorpicker.css" rel="stylesheet" />
        <link href="<?php echo base_url();?>assets/backend/css/dropzone.css" rel="stylesheet" />
        <link href="<?php echo base_url();?>assets/backend/css/bootstrap-wysihtml5.css" rel="stylesheet" />
        <link href="<?php echo base_url();?>assets/backend/sweetalert/sweetalert.css" rel="stylesheet" />
        <link rel="shortcut icon" type="image/png" href="<?php echo base_url();?>assets/backend/images/logo.png"/>
        

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="<?php echo base_url();?>assets/backend/js/html5shiv.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <?php echo $_header; ?>
       
        <section>
            <div class="mainwrapper">
                <?php echo $_sidebar; ?>
                <?php echo $_konten; ?>
                <div class="row">
                    
                    <?php echo $_footer;?> 
                </div>
            </div>
        </section>
        <script src="<?php echo base_url();?>assets/backend/js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/modernizr.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/pace.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/jquery.cookies.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/jquery.sparkline.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/morris.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/raphael-2.1.0.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/bootstrap-wizard.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/select2.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/custom.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/dataTables.responsive.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/dt/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/dataTables.bootstrap4.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/jquery-ui-1.10.3.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/jquery.autogrow-textarea.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/jquery.mousewheel.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/toggles.min.html"></script>
        <script src="<?php echo base_url();?>assets/backend/js/bootstrap-timepicker.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/jquery.maskedinput.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/select2.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/colorpicker.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/dropzone.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/wysihtml5-0.3.0.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/bootstrap-wysihtml5.js"></script>
        <script src="<?php echo base_url();?>assets/backend/sweetalert/sweetalert.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                var url = window.location;
                // Will only work if string in href matches with location
                $('#leftsidebar li a[href="' + url + '"]').parents().addClass('active');
                $('#leftsidebar li a[href="' + url + '"]').parents().children('a.menu-toggle.waves-effect.waves-block').addClass('toggled');
                $('#leftsidebar li a[href="' + url + '"]').parents().children('ul.ml-menu.active').show();
                $('#leftsidebar li a[href="' + url + '"]').addClass('bold');

                // Will also work for relative and absolute hrefs
                $('#leftsidebar li a').filter(function() {
                    return this.href == url;
                }).parent().parent().parent().addClass('active');
            });
        </script>

    </body>
</html>
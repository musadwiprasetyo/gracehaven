 <div id="leftsidebar" class="leftpanel">
    <br/>
    <ul class="nav nav-pills nav-stacked" id="menuutama">
        <li class=""><a href="<?php echo base_url();?>home"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
        <?php if ($this->session->userdata('isstaff')=='1') : ?>
        <li class=""><a href="<?php echo base_url();?>myschedule"><i class="fa fa-graduation-cap"></i> <span>Schedule Class</span></a></li>
        <li class=""><a href="<?php echo base_url();?>pbis"><i class="fa fa-book"></i> <span>Manage PBIS</span></a></li>
        <?php endif; ?>
        <?php if ($this->acl->has_permission('class')||$this->acl->has_permission('resident')) : ?>
        <li class="parent"><a href="#"><i class="fa fa-list"></i> <span>Master</span></a>
            <ul class="children">
              <?php if ($this->acl->has_permission('class')) : ?>
                <li class=""><a href="<?php echo base_url();?>class">Class</a></li>
              <?php endif; ?>
              <?php if ($this->acl->has_permission('classstaff')) : ?>
                <li class=""><a href="<?php echo base_url();?>classstaff">Schedule Class</a></li>
              <?php endif; ?>
               <?php if ($this->acl->has_permission('resident')) : ?>
                <li class=""><a href="<?php echo base_url();?>resident">Resident</a></li>
              <?php endif; ?>
            </ul>
        </li>
        <?php endif; ?>
        <?php if ($this->acl->has_permission('groups')||$this->acl->has_permission('modules')||$this->acl->has_permission('staffs')) : ?>
        <li class="parent"><a href="#"><i class="fa fa-key"></i> <span>Settings</span></a>
            <ul class="children">
              <?php if ($this->acl->has_permission('groups')) : ?>
                <li class=""><a href="<?php echo base_url();?>groups">Groups</a></li>
              <?php endif; ?>
               <?php if ($this->acl->has_permission('modules')) : ?>
                <li><a href="<?php echo base_url();?>modules">Modules</a></li>
              <?php endif; ?>
               <?php if ($this->acl->has_permission('staffs')) : ?>
                <li><a href="<?php echo base_url();?>staffs">Staffs</a></li>
              <?php endif; ?>
            </ul>
        </li>
        <?php endif; ?>
    </ul>
</div>
 <div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url();?>"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li><a href="#">Master</a></li>
                    <li><a href="<?php echo base_url();?>classstaff">Schedule Class</a></li>
                    <li>Schedule</li>
                </ul>
                <h4>Manage Schedule</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
         <?php $flashpesan = $this->session->flashdata('error'); ?>
          <?php if (!empty($flashpesan)) : ?>
          <div class="alert alert-<?php echo $flashpesan[0]; ?>">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <?php echo $flashpesan[1]; ?>
          </div>

          <?php endif; ?> 
        <div class="panel panel-primary-head">
            <div class="panel-heading">
                <div class="pull-right">
                    <?php if ($this->acl->has_permission('schedule-add')) : ?>
                    <button type="button" class="btn btn-sm mt5 btn-white noborder btn-success" data-toggle="modal" data-target="#modalForm"><i class="fa fa-plus"></i> New Schedule</button>
                    <?php endif; ?>
                   
                    <a href="<?php echo base_url();?>classstaff" class="btn btn-default btn-sm mt5"><i class="fa fa-chevron-left"></i> Back</a>
                  
                </div>
                <h4 class="panel-title"><i class="fa fa-book fa-fw"></i>#<?php echo $detail->classstaffid;?> <?php echo strtoupper($detail->class_name);?></h4>
                <h5 class="panel-title"><i class="fa fa-user fa-fw"></i>#<?php echo $detail->id;?> <?php echo strtoupper($detail->name);?></h5>
            </div>

            <table id="table" class="table table-striped table-bordered responsive">
                <thead>
                    <tr>
                        <th class="text-center" width="1%">No</th>
                        <th width="20%">Schedule</th>
                        <th width="15%">Date</th>
                        <th width="15%">Note</th>
                        <th width="2%">Presence</th>
                        <th class="text-center" width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" tabindex="-1"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Schedule- New</h4>
        </div>
        <form id="formData" class="form-horizontal" method="post">
          <input type="hidden" id="classstaffid" value="<?php echo $detail->classstaffid;?>">
          <input type="hidden" id="scheduleid" value="">
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
                  <label class="col-sm-3 control-label">Schedule <span class="asterisk">*</span></label>
                  <div class="col-sm-9">
                      <input type="text" name="schedule" id="schedule" class="form-control" placeholder="Schedule" required="" value="" />
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-3 control-label">Date <span class="asterisk">*</span></label>
                  <div class="col-sm-9">
                      <input type="date" name="date" id="date" class="form-control" placeholder="Date" required="" value="" />
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-3 control-label">Note <span class="asterisk">*</span></label>
                  <div class="col-sm-9">
                      <textarea name="note" id="note" required="" class="form-control"></textarea>
                  </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>

<script src="<?php echo base_url();?>assets/backend/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
  var save_method; 
  $(document).ready(function(){
       $('#modalForm').on('shown.bs.modal', function () {
          $('#modalForm').focus();
      });
      $('#modalForm').on('hidden.bs.modal', function () {
          $(this).find("input,textarea,checkbox").val('').end();
      });
      table= $('#table').DataTable({
          "processing": true,
          "keys"      : true,
          "serverSide": true,
          "pageLength": 10,
          "dom"       : 'frtp',
          "ajax"      :{
                          "url": "<?php echo base_url('classstaff/schedule/getdata/'.$id)?>",
                          "dataType": "json",
                          "type": "POST",
                          "data": function(d) {
                              
                      }
          },
          "columns": [
            { "data": "no", 
              "orderable" : false, 
            },
            { "data": "schedule"},
            { "data": "date"},
            { "data": "note"},
            { "data": "resident"},
            { "data" : "action",
              "orderable" : false,
              "className" : "text-center",
            },
      
          ],
          responsive: true,
          language: {
            search: "_INPUT_",
            searchPlaceholder: "Searching",
          }
      });
       
  })
  $('#formData').submit(function(e){
      e.preventDefault();
      $.ajax({
        type: 'POST',
        url : "<?php echo base_url('schedule/save')?>",
        data: {
                classstaffid  : $('#modalForm #classstaffid').val(),
                scheduleid    : $('#modalForm #scheduleid').val(),
                schedule      : $('#modalForm #schedule').val(),
                date          : $('#modalForm #date').val(),
                note          : $('#modalForm #note').val(),
        },
        dataType: "json",
        success: function(data){
          $('#modalForm').modal('hide');
          table.ajax.reload(null, true);
            setTimeout(function(){
              table.row(0).deselect();
              table.row(0).select();
          },3000);

           if(data.success){
                swal('Yes',data.message,'success');
                table.ajax.reload();
           }else{
              swal('Ups',data.message,'error');
           }
        },
      });
    });
  
  function UpdateFunction(e,$enc_id){
     $.ajax({
        type: 'POST',
        url : "<?php echo base_url('ClassStaff/getDataJsonSchedule')?>",
        data: {
            enc_id         : $enc_id
        },
        dataType: "json",
        success: function(data){
          console.log(data);
          $('#modalForm').removeData('bs.modal')
          $('#modalForm #scheduleid').val(data.id);
          $('#modalForm #classstaffid').val(data.id_class_staff);
          $('#modalForm #schedule').val(data.schedule);
          $('#modalForm #date').val(data.date);
          $('#modalForm #note').val(data.note);
          $('#modalForm #myModalLabel').text('Schedule-Edit');
          $('#modalForm').modal('show');
        },
        error: function(data){
          console.log(data);
        }
    });
 }

  function DeleteFunction (e,$id){
    <?php if ($this->acl->has_permission('class-delete')) : ?>
         swal({
                  title: "Are You Sure?",
                  text: "Remove Data!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes, Remove!",
                  confirmButtonColor: "#ec6c62",
                  closeOnConfirm: false
              },
              function(){
                 $.ajax({
                    type: 'POST',
                    url : "<?php echo base_url('schedule/delete')?>",
                    data: {
                        id         : $id
                    },
                    dataType: "json",
                    success: function(data){
                      console.log(data);
                       if(data.success){
                            swal('Yes',data.message,'success');
                            table.ajax.reload();
                       }else{
                          swal('Ups',data.message,'error');
                       }
                    },
                    error: function(data){
                      console.log(data);
                    }
                });
              });
    <?php else : ?>
      swal('Ups',"You can't access this module. Please contact Administrator.",'error');
      table.ajax.reload();
    <?php endif; ?>
}

</script>
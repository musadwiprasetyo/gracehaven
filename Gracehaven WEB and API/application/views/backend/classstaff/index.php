 <div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url();?>"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li><a href="#">Master</a></li>
                    <li>Schedule Class</li>
                </ul>
                <h4>Manage Schedule Class</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
         <?php $flashpesan = $this->session->flashdata('error'); ?>
          <?php if (!empty($flashpesan)) : ?>
          <div class="alert alert-<?php echo $flashpesan[0]; ?>">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <?php echo $flashpesan[1]; ?>
          </div>

          <?php endif; ?> 
        <div class="panel panel-primary-head">
            <div class="panel-heading">
                <div class="pull-right">
                  <?php if ($this->acl->has_permission('class-add')) : ?>
                    <button type="button" class="btn btn-sm mt5 btn-white noborder btn-success" data-toggle="modal" data-target="#modalForm"><i class="fa fa-plus"></i> New Schedule Class</button>
                  <?php endif; ?>
                  
                </div>
                <h4 class="panel-title">List Schedule Class</h4>
                <p>Searching, ordering, paging etc goodness will be immediately added to the table.</p>
            </div>

            <table id="table" class="table table-striped table-bordered responsive">
                <thead>
                    <tr>
                        <th class="text-center" width="1%">No</th>
                        <th width="20%">Class</th>
                        <th width="15%">Staff</th>
                        <th width="15%">Time</th>
                        <th width="2%">Resident</th>
                        <th class="text-center" width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" tabindex="-1"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Class- New</h4>
        </div>
        <form id="formData" class="form-horizontal" method="post">
          <input type="hidden" id="classstaffid" value="">
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
                  <label class="col-sm-3 control-label">Class  Name <span class="asterisk">*</span></label>
                  <div class="col-sm-9">
                      <select class="form-control" name="programme" id="programme">
                        <?php foreach ($programme as $key => $class): ?>
                          <option value="<?php echo $class->class_id;?>"><?php echo $class->class_name;?></option>
                        <?php endforeach ?>
                      </select>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-3 control-label">Staff Name <span class="asterisk">*</span></label>
                  <div class="col-sm-9">
                      <select class="form-control" name="staff" id="staff">
                        <?php foreach ($staffs as $key => $staff): ?>
                          <option value="<?php echo $staff->id;?>"><?php echo $staff->name;?></option>
                        <?php endforeach ?>
                      </select>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-3 control-label">Day <span class="asterisk">*</span></label>
                  <div class="col-sm-9">
                      <select class="form-control" name="day" id="day">
                        <?php foreach ($days as $key => $day): ?>
                          <option value="<?php echo $day->id;?>"><?php echo $day->day_name;?></option>
                        <?php endforeach ?>
                      </select>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-3 control-label">Clock <span class="asterisk">*</span></label>
                  <div class="col-sm-9">
                      <input type="text" name="clock" id="clock" class="form-control" placeholder="12:00 PM" required="" value="" />
                  </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>

<script src="<?php echo base_url();?>assets/backend/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
  var save_method; 
  $(document).ready(function(){
       $('#modalForm').on('shown.bs.modal', function () {
          $('#modalForm').focus();
      });
      $('#modalForm').on('hidden.bs.modal', function () {
          $(this).find("input,textarea,checkbox").val('').end();
      });
      table= $('#table').DataTable({
          "processing": true,
          "keys"      : true,
          "serverSide": true,
          "pageLength": 10,
          "dom"       : 'frtp',
          "ajax"      :{
                          "url": "<?php echo base_url('classstaff/getdata')?>",
                          "dataType": "json",
                          "type": "POST",
                          "data": function(d) {
                              
                      }
          },
          "columns": [
            { "data": "no", 
              "orderable" : false, 
            },
            { "data": "name"},
            { "data": "staff"},
            { "data": "time"},
            { "data": "resident"},
            { "data" : "action",
              "orderable" : false,
              "className" : "text-center",
            },
      
          ],
          responsive: true,
          language: {
            search: "_INPUT_",
            searchPlaceholder: "Searching",
          }
      });
       
  })
  $('#formData').submit(function(e){
      e.preventDefault();
      $.ajax({
        type: 'POST',
        url : "<?php echo base_url('classstaff/save')?>",
        data: {
                classstaffid    : $('#modalForm #classstaffid').val(),
                clock           : $('#modalForm #clock').val(),
                programme       : $('#modalForm #programme').val(),
                staff           : $('#modalForm #staff').val(),
                day             : $('#modalForm #day').val(),
        },
        dataType: "json",
        success: function(data){
          $('#modalForm').modal('hide');
          table.ajax.reload(null, true);
            setTimeout(function(){
              table.row(0).deselect();
              table.row(0).select();
          },3000);

           if(data.success){
                swal('Yes',data.message,'success');
                table.ajax.reload();
           }else{
              swal('Ups',data.message,'error');
           }
        },
      });
    });
  
  function UpdateFunction(e,$enc_id){
     $.ajax({
        type: 'POST',
        url : "<?php echo base_url('ClassStaff/getDataJson')?>",
        data: {
            enc_id         : $enc_id
        },
        dataType: "json",
        success: function(data){
          console.log(data);
          $('#modalForm').removeData('bs.modal')
          $('#modalForm #classstaffid').val(data.id);
          $('#modalForm #programme').val(data.id_class).prop('selected',true);
          $('#modalForm #staff').val(data.id_staff).prop('selected',true);
          $('#modalForm #day').val(data.day).prop('selected',true);
          $('#modalForm #clock').val(data.clock);
          $('#modalForm #myModalLabel').text('Class Staff-Edit');
          $('#modalForm').modal('show');
        },
        error: function(data){
          console.log(data);
        }
    });
 }

  function DeleteFunction (e,$id){
    <?php if ($this->acl->has_permission('classstaff-delete')) : ?>
         swal({
                  title: "Are You Sure?",
                  text: "Remove Data!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes, Remove!",
                  confirmButtonColor: "#ec6c62",
                  closeOnConfirm: false
              },
              function(){
                 $.ajax({
                    type: 'POST',
                    url : "<?php echo base_url('classstaff/delete')?>",
                    data: {
                        id         : $id
                    },
                    dataType: "json",
                    success: function(data){
                      console.log(data);
                       if(data.success){
                            swal('Yes',data.message,'success');
                            table.ajax.reload();
                       }else{
                          swal('Ups',data.message,'error');
                       }
                    },
                    error: function(data){
                      console.log(data);
                    }
                });
              });
    <?php else : ?>
      swal('Ups',"You can't access this module. Please contact Administrator.",'error');
      table.ajax.reload();
    <?php endif; ?>
}

</script>
 <div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url();?>"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li><a href="#">Master</a></li>
                    <li><a href="<?php echo base_url();?>classstaff">Schedule Class</a></li>
                    <li>Schedule</li>
                </ul>
                <h4>Manage Class Resident</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
         <?php $flashpesan = $this->session->flashdata('error'); ?>
          <?php if (!empty($flashpesan)) : ?>
          <div class="alert alert-<?php echo $flashpesan[0]; ?>">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <?php echo $flashpesan[1]; ?>
          </div>

          <?php endif; ?> 
        <div class="panel panel-primary-head">
            <div class="panel-heading">
                <div class="pull-right">
                
                    
                  
                </div>
                <h4 class="panel-title"><i class="fa fa-book fa-fw"></i>#<?php echo $classstaff->classstaffid;?> <?php echo strtoupper($classstaff->class_name);?></h4>
                  <br/>
            </div>
            <br/>
            <table id="example" class="table table-striped table-bordered responsive">
                <thead>
                    <tr>
                        <th class="text-center" width="1%">No</th>
                        <th width="20%">Resident ID</th>
                        <th width="15%">Resident Name</th>
                        <th width="2%">In Class?</th>
                    </tr>
                </thead>

                <tbody>
                  <?php foreach ($resident as $key=> $res) : ?>
                    <tr>
                      <td><?php echo $key+1; ?></td>
                      <td><?php echo $res->resident_id;?></td>
                      <td><?php echo $res->name;?></td>
                      <td><?php if ($res->id_class_staff==NULL) : ?>
                              <input type="checkbox" name="" data-classstaff="<?php echo $classstaff->classstaffid;?>" data-id="<?php echo $res->resident_id?>">
                          <?php else : ?>
                            <input type="checkbox" name="" data-classstaff="<?php echo $classstaff->classstaffid;?>" data-id="<?php echo $res->resident_id?>" checked="">
                          
                          <?php endif; ?>
                      </td>
                    </tr>   
                  <?php endforeach; ?> 
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" tabindex="-1"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Class- New</h4>
        </div>
        <form id="formData" class="form-horizontal" method="post">
          <input type="hidden" id="classid" value="">
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
                  <label class="col-sm-2 control-label">Name <span class="asterisk">*</span></label>
                  <div class="col-sm-10">
                      <input type="text" name="name" id="name" class="form-control" placeholder="Name" required="" value="" />
                  </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>

<script src="<?php echo base_url();?>assets/backend/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
  var save_method; 
  $(document).ready(function(){
       $('#modalForm').on('shown.bs.modal', function () {
          $('#modalForm').focus();
      });
      $('#modalForm').on('hidden.bs.modal', function () {
          $(this).find("input,textarea,checkbox").val('').end();
      });
      $('#example').DataTable({
        "dom": 'frtp'
      });
      table= $('#table').DataTable({
          "processing": true,
          "keys"      : true,
          "serverSide": true,
          "pageLength": 10,
          "dom"       : 'frtp',
          "ajax"      :{
                          "url": "<?php echo base_url('classstaff/schedule/getdata/'.$id)?>",
                          "dataType": "json",
                          "type": "POST",
                          "data": function(d) {
                              
                      }
          },
          "columns": [
            { "data": "no", 
              "orderable" : false, 
            },
            { "data": "schedule"},
            { "data": "date"},
            { "data": "note"},
            { "data": "resident"},
            { "data" : "action",
              "orderable" : false,
              "className" : "text-center",
            },
      
          ],
          responsive: true,
          language: {
            search: "_INPUT_",
            searchPlaceholder: "Searching",
          }
      });
       
  })
  $('#formData').submit(function(e){
      e.preventDefault();
      $.ajax({
        type: 'POST',
        url : "<?php echo base_url('class/save')?>",
        data: {
                classid    : $('#modalForm #classid').val(),
                name       : $('#modalForm #name').val(),
        },
        dataType: "json",
        success: function(data){
          $('#modalForm').modal('hide');
          table.ajax.reload(null, true);
            setTimeout(function(){
              table.row(0).deselect();
              table.row(0).select();
          },3000);

           if(data.success){
                swal('Yes',data.message,'success');
                table.ajax.reload();
           }else{
              swal('Ups',data.message,'error');
           }
        },
      });
    });
  
  function UpdateFunction(e,$enc_id){
     $.ajax({
        type: 'POST',
        url : "<?php echo base_url('ClassController/getDataJson')?>",
        data: {
            enc_id         : $enc_id
        },
        dataType: "json",
        success: function(data){
          console.log(data);
          $('#modalForm').removeData('bs.modal')
          $('#modalForm #classid').val(data.class_id);
          $('#modalForm #name').val(data.class_name);
          $('#modalForm #myModalLabel').text('Class-Edit');
          $('#modalForm').modal('show');
        },
        error: function(data){
          console.log(data);
        }
    });
 }

  function DeleteFunction (e,$id){
    <?php if ($this->acl->has_permission('class-delete')) : ?>
         swal({
                  title: "Are You Sure?",
                  text: "Remove Data!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes, Remove!",
                  confirmButtonColor: "#ec6c62",
                  closeOnConfirm: false
              },
              function(){
                 $.ajax({
                    type: 'POST',
                    url : "<?php echo base_url('class/delete')?>",
                    data: {
                        id         : $id
                    },
                    dataType: "json",
                    success: function(data){
                      console.log(data);
                       if(data.success){
                            swal('Yes',data.message,'success');
                            table.ajax.reload();
                       }else{
                          swal('Ups',data.message,'error');
                       }
                    },
                    error: function(data){
                      console.log(data);
                    }
                });
              });
    <?php else : ?>
      swal('Ups',"You can't access this module. Please contact Administrator.",'error');
      table.ajax.reload();
    <?php endif; ?>
}
  $('input[type="checkbox"]').change(function() {
        if($(this).is(":checked")) { 
            var id = $(this).data('id');
            var classstaff = $(this).data('classstaff');
  
            $.ajax({
                    type: 'POST',
                    url : "<?php echo base_url('residentclass/insert')?>",
                    data: {
                        id : id,
                        classstaff : classstaff
                    },
                    dataType: "json",
                    success: function(data){
                      console.log(data);
                       if(data.success){
                            swal('Yes',data.message,'success');
                        
                       }else{
                          swal('Ups',data.message,'error');
                       }
                    },
                    error: function(data){
                      console.log(data);
                    }
            });
              
        }else{
            var id = $(this).data('id');
            var classstaff = $(this).data('classstaff');
  
            $.ajax({
                    type: 'POST',
                    url : "<?php echo base_url('residentclass/delete')?>",
                    data: {
                        id : id,
                        classstaff : classstaff
                    },
                    dataType: "json",
                    success: function(data){
                      console.log(data);
                      
                       if(data.success){
                            swal('Yes',data.message,'success');
                       }else{
                          swal('Ups',data.message,'error');
                       }
                    },
                    error: function(data){
                      console.log(data);
                    }
            });
              
        }  
  });
  // $('input[type="checkbox"]').on('ifChecked', function(event){
  //   var id = $(this).data('id');
  //   var classstaff = $(this).data('classstaff');
  //   alert('ok');

  //   alert(event.type + id + classstaff);
  //     // saveToDatabase(1,col,id, nim);
  // });
</script>
 <div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url();?>"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li><a href="#">Master</a></li>
                    <li><a href="<?php echo base_url();?>classstaff">Schedule Class</a></li>
                    <li>Schedule</li>
                </ul>
                <h4>Manage Schedule</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
         <?php $flashpesan = $this->session->flashdata('error'); ?>
          <?php if (!empty($flashpesan)) : ?>
          <div class="alert alert-<?php echo $flashpesan[0]; ?>">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <?php echo $flashpesan[1]; ?>
          </div>

          <?php endif; ?> 
        <div class="panel panel-primary-head">
            <div class="panel-heading">
                <div class="pull-right">
                    <?php if ($this->acl->has_permission('attendance-add')) : ?>
                     <button type="button" class="btn btn-white noborder btn-success" data-toggle="modal" data-target="#modalForm"><i class="fa fa-plus"></i> Attendance Taking</button>
                     <?php endif; ?>
                    <a href="<?php echo base_url();?>classstaff/schedule/<?php echo $encrypt_id;?>" class="btn btn-default"><i class="fa fa-chevron-left"></i> Back</a>
                  
                </div>
                <h4 class="panel-title"><i class="fa fa-book fa-fw"></i>#<?php echo $attendance->id;?> <?php echo strtoupper($attendance->schedule);?></h4>
                  <br/>
            </div>
            <br/>
            <table id="example" class="table table-striped table-bordered responsive">
                <thead>
                    <tr>
                        <th class="text-center" width="1%">No</th>
                        <th width="20%">Resident ID</th>
                        <th width="15%">Resident Name</th>
                        <th width="2%">Presence</th>
                    </tr>
                </thead>

                <tbody>
                
                    <?php foreach ($attendanceresident as $key=> $attendancer) : ?>
                        
                            <tr>
                              <td><?php echo $key+1; ?></td>
                              <td><?php echo $attendancer->resident_id;?></td>
                              <td><?php echo $attendancer->name;?></td>
                              <td>
                                <?php if ($attendancer->status=="present") : ?>
                                  <span class="label label-success">Present</span>
                                <?php else : ?>
                                  <span class="label label-danger">Not Present</span>
                                <?php endif;?>
                                </td>
                            </tr>
                      <?php endforeach; ?> 
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" tabindex="-1"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Resident</h4>
        </div>
        <form id="formData" class="form-horizontal" method="post">
          <input type="hidden" id="classid" value="">
          <div class="modal-body">
            <div class="panel-body">
             <table id="exampleresident" class="table table-striped table-bordered responsive">
                <thead>
                    <tr>
                        <th class="text-center" width="1px"><input type="checkbox" id="checkAll"/> </th>
                        <th width="20%">Resident ID</th>
                        <th width="15%">Resident Name</th>
                    </tr>
                </thead>

                <tbody id="tableresident">
                    
                    <?php foreach ($residentattendance as $key=> $attendancer) : ?>
                            <?php if ($attendancer->status ==NULL) : ?>
                              <tr>
                                <td class="text-center" width="1px">
                                  <input type="checkbox" id="cek" name="residentvalue[]" value="<?php echo $attendancer->id_resident;?>"/>
                                </td>
                                <td><?php echo $attendancer->id_resident;?></td>
                                <td><?php echo $attendancer->name;?></td>
                              </tr>
                            <?php endif;?>
                           
                      <?php endforeach; ?> 
                </tbody>
            </table>
            </div>
          </div>
          <div class="modal-footer">
             <?php if ($this->acl->has_permission('attendance-add')) : ?>
              <button type="button" id="insertPresence" class="btn btn-success">Presence</button>
            <button type="button" id="insertNotPresence" class="btn btn-warning">Not Presence</button>
             <?php endif; ?>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>

<script src="<?php echo base_url();?>assets/backend/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
  var save_method; 
  $(document).ready(function(){
       $('#modalForm').on('shown.bs.modal', function () {
          $('#modalForm').focus();
      });
      $('#modalForm').on('hidden.bs.modal', function () {
          $(this).find("input,textarea,checkbox").val('').end();
      });
      $('#example').DataTable({
        "dom": 'frtp'
      });
      $('#exampleresident').DataTable({
        "dom": 'frtp'
      });
      table= $('#table').DataTable({
          "processing": true,
          "keys"      : true,
          "serverSide": true,
          "pageLength": 10,
          "dom"       : 'frtp',
          "ajax"      :{
                          "url": "<?php echo base_url('classstaff/schedule/getdata/'.$id)?>",
                          "dataType": "json",
                          "type": "POST",
                          "data": function(d) {
                              
                      }
          },
          "columns": [
            { "data": "no", 
              "orderable" : false, 
            },
            { "data": "schedule"},
            { "data": "date"},
            { "data": "note"},
            { "data": "resident"},
            { "data" : "action",
              "orderable" : false,
              "className" : "text-center",
            },
      
          ],
          responsive: true,
          language: {
            search: "_INPUT_",
            searchPlaceholder: "Searching",
          }
      });
       
  })
  $("#checkAll").click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
  });
  
  // $("#insertPresence").click(function(){
  //    var checkValues = $('input[name=residentvalue]:checked').map(function()
  //           {
  //               return $(this).val();
  //     }).get();
  //    console.log(checkValues);
  // });
  $('#insertPresence').on('click', function(){
      sync      = true;
      var count = 0;
      var data  = [];
      $('#tableresident').find('input[type="checkbox"]:checked:not(:disabled)').each(function () {
        count += 1;
        data.push($(this).val());
      });
     
      if (count > 0) {
        swal({
          title: "Confirmation",
          text: "Are You Sure?",
          type: "info",
          showCancelButton: true,
          closeOnConfirm: false,
          showLoaderOnConfirm: true
        }, function () {         
          $.ajax({
            type: 'POST',
            url: '<?php echo base_url('attendance/insert')?>',
            data: {
              value:data,
              id_class_staff:<?php echo $attendance->id_class_staff;?>,
              id_scs:<?php echo $attendance->id;?>,
              status:'present'
            },
            success: function(data){
              swal('Success!', 'Success Inserted','success');
              $('#modalForm').modal('hide');
            }
          });
        });
      }else {
        swal('Ups!', 'Not Checked','error');
      }
    });
  $('#insertNotPresence').on('click', function(){
      sync      = true;
      var count = 0;
      var data  = [];
      $('#tableresident').find('input[type="checkbox"]:checked:not(:disabled)').each(function () {
        count += 1;
        data.push($(this).val());
      });
     
      if (count > 0) {
        swal({
          title: "Confirmation",
          text: "Are You Sure?",
          type: "info",
          showCancelButton: true,
          closeOnConfirm: false,
          showLoaderOnConfirm: true
        }, function () {         
          $.ajax({
            type: 'POST',
            url: '<?php echo base_url('attendance/insert')?>',
            data: {
              value:data,
              id_class_staff:<?php echo $attendance->id_class_staff;?>,
              id_scs:<?php echo $attendance->id;?>,
              status:'not present'
            },
            success: function(data){
              swal('Success!', 'Success Inserted','success');
              $('#modalForm').modal('hide');
            }
          });
        });
      }else {
        swal('Ups!', 'Not Checked','error');
      }
    });
  $('#formData').submit(function(e){
      e.preventDefault();
      $.ajax({
        type: 'POST',
        url : "<?php echo base_url('class/save')?>",
        data: {
                classid    : $('#modalForm #classid').val(),
                name       : $('#modalForm #name').val(),
        },
        dataType: "json",
        success: function(data){
          $('#modalForm').modal('hide');
          table.ajax.reload(null, true);
            setTimeout(function(){
              table.row(0).deselect();
              table.row(0).select();
          },3000);

           if(data.success){
                swal('Yes',data.message,'success');
                table.ajax.reload();
           }else{
              swal('Ups',data.message,'error');
               $('#modalForm').hide();
           }
        },
      });
    });
  
  function UpdateFunction(e,$enc_id){
     $.ajax({
        type: 'POST',
        url : "<?php echo base_url('ClassController/getDataJson')?>",
        data: {
            enc_id         : $enc_id
        },
        dataType: "json",
        success: function(data){
          console.log(data);
          $('#modalForm').removeData('bs.modal')
          $('#modalForm #classid').val(data.class_id);
          $('#modalForm #name').val(data.class_name);
          $('#modalForm #myModalLabel').text('Class-Edit');
          $('#modalForm').modal('show');
        },
        error: function(data){
          console.log(data);
        }
    });
 }

  function DeleteFunction (e,$id){
    <?php if ($this->acl->has_permission('class-delete')) : ?>
         swal({
                  title: "Are You Sure?",
                  text: "Remove Data!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes, Remove!",
                  confirmButtonColor: "#ec6c62",
                  closeOnConfirm: false
              },
              function(){
                 $.ajax({
                    type: 'POST',
                    url : "<?php echo base_url('class/delete')?>",
                    data: {
                        id         : $id
                    },
                    dataType: "json",
                    success: function(data){
                      console.log(data);
                       if(data.success){
                            swal('Yes',data.message,'success');
                            table.ajax.reload();
                       }else{
                          swal('Ups',data.message,'error');
                       }
                    },
                    error: function(data){
                      console.log(data);
                    }
                });
              });
    <?php else : ?>
      swal('Ups',"You can't access this module. Please contact Administrator.",'error');
      table.ajax.reload();
    <?php endif; ?>
}

</script>
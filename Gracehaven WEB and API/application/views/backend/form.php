 <div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">
                            <div class="media-body">
                                <ul class="breadcrumb">
                                    <li><a href="#"><i class="glyphicon glyphicon-home"></i></a></li>
                                    <li><a href="#">Forms</a></li>
                                    <li>Form Layouts</li>
                                </ul>
                                <h4>Form Layouts</h4>
                            </div>
                        </div>
                    </div>
                    
                    <div class="contentpanel">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form-horizontal">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Horizontal Form</h4>
                                            <p>Basic form with a class name of <code>.form-horizontal</code>.</p>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Name <span class="asterisk">*</span></label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="name" class="form-control" />
                                                </div>
                                            </div>
                                        
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Email</label>
                                                <div class="col-sm-8">
                                                    <input type="email" name="email" class="form-control" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Password</label>
                                                <div class="col-sm-8">
                                                    <input type="password" name="password" class="form-control" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Address</label>
                                                <div class="col-sm-8">
                                                    <textarea name="address" class="form-control" rows="5"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">File</label>
                                                <div class="col-sm-8">
                                                    <input type="file" name="file" class="form-control" />
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-sm-3 control-label">Select</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control">
                                                        <option value="">Pilih</option>
                                                        <option value="">Value 1</option>
                                                        <option value="">Value 2</option>
                                                    </select>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-sm-3 control-label">Select 2</label>
                                                <div class="col-sm-8">
                                                     <select id="select-basic" data-placeholder="Choose One" class="widthtest">
                                                        <option value="">Choose One</option>
                                                            <option value="AK">Alaska</option>
                                                            <option value="HI">Hawaii</option>
                                                            <option value="CA">California</option>
                                                            <option value="NV">Nevada</option>
                                                            <option value="OR">Oregon</option>
                                                            <option value="WA">Washington</option>
                                                       
                                                            <option value="AZ">Arizona</option>
                                                            <option value="CO">Colorado</option>
                                                            <option value="ID">Idaho</option>
                                                            <option value="MT">Montana</option>
                                                            <option value="NE">Nebraska</option>
                                                            <option value="NM">New Mexico</option>
                                                            <option value="ND">North Dakota</option>
                                                            <option value="UT">Utah</option>
                                                            <option value="WY">Wyoming</option>
                                                       
                                                            <option value="AL">Alabama</option>
                                                            <option value="AR">Arkansas</option>
                                                            <option value="IL">Illinois</option>
                                                            <option value="IA">Iowa</option>
                                                            <option value="KS">Kansas</option>
                                                            <option value="KY">Kentucky</option>
                                                            <option value="LA">Louisiana</option>
                                                            <option value="MN">Minnesota</option>
                                                            <option value="MS">Mississippi</option>
                                                            <option value="MO">Missouri</option>
                                                            <option value="OK">Oklahoma</option>
                                                            <option value="SD">South Dakota</option>
                                                            <option value="TX">Texas</option>
                                                            <option value="TN">Tennessee</option>
                                                            <option value="WI">Wisconsin</option>
                                                       
                                                            <option value="CT">Connecticut</option>
                                                            <option value="DE">Delaware</option>
                                                            <option value="FL">Florida</option>
                                                            <option value="GA">Georgia</option>
                                                            <option value="IN">Indiana</option>
                                                            <option value="ME">Maine</option>
                                                            <option value="MD">Maryland</option>
                                                            <option value="MA">Massachusetts</option>
                                                            <option value="MI">Michigan</option>
                                                            <option value="NH">New Hampshire</option>
                                                            <option value="NJ">New Jersey</option>
                                                            <option value="NY">New York</option>
                                                            <option value="NC">North Carolina</option>
                                                            <option value="OH">Ohio</option>
                                                            <option value="PA">Pennsylvania</option>
                                                            <option value="RI">Rhode Island</option>
                                                            <option value="SC">South Carolina</option>
                                                            <option value="VT">Vermont</option>
                                                            <option value="VA">Virginia</option>
                                                            <option value="WV">West Virginia</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Date</label>
                                                <div class="col-sm-8">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Gender <span class="asterisk">*</span></label>
                                                <div class="col-sm-8">
                                                    <div class="rdio rdio-primary">
                                                        <input type="radio" id="male" value="m" name="gender" required="">
                                                        <label for="male">Male</label>
                                                    </div>
                                                    <div class="rdio rdio-primary">
                                                        <input type="radio" value="f" id="female" name="gender">
                                                        <label for="female">Female</label>
                                                    </div>
                                                    <label id="genderError" class="error" for="gender"></label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Interest <span class="asterisk">*</span></label>
                                                <div class="col-sm-8">
                                                    <div class="ckbox ckbox-primary">
                                                        <input type="checkbox" id="int_website" value="m" name="int[]" required="">
                                                        <label for="int_website">Website</label>
                                                    </div>
                                                    
                                                    <div class="ckbox ckbox-primary">
                                                        <input type="checkbox" value="f" id="int_software" name="int[]">
                                                        <label for="int_software">Software</label>
                                                    </div>
                                                
                                                    <div class="ckbox ckbox-primary">
                                                        <input type="checkbox" value="f" id="int_mobile" name="int[]">
                                                        <label for="int_mobile">Mobile</label>
                                                    </div>
                                                    <label id="intError" class="error" for="int[]"></label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Content <span class="asterisk">*</span></label>
                                                <div class="col-sm-8">
                                                    <textarea id="wysiwyg" placeholder="Enter text here..." class="form-control" rows="10"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <button class="btn btn-primary mr5">Submit</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <script src="<?php echo base_url();?>assets/backend/js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/bootstrap-timepicker.min.js"></script>
        <script type="text/javascript">

          $(document).ready(function(){
               $('#datepicker').datepicker();
               $("#select-basic, #select-multi").select2();  
          })
        </script>
<script>
           
        </script>
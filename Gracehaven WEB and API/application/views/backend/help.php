<div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="pageicon pull-left">
                <i class="fa fa-laptop"></i>
            </div>
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url();?>home"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li>Help</li>
                </ul>
                <h4>Help</h4>
            </div>
        </div>
    </div>
    
    <div class="contentpanel">
        
        <h5 class="lg-title">Tutorial</h5>
        <p>By default, all the <code>.panel</code> does is apply some basic border and padding to contain some content.</p>
        <div class="panel panel-default">
            <div class="panel-body">
                I am the default panel. I have no header and footer
            </div>
        </div>
    </div>
</div>
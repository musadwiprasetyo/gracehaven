 <div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url();?>home"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li><a href="#">Settings</a></li>
                    <li>Roles</li>
                </ul>
                <h4>Manage Roles</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-primary-head">
            <div class="panel-heading">
                <div class="pull-right">
                  <div class="btn-group">
                      <a href="<?php echo base_url();?>role/add" class="btn btn-sm mt5 btn-white noborder btn-success"><i class="fa fa-plus"></i> New Data</a>
                  </div>
                </div>
                <h4 class="panel-title">Basic Configuration</h4>
                <p>Searching, ordering, paging etc goodness will be immediately added to the table, as shown in this example.</p>

            </div>

            <table id="table" class="table table-striped table-bordered responsive">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 1px;">No</th>
                        <th width="30%">Name</th>
                        <th width="45%">Description</th>
                        <th class="text-center" style="width: 100px;">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
  var save_method; 
  $(document).ready(function(){
      table= $('#table').DataTable({
          "processing": true,
          "keys"      : true,
          "serverSide": true,
          "pageLength": 10,
          "dom"       : 'frtp',
          "ajax"      :{
                          "url": "<?php echo base_url('role/getdata')?>",
                          "dataType": "json",
                          "type": "POST",
                          "data": function(d) {
                              
                      }
          },
          "columns": [
            { "data": "no", 
              "orderable" : false, 
            },
            { "data": "name"},
            { "data": "description"},
           
            { "data" : "action",
              "orderable" : false,
              "className" : "text-center",
            },
      
          ],
          responsive: true,
          language: {
            search: "_INPUT_",
            searchPlaceholder: "Searching",
          }
      });
  })
</script>
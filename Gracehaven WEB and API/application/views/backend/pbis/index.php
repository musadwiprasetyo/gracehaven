 <div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url();?>"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li>PBIS</li>
                </ul>
                <h4>Manage PBIS</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
         <?php $flashpesan = $this->session->flashdata('error'); ?>
          <?php if (!empty($flashpesan)) : ?>
          <div class="alert alert-<?php echo $flashpesan[0]; ?>">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <?php echo $flashpesan[1]; ?>
          </div>

          <?php endif; ?> 
        <div class="panel panel-primary-head">
            <div class="panel-heading">
                <div class="pull-right">
                  <?php if ($this->acl->has_permission('class-add')) : ?>
                    <button type="button" class="btn btn-sm mt5 btn-white noborder btn-success" data-toggle="modal" data-target="#modalForm"><i class="fa fa-plus"></i> New PBIS</button>
                  <?php endif; ?>
                  
                </div>
                <h4 class="panel-title">List PBIS</h4>
                <p>Searching, ordering, paging etc goodness will be immediately added to the table.</p>
            </div>

            <table id="table" class="table table-striped table-bordered responsive">
                <thead>
                    <tr>
                        <th class="text-center" width="1%">No</th>
                        <th width="8%">Category</th>
                        <th width="20%">Sub Category</th>
                        <th width="8%">Resident</th>
                        <th width="8%">Time</th>
                        <th width="2%">Point</th>
                        <th width="2%">Staff</th>
                        <th class="text-center" width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" tabindex="-1"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">PBIS- New</h4>
        </div>
        <form id="formData" class="form-horizontal" method="post">
          <input type="hidden" id="id" value="">
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
                  <label class="col-sm-4 control-label">Category Routine <span class="asterisk">*</span></label>
                  <div class="col-sm-8">
                      <select class="form-control" name="categoryroutine" id="categoryroutine">
                        <option value="">Select Category</option>
                        <?php foreach ($categoryroutine as $key => $category): ?>
                          <option value="<?php echo $category->id;?>"><?php echo $category->name;?></option>
                        <?php endforeach ?>
                      </select>
                  </div>
              </div>
               <div class="form-group">
                  <label class="col-sm-4 control-label">Sub Routine <span class="asterisk">*</span></label>
                  <div class="col-sm-8">
                      <select class="form-control" name="subroutine" id="subroutine">
                       
                      </select>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-4 control-label">Resident <span class="asterisk">*</span></label>
                  <div class="col-sm-8">
                      <select class="form-control" name="resident" id="resident">
                        <?php foreach ($residents as $key => $resident): ?>
                          <option value="<?php echo $resident->resident_id;?>"><?php echo $resident->name;?></option>
                        <?php endforeach ?>
                      </select>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-4 control-label">Date <span class="asterisk">*</span></label>
                  <div class="col-sm-8">
                      <input type="date" name="date" id="date" class="form-control" placeholder="12:00 PM" required="" value="" />
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-4 control-label">Note <span class="asterisk">*</span></label>
                  <div class="col-sm-8">
                      <textarea class="form-control" name="note" id="note"></textarea>
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-4 control-label">Star <span class="asterisk">*</span></label>
                  <div class="col-sm-8">
                      <select class="form-control" name="star" id="star">
                        <?php foreach ($star as $key => $s): ?>
                          <option value="<?php echo $s;?>"><?php echo $s;?></option>
                        <?php endforeach ?>
                      </select>
                  </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>

<script src="<?php echo base_url();?>assets/backend/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
  var save_method; 
  $(document).ready(function(){
       $('#modalForm').on('shown.bs.modal', function () {
          $('#modalForm').focus();
      });
      $('#modalForm').on('hidden.bs.modal', function () {
          $(this).find("input,textarea,checkbox").val('').end();
      });
      table= $('#table').DataTable({
          "processing": true,
          "keys"      : true,
          "serverSide": true,
          "pageLength": 10,
          "dom"       : 'frtp',
          "ajax"      :{
                          "url": "<?php echo base_url('pbis/getdata')?>",
                          "dataType": "json",
                          "type": "POST",
                          "data": function(d) {
                              
                      }
          },
          "columns": [
            { "data": "no", 
              "orderable" : false, 
            },
            { "data": "catname"},
            { "data": "routine_name"},
            { "data": "name"},
            { "data": "time"},
            { "data": "point"},
            { "data": "staff"},
            { "data" : "action",
              "orderable" : false,
              "className" : "text-center",
            },
          ],
          responsive: true,
          language: {
            search: "_INPUT_",
            searchPlaceholder: "Searching",
          }
      });
     $('#formData').submit(function(e){
      e.preventDefault();
      var category = $('#modalForm #categoryroutine').val();
      var subroutine = $('#modalForm #subroutine').val();
      var resident = $('#modalForm #resident').val();
      var date = $('#modalForm #date').val();
      var note = $('#modalForm #note').val();
      var star = $('#modalForm #star').val();
      var id   = $('#modalForm #id').val();
      $.ajax({
        type: 'POST',
        url : "<?php echo base_url('pbis/save')?>",
        data: {
                id              : id,
                routine_id      : subroutine,
                resident_id     : resident,
                date            : date,
                description     : note,
                star            : star,
        },
        dataType: "json",
        success: function(data){
          $('#modalForm').modal('hide');
          table.ajax.reload(null, true);
            setTimeout(function(){
              table.row(0).deselect();
              table.row(0).select();
          },3000);

           if(data.success){
                swal('Yes',data.message,'success');
                table.ajax.reload();
           }else{
              swal('Ups',data.message,'error');
           }
        },
      });
    });

  });
   $('#categoryroutine').on('change', function(){
        var ids = $('#categoryroutine').val();
        $.ajax({
          type : 'GET', 
          url  : '<?php echo base_url('pbis/getroutine/')?>'+ids, 
          success : function(option){
            console.log(option);
            $('#subroutine').html(option); 
          }
        }); 
  }); 
  function UpdateFunction(e,$enc_id){
     $.ajax({
        type: 'POST',
        url : "<?php echo base_url('PBIS/getDataJson')?>",
        data: {
            enc_id         : $enc_id
        },
        dataType: "json",
        success: function(data){
          console.log(data);
          $('#modalForm').removeData('bs.modal')
          $('#modalForm #id').val(data.id);
          $('#modalForm #date').val(data.date);
          $('#modalForm #note').val(data.description);
          $('#modalForm #categoryroutine').val(data.catid);
          $.ajax({
            type : 'GET', 
            url  : '<?php echo base_url('pbis/getroutine/')?>'+data.catid, 
            success : function(option){
              console.log(option);
              $('#subroutine').html(option);
              $('#modalForm #subroutine').val(data.routine_id).prop('selected',true);
            }
          }); 
          $('#modalForm #resident').val(data.resident_id).prop('selected',true);
          $('#modalForm #star').val(data.star).prop('selected',true);
          $('#modalForm #myModalLabel').text('PBIS-Edit');
          $('#modalForm').modal('show');
        },
        error: function(data){
          console.log(data);
        }
    });
 }
 function DeleteFunction (e,$id){
    <?php if ($this->acl->has_permission('class-delete')) : ?>
         swal({
                  title: "Are You Sure?",
                  text: "Remove Data!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes, Remove!",
                  confirmButtonColor: "#ec6c62",
                  closeOnConfirm: false
              },
              function(){
                 $.ajax({
                    type: 'POST',
                    url : "<?php echo base_url('pbis/delete')?>",
                    data: {
                        id         : $id
                    },
                    dataType: "json",
                    success: function(data){
                      console.log(data);
                       if(data.success){
                            swal('Yes',data.message,'success');
                            table.ajax.reload();
                       }else{
                          swal('Ups',data.message,'error');
                       }
                    },
                    error: function(data){
                      console.log(data);
                    }
                });
              });
    <?php else : ?>
      swal('Ups',"You can't access this module. Please contact Administrator.",'error');
      table.ajax.reload();
    <?php endif; ?>
}
</script>
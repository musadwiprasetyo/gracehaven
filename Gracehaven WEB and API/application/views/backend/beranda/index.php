 <div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="#"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li>Dashboard</li>
                </ul>
                <h4>Dashboard</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="alert alert-info">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
            <strong>Wellcome, </strong> <?php echo strtoupper($this->session->userdata('name'));?>
        </div>
        <div class="row row-stat">
            <div class="col-md-6">
                <div class="panel panel-success-alt noborder">
                    <div class="panel-heading noborder">
                       
                        <div class="panel-icon"><i class="fa fa-graduation-cap"></i></div>
                        <div class="media-body">
                            <h5 class="md-title nomargin">Manage </h5>
                            <h1 class="mt5">Schedule</h1>
                        </div>
                        <hr>
                        <div class="clearfix mt20">
                            <div class="pull-left">
                               
                            </div>
                            <div class="pull-right">
                                <h4 class="nomargin">
                                    <a href="<?php echo base_url();?>myschedule" class="btn btn-primary"><span>Manage Schedule</span></a>
                                </h4>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="panel panel-primary noborder">
                    <div class="panel-heading noborder">
                        
                        <div class="panel-icon"><i class="fa fa-book"></i></div>
                        <div class="media-body">
                            <h5 class="md-title nomargin">Manage</h5>
                            <h1 class="mt5">PBIS</h1>
                        </div>
                        <hr>
                        <div class="clearfix mt20">
                            <div class="pull-left">
                             
                            </div>
                            <div class="pull-right">
                                <h4 class="nomargin">
                                    <a href="<?php echo base_url();?>pbis" class="btn btn-success"><span>Manage PBIS</span></a>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
           

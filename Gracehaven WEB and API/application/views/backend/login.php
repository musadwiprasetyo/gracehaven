<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Gracehaven | Login</title>

        <link href="<?php echo base_url();?>assets/backend/css/style.default.css" rel="stylesheet">
        <link rel="shortcut icon" type="image/png" href="<?php echo base_url();?>assets/backend/images/logo.png"/>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="signin">
        <section>
            <div class="panel panel-signin">
                <div class="panel-body">
                    <?php $flashpesan = $this->session->flashdata('error'); ?>
                      <?php if (!empty($flashpesan)) : ?>
                      <div class="alert alert-<?php echo $flashpesan[0]; ?>">
                          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                          <?php echo $flashpesan[1]; ?>
                      </div>
                      <?php endif; ?>
                    <div class="logo text-center">
                        <img src="<?php echo base_url();?>assets/backend/images/logo-2.png" alt="Gracehaven" width="300">
                    </div>
                    <br />
                    <h4 class="text-center mb5">Sign in to your account</h4>  
                    <div class="mb30"></div>
                    
                    <form action="<?php echo base_url();?>ceklogin" method="post">
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" class="form-control"  name="username" placeholder="Username">
                        </div>
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input type="password" class="form-control" name="password" placeholder="Password">
                        </div>
                        
                        <div class="clearfix">
                            <button type="submit" class="btn btn-success btn-block">Log In <i class="fa fa-angle-right ml5"></i></button>
                        </div>                      
                    </form>
                    
                </div>
                <div class="panel-footer text-center">
                    <a href="" class="btn-block"></a>
                </div>
            </div>
            
        </section>


        <script src="<?php echo base_url();?>assets/backend/js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/modernizr.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/pace.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/retina.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/js/jquery.cookies.js"></script>

        <script src="<?php echo base_url();?>assets/backend/js/custom.js"></script>
        </body>
</html>

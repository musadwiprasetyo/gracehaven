 <div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url();?>home"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li><a href="#">Profil</a></li>
                </ul>
                <h4>Profil</h4>
            </div>
        </div>
    </div>
    
    <div class="contentpanel">
         <?php $flashpesan = $this->session->flashdata('error'); ?>
          <?php if (!empty($flashpesan)) : ?>
          <div class="alert alert-<?php echo $flashpesan[0]; ?>">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <?php echo $flashpesan[1]; ?>
          </div>

          <?php endif; ?> 
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="POST"action="<?php echo base_url();?>profil/updated">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Form</h4>
                            <input type="hidden" name="type" class="form-control" required="" value="<?php echo $staff ? $staff->group_id : ''?>" />
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Name <span class="asterisk">*</span></label>
                                <div class="col-sm-7">
                                    <input type="text" name="name" class="form-control" required="" value="<?php echo $staff ? $staff->name : ''?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Password <?php echo $staff ? '' : '<span class="asterisk">*</span>'?></label>
                                <div class="col-sm-7">
                                    <input type="password" name="password" class="form-control"  <?php echo $staff ? '' : 'required';?>  value="" />
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                             <a href="<?php echo base_url();?>home" class="btn btn-default">Back</a>
                            <button class="btn btn-primary mr5" type="Submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript">

  $(document).ready(function(){
       $('#datepicker').datepicker();
       $("#select-basic, #select-multi").select2();  
  })
</script>
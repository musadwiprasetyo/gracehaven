 <div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url();?>home"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li><a href="#">Settings</a></li>
                    <li><a href="<?php echo base_url();?>staffs">Staffs</a></li>
                    <li><a href="#">Detail Profil</a></li>         
                </ul>
                <h4>Detail Profil</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="pageheader">
            <div class="media">
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href="index.php"><i class="glyphicon glyphicon-home"></i></a></li>
                        <li>Profil</li>
                    </ul>
                    <h4>Profil</h4>
                </div>
            </div>
        </div>
        <div class="contentpanel">
            <div class="alert alert-info">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                <strong>Heads up!</strong> This <a class="alert-link" href="#">alert needs your attention</a>, but it's not super important.
            </div>
             <div class="row">
                <div class="col-sm-4 col-md-3">
                    <div class="text-center">
                        <img src="<?php echo base_url();?>assets/backend/images/photos/profile-big.jpg" class="img-circle img-offline img-responsive img-profile" alt="" />
                        <h4 class="profile-name mb5"><?php echo $staff->name;?></h4>
                        <div><i class="fa fa-briefcase"></i> <?php echo $staff->divisi;?></div>
                    
                        <div class="mb20"></div>
                    </div>
                    <h5 class="md-title">Address</h5>
                    <address>
                        795 Folsom Ave, Suite 600<br>
                        San Francisco, CA 94107<br>
                        <abbr title="Phone">P:</abbr> (123) 456-7890
                    </address>
                </div>
                
                <div class="col-sm-8 col-md-9">
                    <ul class="nav nav-tabs nav-line">
                        <li class="active"><a href="#activities" data-toggle="tab"><strong>Activities</strong></a></li>
                        <li><a href="#agenda" data-toggle="tab"><strong>My Agenda</strong></a></li>
                    </ul>
                    <div class="tab-content nopadding noborder">
                        <div class="tab-pane active" id="activities">
                            <div class="activity-list">  
                                <div class="media">
                                    <div class="media-body">
                                        <strong>Ray Sin</strong> started following <strong>Eileen Sideways</strong>. <br />
                                        <small class="text-muted">Yesterday at 3:30pm</small>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-body">
                                        <strong>Ray Sin</strong> started following <strong>Eileen Sideways</strong>. <br />
                                        <small class="text-muted">Yesterday at 3:30pm</small>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-white btn-block">Show More</button>
                        </div>
                    <div class="tab-pane" id="agenda">
                        <div class="events">
                            <h5 class="lg-title mb20">Agenda</h5>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="media">
                                        <div class="media-body">
                                            <h4 class="event-title"><a href="#">Free Living Trust Seminar</a></h4>
                                            <small class="text-muted"><i class="fa fa-map-marker"></i> Silicon Valley, San Francisco, CA</small>
                                            <small class="text-muted"><i class="fa fa-calendar"></i> Sunday, January 15, 2014 at 11:00am</small>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor...</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="media">
                                        <div class="media-body">
                                            <h4 class="event-title"><a href="#">Free Living Trust Seminar</a></h4>
                                            <small class="text-muted"><i class="fa fa-map-marker"></i> Silicon Valley, San Francisco, CA</small>
                                            <small class="text-muted"><i class="fa fa-calendar"></i> Sunday, January 15, 2014 at 11:00am</small>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor...</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                        </div>
                    </div>
                </div>    
                </div>
            </div>  
        </div>
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/jquery-1.11.1.min.js"></script>

 <div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url();?>home"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li><a href="#">Settings</a></li>
                    <li><a href="<?php echo base_url();?>groups">Groups</a></li>
                   
                </ul>
                <h4>Detail Group</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
         <?php $flashpesan = $this->session->flashdata('error'); ?>
          <?php if (!empty($flashpesan)) : ?>
          <div class="alert alert-<?php echo $flashpesan[0]; ?>">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <?php echo $flashpesan[1]; ?>
          </div>

          <?php endif; ?> 
        <div class="panel panel-primary-head">
            <div class="panel-heading">
                <h4 class="panel-title"><?php echo $group->group_name;?></h4>
            </div><br/><br/>
            <div class="row">
                <div class="col-md-12">
                   <ul class="nav nav-tabs nav-info">
                        <li class="active"><a href="#modules" data-toggle="tab"><strong>Modules</strong></a></li>
                        <li><a href="#users" data-toggle="tab"><strong>Users</strong></a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content tab-content-info mb30">
                        <div class="tab-pane active" id="modules">
                             <br/>
                             <h5 class="lg-title mb5">Module</h5>
                              <p class="mb20">For access menu in this role</p>
                              <table id="table" class="table table-striped table-bordered responsive">
                                  <thead>
                                      <tr>
                                          <th class="text-center" style="width: 1px;">No</th>
                                          <th width="30%">Name</th>
                                          <th width="10%">Keyword</th>
                                          <th width="45%">Description</th>
                                          <th width="5%">Group</th>
                                          <th width="5%">Status</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                  </tbody>
                              </table>
                        </div>
                      
                        <div class="tab-pane" id="users">
                             <br/>
                             <h5 class="lg-title mb5">Users</h5>
                              <p class="mb20">For access user in this role</p>
                              <table id="table2" class="table table-striped table-bordered responsive display" width="100%">
                                  <thead>
                                      <tr>
                                          <th class="text-center" width="1%">No</th>
                                          <th width="100%">Name</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                  </tbody>
                              </table>
                        </div>
                      
                       
                    </div>
                  </div>
                </div>

           
        </div>
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
  var table,table2; 
  $(document).ready(function(){
      table= $('#table').DataTable({
          "processing": true,
          "keys"      : true,
          "serverSide": true,
          "pageLength": 10,
          "dom"       : 'frtp',
          "ajax"      :{
                          "url": "<?php echo base_url('group/getdatapermission/'.$enc_id)?>",
                          "dataType": "json",
                          "type": "POST",
                          "data": function(d) {
                              
                      }
          },
          "columns": [
            { "data": "no", 
              "orderable" : false, 
            },
            { "data": "name"},
            { "data": "key"},
            { "data": "description","orderable" : false,},
            { "data": "group"},
            { "data": "status"},
            
      
          ],
          responsive: true,
          language: {
            search: "_INPUT_",
            searchPlaceholder: "Searching",
          }
      });
       table2= $('#table2').DataTable({
          "processing": true,
          "keys"      : true,
          "serverSide": true,
          "pageLength": 10,
          "dom"       : 'frtp',
          "ajax"      :{
                          "url": "<?php echo base_url('group/getdatauser/'.$enc_id)?>",
                          "dataType": "json",
                          "type": "POST",
                          "data": function(d) {
                              
                      }
          },
          "columns": [
            { "data": "no", 
              "orderable" : false, 
            },
            { "data": "name"},
      
          ],
          responsive: true,
          language: {
            search: "_INPUT_",
            searchPlaceholder: "Searching",
          }
      });
  })
</script>
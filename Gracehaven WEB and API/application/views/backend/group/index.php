 <div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url();?>home"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li><a href="#">Settings</a></li>
                    <li>Group</li>
                </ul>
                <h4>Manage Group</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
         <?php $flashpesan = $this->session->flashdata('error'); ?>
          <?php if (!empty($flashpesan)) : ?>
          <div class="alert alert-<?php echo $flashpesan[0]; ?>">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <?php echo $flashpesan[1]; ?>
          </div>

          <?php endif; ?> 
        <div class="panel panel-primary-head">
            <div class="panel-heading">
                <div class="pull-right">
                  <?php if ($this->acl->has_permission('group-add')) : ?>
                    <div class="btn-group">
                        <a href="<?php echo base_url();?>group/add" class="btn btn-sm mt5 btn-white noborder btn-success"><i class="fa fa-plus"></i> New Data</a>
                    </div>
                  <?php endif; ?>
                </div>
                <h4 class="panel-title">Group</h4>
                <p>Searching, ordering, paging etc goodness will be immediately added to the table.</p>

            </div>

            <table id="table" class="table table-striped table-bordered responsive display">
                <thead>
                    <tr>
                        <th class="text-center" width="2%">No</th>
                        <th width="30%">Name</th>
                        <th class="text-center" style="width: 100px;">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
  var table; 
   
  $(document).ready(function(){
      table= $('#table').DataTable({
          "processing": true,
          "keys"      : true,
          "serverSide": true,
          "pageLength": 10,
          "dom"       : 'frtp',
          "ajax"      :{
                          "url": "<?php echo base_url('group/getdata')?>",
                          "type": "POST"
          },
          "columns": [
            { "data": "no", 
              "orderable" : false, 
            },
            { "data": "name"},
            { "data" : "action",
              "orderable" : false,
              "className" : "text-center",
            },
      
          ],
          language: {
            search: "_INPUT_",
            searchPlaceholder: "Searching",
          }
      });
       
  })
  
  function DeleteFunction (e,$id){
    <?php if ($this->acl->has_permission('group-delete')) : ?>
         swal({
                  title: "Are You Sure?",
                  text: "Remove Data!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes, Remove!",
                  confirmButtonColor: "#ec6c62",
                  closeOnConfirm: false
              },
              function(){
                 $.ajax({
                    type: 'POST',
                    url : "<?php echo base_url('Group/deleteRole')?>",
                    data: {
                        id         : $id
                    },
                    dataType: "json",
                    success: function(data){
                      console.log(data);
                       if(data.success){
                            swal('Yes',data.message,'success');
                            table.ajax.reload();
                       }else{
                          swal('Ups',data.message,'error');
                       }
                    },
                    error: function(data){
                      console.log(data);
                    }
                });
              });
    <?php else : ?>
      swal('Ups',"You can't access this module. Please contact Administrator.",'error');
      table.ajax.reload();
    <?php endif; ?>
}
</script>
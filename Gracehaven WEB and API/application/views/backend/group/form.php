 <div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url();?>home"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li><a href="#">Settings</a></li>
                    <li><a href="<?php echo base_url();?>groups">Groups</a></li>
                    <li><?php echo $group ? 'Edit Group' : 'New Group'?> </li>
                </ul>
                <h4><?php echo $group ? 'Edit Group' : 'New Group'?> </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <?php $flashpesan = $this->session->flashdata('error'); ?>
        <?php if (!empty($flashpesan)) : ?>
        <div class="alert alert-<?php echo $flashpesan[0]; ?>">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $flashpesan[1]; ?>
        </div>
        <?php endif; ?> 
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="POST" <?php if(isset($group)):?> action="<?php echo base_url();?>group/updated/<?php echo $enc_id;?>" 
                <?php else: ?> action="<?php echo base_url();?>group/save" 
                <?php endif ?>>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Form</h4>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Name <span class="asterisk">*</span></label>
                                <div class="col-sm-7">
                                    <input type="text" name="name" class="form-control" required="" value="<?php echo $group ? $group->group_name : ''?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Modules</label>
                                <div class="col-sm-7">
                                    <?php echo $checkbox_loop; ?>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <a href="<?php echo base_url();?>groups" class="btn btn-default">Back</a>
                            <button class="btn btn-primary mr5" type="Submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
       $('#datepicker').datepicker();
       $("#select-basic, #select-multi").select2();  
  })
</script>
 <div class="mainpanel">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url();?>home"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li><a href="#">Settings</a></li>
                    <li><a href="<?php echo base_url();?>modules">Modules</a></li>
                    <li><a href="#">Detail Module</a></li>         
                </ul>
                <h4>Detail Module</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
         <?php $flashpesan = $this->session->flashdata('error'); ?>
          <?php if (!empty($flashpesan)) : ?>
          <div class="alert alert-<?php echo $flashpesan[0]; ?>">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <?php echo $flashpesan[1]; ?>
          </div>

          <?php endif; ?> 
        <div class="panel panel-primary-head">
            <div class="panel-heading">
                <h4 class="panel-title"><?php echo $module->name;?></h4>
                <p><?php echo $module->description;?></p>
            </div><br/><br/>
          
        </div>
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10">
                                       <p class="control-label"><?php echo $module->name;?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Key</label>
                                <div class="col-sm-10">
                                      <p class="control-label"><?php echo $module->key;?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-10">
                                      <p class="control-label"><?php echo $module->description;?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Group</label>
                                <div class="col-sm-10">
                                      <p class="control-label"><?php echo $module->group;?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Order</label>
                                <div class="col-sm-10">
                                      <p class="control-label"><?php echo $module->order;?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-10">
                                      <p class="control-label"><?php echo $module->statuspermission==1 ? 'Active' : 'Disable'?> </p>
                                </div>
                            </div>
                           
                        </div>
                       
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/jquery-1.11.1.min.js"></script>

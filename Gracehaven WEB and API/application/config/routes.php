<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] 			= 'Home/index';
$route['login'] 				 		= 'Auth';
$route['ceklogin'] 				 		= 'Auth/cekAuth';
$route['logout'] 				 		= 'Auth/logout';
$route['home'] 				 			= 'Home/index';

$route['groups'] 			 			= 'Group';
$route['group/getdata'] 				= 'Group/getData';
$route['group/add']  		   			= 'Group/form';
$route['group/edit/(:any)']  		   	= 'Group/form/$1';
$route['group/save']  					= 'Group/save';
$route['group/updated/(:any)']  		= 'Group/save/$1';
$route['group/detail/(:any)']      		= 'Group/Detail/$1';
$route['group/getdatapermission/(:any)']= 'Group/getDataPermission/$1';
$route['group/getdatauser/(:any)']	    = 'Group/getDataUser/$1';


$route['modules'] 			 			= 'Modules';
$route['module/getdata'] 				= 'Modules/getData';
$route['module/add']  		   			= 'Modules/form';
$route['module/edit/(:any)']  		   	= 'Modules/form/$1';
$route['module/save']  					= 'Modules/save';
$route['module/updated/(:any)']  		= 'Modules/save/$1';
$route['module/detail/(:any)']      	= 'Modules/Detail/$1';



$route['staffs'] 			 			= 'Staff';
$route['staff/getdata'] 				= 'Staff/getData';
$route['staff/add']  		   			= 'Staff/form';
$route['staff/edit/(:any)']  		   	= 'Staff/form/$1';
$route['staff/save']  					= 'Staff/save';
$route['staff/updated/(:any)']  		= 'Staff/save/$1';
$route['staff/detail/(:any)']      		= 'Staff/Detail/$1';
$route['staff/deletestaff']      		= 'Staff/deleteStaff';


$route['class'] 			 			= 'ClassController';
$route['class/getdata'] 				= 'ClassController/getData';
$route['class/add']  		   		    = 'ClassController/form';
$route['class/edit/(:any)']  		    = 'ClassController/form/$1';
$route['class/save']  					= 'ClassController/save';
$route['class/updated/(:any)']  		= 'ClassController/save/$1';
$route['class/delete']      			= 'ClassController/deleteData';


$route['classstaff'] 			 		= 'ClassStaff';
$route['classstaff/getdata'] 			= 'ClassStaff/getData';
$route['classstaff/add']  		   		= 'ClassStaff/form';
$route['classstaff/edit/(:any)']  		= 'ClassStaff/form/$1';
$route['classstaff/save']  				= 'ClassStaff/save';
$route['classstaff/updated/(:any)']  	= 'ClassStaff/save/$1';
$route['classstaff/delete']      		= 'ClassStaff/deleteData';

$route['classstaff/schedule/(:any)']  	= 'ClassStaff/Schedule/$1';
$route['classstaff/schedule/getdata/(:any)'] = 'ClassStaff/ScheduleGetData/$1';
$route['classstaff/schedule/attendance/(:any)']  	= 'ClassStaff/Attendance/$1';
$route['attendance/insert']  	= 'ClassStaff/AttendanceInsert';

$route['classstaff/detailresident/(:any)']  	= 'ClassStaff/DetailResident/$1';
$route['residentclass/delete']  		= 'ClassStaff/ResidentDelete';
$route['residentclass/insert']  	    = 'ClassStaff/InsertResident';


$route['schedule/save']      			= 'ClassStaff/ScheduleSave';
$route['schedule/delete']      			= 'ClassStaff/deleteDataSchedule';


$route['resident'] 			 			= 'Resident';
$route['resident/getdata'] 				= 'Resident/getData';
$route['resident/add']  		   		= 'Resident/form';
$route['resident/edit/(:any)']  		= 'Resident/form/$1';
$route['resident/save']  				= 'Resident/save';
$route['resident/updated/(:any)']  		= 'Resident/save/$1';
$route['resident/detail/(:any)']      	= 'Resident/Detail/$1';
$route['resident/deleteresident']      	= 'Resident/deleteResident';

$route['help']      					= 'Help';

$route['404_override'] 		 = '';

//PBIS
$route['pbis'] 			 		= 'PBIS';
$route['pbis/getdata'] 			= 'PBIS/getData';
$route['pbis/getroutine/(:any)']= 'PBIS/getRoutine/$1';
$route['pbis/save']  		    = 'PBIS/save';
$route['pbis/delete']      		= 'PBIS/deleteData';

//PROFIL
$route['profil'] 			    = 'Staff/profil';
$route['profil/updated'] 		= 'Staff/profilupdated';


//schedule per id
$route['myschedule'] 			 		= 'Schedule';
$route['myschedule/getdata'] 			= 'Schedule/getData';
$route['myschedule/add']  		   		= 'Schedule/form';
$route['myschedule/edit/(:any)']  		= 'Schedule/form/$1';
$route['myschedule/save']  				= 'Schedule/save';
$route['myschedule/updated/(:any)']  	= 'Schedule/save/$1';
$route['myschedule/delete']      		= 'Schedule/deleteData';

$route['myschedule/schedule/(:any)']  	= 'Schedule/Schedule/$1';
$route['myschedule/schedule/getdata/(:any)'] = 'Schedule/ScheduleGetData/$1';
$route['myschedule/schedule/attendance/(:any)']  	= 'Schedule/Attendance/$1';
$route['myschedule/attendance/insert']  	= 'Schedule/AttendanceInsert';

$route['myschedule/detailresident/(:any)']  	= 'Schedule/DetailResident/$1';
$route['myschedule/residentclass/delete']  		= 'Schedule/ResidentDelete';
$route['myschedule/residentclass/insert']  	    = 'Schedule/InsertResident';


$route['myschedule/schedule/save']      			= 'Schedule/ScheduleSave';
$route['myschedule/schedule/delete']      			= 'Schedule/deleteDataSchedule';


//API MOBILE

$route['api/login']		     	 = 'ApiController';
$route['api/profilresident']= 'ApiController/profilresident';
$route['api/profilstaff']= 'ApiController/profilstaff';


$route['api/getresident']	 	 = 'ApiController/getResident';
$route['api/getclass']	 	     = 'ApiController/getClass';
$route['api/processclass']	 	 = 'ApiController/processClass';
$route['api/deleteclass']	 	 = 'ApiController/deleteClass';

$route['api/getclassstaff']	 	        = 'ApiController/getClassStaff';
$route['api/getclassstafftoday/(:any)']	   = 'ApiController/getClassStaffToday/$1';
$route['api/getclassresidenttoday/(:any)']	   = 'ApiController/getClassResidentToday/$1';


$route['api/getclassstaffbyuser/(:any)']	 	        = 'ApiController/getClassStaffByUser/$1';
$route['api/getclassstaffschedule/(:any)']= 'ApiController/getClassStaffSchedule/$1';
$route['api/dataattendance/(:any)']= 'ApiController/getDataAttendance/$1';

$route['api/categoryroutine']	 = 'ApiController/getCategoryRoutine';
$route['api/routine/(:any)']	 = 'ApiController/getRoutine/$1';

$route['api/getroutineinput']	 = 'ApiController/getRoutineInput';
$route['api/getroutinerecord']	 = 'ApiController/getRoutineRecord';


$route['api/attendancetaking']	 = 'ApiController/AttendanceTaking';
$route['api/logout']		     = 'ApiController/logout';

//master

$route['api/masterstaff']	 	= 'ApiController/masterstaff';
$route['api/masterclass']	 	= 'ApiController/masterclass';
$route['api/masterday']	 		= 'ApiController/masterday';

$route['api/masterresident']	= 'ApiController/masterresident';

//PBIS
$route['api/processpbis']	= 'ApiController/processPBIS';

//recordPBIS
$route['api/recordpbis']		     = 'ApiController/recordPBIS';
$route['api/detailrecordpbis/(:any)']= 'ApiController/detailrecordPBIS/$1';
$route['api/detailrecordPBISperResident/(:any)/(:any)']= 'ApiController/detailrecordPBISperResident/$1/$2';

//class staff
$route['api/processclassstaff']		 = 'ApiController/processClassStaff';
//schedule
$route['api/processchedule']	     = 'ApiController/processSchedule';

$route['translate_uri_dashes'] = FALSE;

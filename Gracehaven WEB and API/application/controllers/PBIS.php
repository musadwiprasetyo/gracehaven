<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PBIS extends CI_Controller {

	function __construct() 
	{
		parent::__construct();
		
		$this->load->model('Model_pbis','',TRUE);
		$this->load->model('Model_routine','',TRUE);
		if ($this->session->userdata('login')!=TRUE) {
    		redirect('manage/login');
    	} 
    	if ($this->session->userdata('isstaff')!='1'){
    		redirect('home');
    	}
	}
   private function status()
	{

		$status = array('1'=>'Active','0'=>'Disable');
		return $status;
	}
	private function star()
	{

		$star = array('1','2','3','4','5');
		return $star;
	}

	public function index()
	{
		$data['judul']	         = 'Gracehaven | PBIS';	
		$data['status'] 		 = $this->status();
		$data['categoryroutine'] = $this->Model_pbis->categoryroutine()->result();
		$data['subroutine'] 	 = $this->Model_pbis->subroutine()->result();
		$data['residents']       = $this->Model_pbis->residents()->result();
		$data['star'] 			 = $this->star();
		
		if ($this->session->userdata('isstaff')=='1') {
			$this->template->display('backend/pbis/index', $data);
		} else {
			$this->load->view('errors/html/error_access', $data);
		}
	}
	public function getData()
    {
		$query = $this->Model_pbis->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($query as $key=> $pbis) {
			$enc_id = $this->Model_pbis->safe_encode($this->encryption->encrypt($pbis->id));
			//count resident by id class staff
			$action = "";
			$action .="<p class='nomargin'>";
			if ($this->session->userdata('isstaff')=='1') {
		        $action .='<a href="javascript:void(0)" class="btn btn-primary btn-xs"  onclick="UpdateFunction(this,\''.$enc_id.'\')" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>&nbsp;';
			}
			if ($this->session->userdata('isstaff')=='1') { 
	        	$action .='<a href="javascript:void(0)" class="btn btn-danger btn-xs"  onclick="DeleteFunction(this,\''.$pbis->id.'\')" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></a>&nbsp';
			} 
			
			$action .="</p'>";
			$no++;
			$row = array();
			$row['no'] 				= $no;    
			$row['name'] 			= $pbis->name;     
			$row['staff'] 			= $pbis->staffid.'#'.ucfirst($pbis->staffname);     
			$row['time'] 			= $pbis->day_name.','.date('d/m/Y',strtotime($pbis->date)); 
			$row['routine_name']    = $pbis->routine_name; 
			$row['catname'] 		= $pbis->catname; 
			$row['point'] 			= $pbis->star.'/'.$pbis->default_star; 
			$row['action'] 			= $action;
			$data[] 				= $row;

		}
		if ($this->session->userdata('isstaff')=='1') {
			$output = array(
					"draw" 				=> $_POST['draw'],
					"recordsTotal" 		=> $this->Model_pbis->count_all(),
					"recordsFiltered" 	=> $this->Model_pbis->count_filtered(),
					"data" 				=> $data,
			);
		} else {
			$output = array(
				"draw" 				=> 0,
				"recordsTotal" 		=> 0,
				"recordsFiltered" 	=> 0,
				"data" 				=> null,
			);
		}
		
		echo json_encode($output);
    }
    public function getRoutine($id)
    {
    	$routines = $this->Model_routine->viewByCategory($id)->result(); 
		echo '<option> Select Routine </option>';
		foreach ($routines as $r){
			 echo '<option value="'.$r->id.'">'.$r->routine_name.'</option>'; 
		}
    }

    public function statusData()
    {
    	$id= $this->input->post('id');
    	$value= $this->input->post('value');
		$query = $this->Model_pbis->updateStatusData($id,$value);
    	if($query==FALSE) {
    		 $data = array(
                    "success"    => FALSE,  
                    "message"    => 'Failed Updated.'
                    );
			
		} else {
			 $data = array(
                    "success"    => TRUE,  
                    "message"    => 'Success Updated.'
                    );
		}	
	    echo json_encode($data);
    }
    public function getDataJson()
    {
    	$enc_id= $this->input->post('enc_id');
    	$id = $this->encryption->decrypt($this->Model_pbis->safe_decode($enc_id));
    	$data	= $this->Model_pbis->getDataJson($id)->row();
	    echo json_encode($data);

    }
    public function save()
    {
    	$insertUpdateSubmit = $this->Model_pbis->insertUpdateSubmit();
		if($insertUpdateSubmit==TRUE) {
			$id 	= $this->input->post('id');
			if ($id =='') {
				$data = array(
	                    "success"    => TRUE,  
	                    "message"    => 'Success Add Data.'
	            );
			} else {
				$data = array(
	                    "success"    => TRUE,  
	                    "message"    => 'Success Updated Data.'
	            );
			}
			
		} else {
			$data = array(
                    "success"    => FALSE,  
                    "message"    => 'Failed Add Or Update Data.'
            );
		}	
	    echo json_encode($data);
    }

    public function deleteData()
    {
    	$id= $this->input->post('id');
		$delete = $this->Model_pbis->deleteData($id);
		if ($delete) {
			$data = array(
                "success"    => TRUE,  
                "message"    => 'Success Deleted.'
            );
		} else {
			$data = array(
                "success"    => FALSE,  
                "message"    => 'Failed Deleted.'
            );
		}	
	    echo json_encode($data);
    }    
}

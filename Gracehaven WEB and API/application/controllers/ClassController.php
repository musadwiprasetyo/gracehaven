<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ClassController extends CI_Controller {

	function __construct() 
	{
		parent::__construct();
		
		$this->load->model('Model_class','',TRUE);
		if ($this->session->userdata('login')!=TRUE) {
    		redirect('manage/login');
    	} 
	}
   private function status()
	{
		$status = array('1'=>'Active','0'=>'Disable');
		return $status;
	}

	public function index()
	{
		$data['judul']		= 'Gracehaven | Class';	
		$data['status'] = $this->status();
		if ($this->acl->has_permission('class')) {
			$this->template->display('backend/class/index', $data);
		} else {
			$this->load->view('errors/html/error_access', $data);
		}
	}
	public function getData()
    {
		$query = $this->Model_class->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($query as $key=> $class) {
			$enc_id = $this->Model_class->safe_encode($this->encryption->encrypt($class->class_id));

			$action = "";
			$action .="<p class='nomargin'>";
			if ($this->acl->has_permission('class-edit')) {
		        $action .='<a href="javascript:void(0)" class="btn btn-primary btn-xs"  onclick="UpdateFunction(this,\''.$enc_id.'\')" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>&nbsp;';
			}
			if ($this->acl->has_permission('class-delete')) { 
	        	$action .='<a href="javascript:void(0)" class="btn btn-danger btn-xs"  onclick="DeleteFunction(this,\''.$class->class_id.'\')" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></a>';
			} 
			

			$action .="</p'>";
			$no++;
			$row = array();
			$row['no'] 				= $no;    
			$row['name'] 			= $class->class_name;     
			$row['action'] 			= $action;
			$data[] 				= $row;

		}
		if ($this->acl->has_permission('class')) {
			$output = array(
					"draw" 				=> $_POST['draw'],
					"recordsTotal" 		=> $this->Model_class->count_all(),
					"recordsFiltered" 	=> $this->Model_class->count_filtered(),
					"data" 				=> $data,
			);
		} else {
			$output = array(
				"draw" 				=> 0,
				"recordsTotal" 		=> 0,
				"recordsFiltered" 	=> 0,
				"data" 				=> null,
			);
		}
		
		echo json_encode($output);
    }
    public function statusData()
    {
    	$id= $this->input->post('id');
    	$value= $this->input->post('value');
		$query = $this->Model_class->updateStatusData($id,$value);
    	if($query==FALSE) {
    		 $data = array(
                    "success"    => FALSE,  
                    "message"    => 'Failed Updated.'
                    );
			
		} else {
			 $data = array(
                    "success"    => TRUE,  
                    "message"    => 'Success Updated.'
                    );
		}	
	    echo json_encode($data);
    }
    public function getDataJson()
    {
    	$enc_id= $this->input->post('enc_id');
    	$id = $this->encryption->decrypt($this->Model_class->safe_decode($enc_id));
    	$data	= $this->Model_class->getDataJson($id)->row();
	    echo json_encode($data);

    }
    public function save()
    {
    	$insertUpdateSubmit = $this->Model_class->insertUpdateSubmit();
		if($insertUpdateSubmit==TRUE) {
			$classid 	= $this->input->post('classid');
			if ($classid =='') {
				$data = array(
	                    "success"    => TRUE,  
	                    "message"    => 'Success Add Data.'
	            );
			} else {
				$data = array(
	                    "success"    => TRUE,  
	                    "message"    => 'Success Updated Data.'
	            );
			}
			
		} else {
			$data = array(
                    "success"    => FALSE,  
                    "message"    => 'Failed Add Or Update Data.'
            );
		}	
	    echo json_encode($data);
    }

    public function deleteData()
    {
    	$id= $this->input->post('id');
		$query = $this->Model_class->cekExist($id);
		if ($query > 0) {
			$data = array(
                    "success"    => FALSE,  
                    "message"    => "You can't remove because this class used by product. Please disable if you not used."
                    );
		} else {
			$delete = $this->Model_class->deleteData($id);
			if ($delete) {
				$data = array(
                    "success"    => TRUE,  
                    "message"    => 'Success Deleted.'
                );
			} else {
				$data = array(
                    "success"    => FALSE,  
                    "message"    => 'Failed Deleted.'
                );
			}	
		}
	    echo json_encode($data);
    }
}

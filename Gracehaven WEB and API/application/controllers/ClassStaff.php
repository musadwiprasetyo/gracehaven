<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ClassStaff extends CI_Controller {

	function __construct() 
	{
		parent::__construct();
		
		$this->load->model('Model_classstaff','',TRUE);
		if ($this->session->userdata('login')!=TRUE) {
    		redirect('manage/login');
    	} 
	}
   private function status()
	{
		$status = array('1'=>'Active','0'=>'Disable');
		return $status;
	}

	public function index()
	{
		$data['judul']		= 'Gracehaven | Schedule Class';	
		$data['status'] = $this->status();
		$data['programme']	= $this->Model_classstaff->getClass()->result();
		$data['staffs']	= $this->Model_classstaff->getStaff()->result();
		$data['days']	= $this->Model_classstaff->getDay()->result();

		if ($this->acl->has_permission('classstaff')) {
			$this->template->display('backend/classstaff/index', $data);
		} else {
			$this->load->view('errors/html/error_access', $data);
		}
	}
	public function getData()
    {
		$query = $this->Model_classstaff->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($query as $key=> $class) {
			$enc_id = $this->Model_classstaff->safe_encode($this->encryption->encrypt($class->id));
			//count resident by id class staff
			$resident= $this->Model_classstaff->countResident($class->id)->num_rows();
			$action = "";
			$action .="<p class='nomargin'>";

			if ($this->acl->has_permission('classstaff-edit')) {
		        $action .='<a href="javascript:void(0)" class="btn btn-primary btn-xs"  onclick="UpdateFunction(this,\''.$enc_id.'\')" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>&nbsp;';
			}
			if ($this->acl->has_permission('classstaff-delete')) { 
	        	$action .='<a href="javascript:void(0)" class="btn btn-danger btn-xs"  onclick="DeleteFunction(this,\''.$class->id.'\')" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></a>&nbsp';
			} 
			if ($this->acl->has_permission('classstaff-detail')) {
		       $action .='<a href="classstaff/detailresident/'.$enc_id.'" class="btn btn-warning btn-xs" data-toggle="tooltip" title="Resident"><i class="fa fa-users"></i></a>&nbsp;';
			}
			if ($this->acl->has_permission('classstaff-schedule')) {
		        
		        $action .='<a href="classstaff/schedule/'.$enc_id.'" class="btn btn-default btn-xs" data-toggle="tooltip" title="Schedule"><i class="fa fa-book"></i></a>&nbsp;';
			}
			

			$action .="</p'>";
			$no++;
			$row = array();
			$row['no'] 				= $no;    
			$row['name'] 			= $class->id.'#'.$class->class_name;     
			$row['staff'] 			= $class->id_staff.'#'.ucfirst($class->name);     
			$row['time'] 			= $class->day_name.','.$class->clock; 
			$row['resident'] 		= $resident; 
			$row['action'] 			= $action;
			$data[] 				= $row;

		}
		if ($this->acl->has_permission('classstaff')) {
			$output = array(
					"draw" 				=> $_POST['draw'],
					"recordsTotal" 		=> $this->Model_classstaff->count_all(),
					"recordsFiltered" 	=> $this->Model_classstaff->count_filtered(),
					"data" 				=> $data,
			);
		} else {
			$output = array(
				"draw" 				=> 0,
				"recordsTotal" 		=> 0,
				"recordsFiltered" 	=> 0,
				"data" 				=> null,
			);
		}
		
		echo json_encode($output);
    }
    public function statusData()
    {
    	$id= $this->input->post('id');
    	$value= $this->input->post('value');
		$query = $this->Model_classstaff->updateStatusData($id,$value);
    	if($query==FALSE) {
    		 $data = array(
                    "success"    => FALSE,  
                    "message"    => 'Failed Updated.'
                    );
			
		} else {
			 $data = array(
                    "success"    => TRUE,  
                    "message"    => 'Success Updated.'
                    );
		}	
	    echo json_encode($data);
    }
    public function getDataJson()
    {
    	$enc_id= $this->input->post('enc_id');
    	$id = $this->encryption->decrypt($this->Model_classstaff->safe_decode($enc_id));
    	$data	= $this->Model_classstaff->getDataJson($id)->row();
	    echo json_encode($data);

    }
    public function save()
    {
    	$insertUpdateSubmit = $this->Model_classstaff->insertUpdateSubmit();
		if($insertUpdateSubmit==TRUE) {
			$classstaffid 	= $this->input->post('classstaffid');
			if ($classstaffid =='') {
				$data = array(
	                    "success"    => TRUE,  
	                    "message"    => 'Success Add Data.'
	            );
			} else {
				$data = array(
	                    "success"    => TRUE,  
	                    "message"    => 'Success Updated Data.'
	            );
			}
			
		} else {
			$data = array(
                    "success"    => FALSE,  
                    "message"    => 'Failed Add Or Update Data.'
            );
		}	
	    echo json_encode($data);
    }

    public function deleteData()
    {
    	$id= $this->input->post('id');
		$query = $this->Model_classstaff->cekExist($id);
		if ($query > 0) {
			$data = array(
                    "success"    => FALSE,  
                    "message"    => "You can't remove because this class used by resident"
                    );
		} else {
			$delete = $this->Model_classstaff->deleteData($id);
			if ($delete) {
				$data = array(
                    "success"    => TRUE,  
                    "message"    => 'Success Deleted.'
                );
			} else {
				$data = array(
                    "success"    => FALSE,  
                    "message"    => 'Failed Deleted.'
                );
			}	
		}
	    echo json_encode($data);
    }
   	public function Schedule($enc_id)
	{
		$id = $this->encryption->decrypt($this->Model_classstaff->safe_decode($enc_id));
		if($id!=FALSE) {
				$data['detail'] = $this->Model_classstaff->getdataclassstaff($id);
				$data['judul']		= 'Gracehaven | Schedule';
				$data['enc_id'] 	= $enc_id;
				$data['id']			= $id;
				
				$this->template->display('backend/classstaff/schedule', $data);
		} else {
			$this->session->set_flashdata('error',array('warning','Data not found','warning'));
			redirect('classstaff');
		}
	}
	public function DetailResident($enc_id)
	{
		$id = $this->encryption->decrypt($this->Model_classstaff->safe_decode($enc_id));
		if($id!=FALSE) {
				$data['classstaff']	= $this->Model_classstaff->getdataclassstaff($id);
				$data['resident'] 	= $this->Model_classstaff->DetailResident($id);

				$data['judul']		= 'Gracehaven | Class Staff for Resident';
				$data['enc_id'] 	= $enc_id;
				$data['id']			= $id;
				
				$this->template->display('backend/classstaff/detailresident', $data);
		} else {
			$this->session->set_flashdata('error',array('warning','Data not found','warning'));
			redirect('classstaff');
		}
	}

	
	public function ScheduleGetData($id)
    {
		$query = $this->Model_classstaff->get_datatables_schedule($id);
		$data = array();
		$no = $_POST['start'];
		foreach ($query as $key=> $class) {
			$enc_id = $this->Model_classstaff->safe_encode($this->encryption->encrypt($class->id));

			//count resident by id class staff
			$resident= $this->Model_classstaff->countResident($class->id_class_staff)->num_rows();

			$residentpresence= $this->Model_classstaff->countResidentPresence($class->id)->num_rows();

			$action = "";
			$action .="<p class='nomargin'>";

			if ($this->acl->has_permission('schedule-edit')) {
		        $action .='<a href="javascript:void(0)" class="btn btn-primary btn-xs"  onclick="UpdateFunction(this,\''.$enc_id.'\')" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>&nbsp;';
			}
			if ($this->acl->has_permission('schedule-delete')) { 
	        	$action .='<a href="javascript:void(0)" class="btn btn-danger btn-xs"  onclick="DeleteFunction(this,\''.$class->id.'\')" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></a>&nbsp';
			} 
			if ($this->acl->has_permission('schedule-attendance')) {
		        $action .='<a href="attendance/'.$enc_id.'" class="btn btn-default btn-xs" data-toggle="tooltip" title="Attendance"><i class="fa fa-users"></i></a>&nbsp;';
			}
			

			$action .="</p'>";
			$no++;
			$row = array();
			$row['no'] 				= $no;    
			$row['schedule'] 		= $class->id.'#'.$class->schedule;     
			$row['date'] 			= $class->date;     
			$row['note'] 			= $class->note; 
			$row['resident'] 		= $resident.'|'.$residentpresence; 
			$row['action'] 			= $action;
			$data[] 				= $row;

		}
		if ($this->acl->has_permission('classstaff-schedule')) {
			$output = array(
					"draw" 				=> $_POST['draw'],
					"recordsTotal" 		=> $this->Model_classstaff->count_all_schedule($id),
					"recordsFiltered" 	=> $this->Model_classstaff->count_filtered_schedule($id),
					"data" 				=> $data,
			);
		} else {
			$output = array(
				"draw" 				=> 0,
				"recordsTotal" 		=> 0,
				"recordsFiltered" 	=> 0,
				"data" 				=> null,
			);
		}
		
		echo json_encode($output);
    }
    public function Attendance($enc_id)
    {
    	$id = $this->encryption->decrypt($this->Model_classstaff->safe_decode($enc_id));
		if($id!=FALSE) {
				$data['attendance'] 	= $this->Model_classstaff->Attendance($id)->row();

				$data['encrypt_id'] 	= $this->Model_classstaff->safe_encode($this->encryption->encrypt($data['attendance']->id_class_staff));
				$data['residentattendance'] = $this->Model_classstaff->ResidentAttendance($data['attendance']->id_class_staff,$data['attendance']->id);

				$data['attendanceresident'] = $this->Model_classstaff->AttendanceResident($data['attendance']->id)->result();
				

				$data['judul']		= 'Gracehaven | Attendance';
				$data['enc_id'] 	= $enc_id;
				$data['id']			= $id;
				
				$this->template->display('backend/classstaff/attendance', $data);
		} else {
			$this->session->set_flashdata('error',array('warning','Data not found','warning'));
			redirect('classstaff');
		}
    }
    public function getDataJsonSchedule()
    {
    	$enc_id= $this->input->post('enc_id');
    	$id = $this->encryption->decrypt($this->Model_classstaff->safe_decode($enc_id));
    	$data	= $this->Model_classstaff->getDataJsonSchedule($id)->row();
	    echo json_encode($data);

    }

    public function ScheduleSave()
    {

    	$insertUpdateSubmit = $this->Model_classstaff->insertUpdateSubmitSchedule();
		if($insertUpdateSubmit==TRUE) {
			$scheduleid 	= $this->input->post('scheduleid');
			if ($scheduleid =='') {
				$data = array(
	                    "success"    => TRUE,  
	                    "message"    => 'Success Add Data.'
	            );
			} else {
				$data = array(
	                    "success"    => TRUE,  
	                    "message"    => 'Success Updated Data.'
	            );
			}
			
		} else {
			$data = array(
                    "success"    => FALSE,  
                    "message"    => 'Failed Add Or Update Data.'
            );
		}	
	    echo json_encode($data);
    }
    public function deleteDataSchedule()
    {
    	$id= $this->input->post('id');
		$query = $this->Model_classstaff->cekExistAttendance($id);
		if ($query > 0) {
			$data = array(
                    "success"    => FALSE,  
                    "message"    => "You can't remove because this schedule used by attendance resident. Please delete attendance first"
                    );
		} else {
			$delete = $this->Model_classstaff->deleteDataSchedule($id);
			if ($delete) {
				$data = array(
                    "success"    => TRUE,  
                    "message"    => 'Success Deleted.'
                );
			} else {
				$data = array(
                    "success"    => FALSE,  
                    "message"    => 'Failed Deleted.'
                );
			}	
		}
	    echo json_encode($data);
    }
    public function ResidentDelete()
    {
    	$id= $this->input->post('id');
    	$classstaff= $this->input->post('classstaff');

    	$delete = $this->Model_classstaff->deleteResident($id,$classstaff);
		if ($delete) {
			$data = array(
                "success"    => TRUE,  
                "message"    => 'Success Deleted.'
            );
		} else {
			$data = array(
                "success"    => FALSE,  
                "message"    => 'Failed Deleted.'
            );
		}	
	    echo json_encode($data);

    }
    public function InsertResident()
    {
    	$id= $this->input->post('id');
    	$classstaff= $this->input->post('classstaff');

    	$insert = $this->Model_classstaff->InsertResident($id,$classstaff);
		if ($insert) {
			$data = array(
                "success"    => TRUE,  
                "message"    => 'Success Inserted.'
            );
		} else {
			$data = array(
                "success"    => FALSE,  
                "message"    => 'Failed Inserted.'
            );
		}
	    echo json_encode($data);
			
    }
    public function AttendanceInsert(){
      $dataresident  = $this->input->post('value');
      $id_class_staff= $this->input->post('id_class_staff');
      $id_scs 		 = $this->input->post('id_scs');
      $status 		 = $this->input->post('status');
      foreach ($dataresident as $key => $value) {
	  		$newdata = Array (			
					'id_class_staff'=> $id_class_staff,
					'id_scs'	    => $id_scs,
					'id_resident'	=> $value,
					'date'	 		=> date('Y-m-d'),
					'time'			=> date('H:i'),
					'status'	    => $status,
			);
			$this->db->insert('attendance_resident', $newdata);
      }
      $data = array(
                "success"    => TRUE,  
                "message"    => 'Success Inserted'
            );
      echo json_encode($data);
    }
}

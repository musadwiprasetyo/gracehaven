<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller {

	function __construct() 
	{
		parent::__construct();
		
		$this->load->model('Model_staff','',TRUE);
		$this->load->model('Model_group','',TRUE);

		if ($this->session->userdata('login')!=TRUE) {
    		redirect('login');
    	} 
	}
   private function status()
	{
		$status = array('0'=>'Disable','1'=>'Active');
		return $status;
	}

	public function index()
	{
		$data['judul']		= "Gracehaven-Staff";	
		if ($this->acl->has_permission('staffs')) {
			$this->template->display('backend/staff/index', $data);
		} else {
			$this->load->view('errors/html/error_access', $data);
		}
	}
	public function getData()
    {
		$query = $this->Model_staff->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($query as $key=> $staff) {
			$enc_id = $this->Model_staff->safe_encode($this->encryption->encrypt($staff->id));

			$action = "";
			$action .="<p class='nomargin'>";
			
			if ($this->acl->has_permission('staff-edit')) {
		        $action .='<a href="staff/edit/'.$enc_id.'" class="btn btn-primary btn-xs" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>&nbsp;';  
			}
			if ($this->acl->has_permission('staff-delete')) { 
				if ($staff->id!=$this->session->userdata('user_id')) {
					$action .='<a href="javascript:void(0)" class="btn btn-danger btn-xs"  onclick="DeleteFunction(this,\''.$staff->id.'\')" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></a>';
				} else{
					$action .='<a href="javascript:void(0)" class="btn btn-danger btn-xs"  onclick="" data-toggle="tooltip" title="Delete" disabled><i class="fa fa-trash-o"></i></a>';
				}
	        	
			} 
			$action .="</p'>";
			$no++;
			$row = array();
			$row['no'] 				= $no;
			$row['name'] 			= $staff->name;       
			$row['username']        = $staff->username;         
			$row['group']        	= $staff->group_name;         
			$row['action'] 			= $action;
			$data[] 				= $row;

		}
		if ($this->acl->has_permission('staffs')) {
			$output = array(
					"draw" 				=> $_POST['draw'],
					"recordsTotal" 		=> $this->Model_staff->count_all(),
					"recordsFiltered" 	=> $this->Model_staff->count_filtered(),
					"data" 				=> $data,
			);
		} else {
			$output = array(
				"draw" 				=> 0,
				"recordsTotal" 		=> 0,
				"recordsFiltered" 	=> 0,
				"data" 				=> null,
			);
		}
		
		echo json_encode($output);
    }
    public function form($enc_id=null)
	{
		
		$id = $this->encryption->decrypt($this->Model_staff->safe_decode($enc_id));
		$data['status'] = $this->status();
		if($id!=FALSE) {
			    $staff = $this->Model_staff->get_staff($id)->row();
				$data['staff'] 	  		= $this->Model_staff->get_staff($id)->row();
				$data['role'] 			= $this->Model_group->getrole();
				$data['judul'] 			= 'Gracehaben | Edit Data';
				$data['enc_id'] 		= $enc_id;
				$data['selectedrole']	= $staff->group_id;
				
				if ($this->acl->has_permission('staff-edit')) {
					$this->template->display('backend/staff/form', $data);
				} else {
					$this->load->view('errors/html/error_access', $data);
				}
				
		} else {
				$data['judul']			= "Gracehaven-Staff";
				$data['staff'] 		= null;
				$data['enc_id'] 		= null;
				$data['selected']		= 1;
				$data['role'] 			= $this->Model_group->getroleactive();
				$data['selectedrole']	= null;
				if ($this->acl->has_permission('staff-add')) {
					$this->template->display('backend/staff/form', $data);
				} else {
					$this->load->view('errors/html/error_access', $data);
				}
		}
	}
	 
	public function save($enc_id=null)
	{
		if ($enc_id) {
			
			$id = $this->encryption->decrypt($this->Model_staff->safe_decode($enc_id));
			$process = $this->Model_staff->processupdate($id);
			if($process==FALSE) {
				$this->session->set_flashdata('error',array('warning','Failed or no data updated. Try Again.','warning'));
				redirect('staff/edit/'.$enc_id.'');
			} else {
				$this->session->set_flashdata('error',array('success','Success Updated data','success'));
				redirect('staffs');
			}

		} else {
			$insert = $this->Model_staff->insert();
			if($insert==FALSE) {
				$this->session->set_flashdata('error',array('warning','Failed. Try Again.','warning'));
				redirect('staff/add');
			} else {
				$this->session->set_flashdata('error',array('success','Success Add Data','success'));
				redirect('staffs');
			}				
		}
	}
	public function Detail($enc_id)
	{
		$id = $this->encryption->decrypt($this->Model_staff->safe_decode($enc_id));
		if($id!=FALSE) {
				$data['staff'] = $this->Model_staff->get_staff($id)->row();
				$data['judul']= 'Detail';
				$data['enc_id'] = $enc_id;
				$this->template->display('backend/staff/detail', $data);
		} else {
			$this->session->set_flashdata('error',array('warning','Data not found','warning'));
			redirect('staffs');
		}
		
	}
	
    public function statusStaff()
    {
    	$id= $this->input->post('id');
    	$value= $this->input->post('value');
		$query = $this->Model_staff->updateStatusStaff($id,$value);
    	if($query==FALSE) {
    		 $data = array(
                    "success"    => FALSE,  
                    "message"    => 'Failed Updated.'
                    );
			
		} else {
			 $data = array(
                    "success"    => TRUE,  
                    "message"    => 'Success Updated.'
                    );
		}	
	    echo json_encode($data);
    }
    public function deleteStaff()
    {
    	$id= $this->input->post('id');
		
			$delete = $this->Model_staff->deleteStaff($id);
			if ($delete) {
				$data = array(
                    "success"    => TRUE,  
                    "message"    => 'Success Deleted.'
                );
			} else {
				$data = array(
                    "success"    => FALSE,  
                    "message"    => 'Failed Deleted.'
                );
			}	
		
	    echo json_encode($data);
    }
    public function profil()
    {
        $data['judul']		= "Gracehaven-Profil";
        $id= $this->session->userdata('user_id');
        $staff = $this->Model_staff->get_staff($id)->row();
		$data['staff'] 	  		= $this->Model_staff->get_staff($id)->row();
		$data['role'] 			= $this->Model_group->getrole();
		$data['judul'] 			= 'Gracehaben | Edit Data';
		$data['selectedrole']	= $staff->group_id;
		
        $this->template->display('backend/staff/profil', $data);
    }
    public function profilupdated()
	{
		$id= $this->session->userdata('user_id');
		if ($id) {
			$process = $this->Model_staff->processupdateprofil($id);
			if($process==FALSE) {
				$this->session->set_flashdata('error',array('warning','Failed or no data updated. Try Again.','warning'));
				redirect('profil');
			} else {
				$this->session->set_flashdata('error',array('success','Success Updated data','success'));
				redirect('profil');
			}

		}
	}
}

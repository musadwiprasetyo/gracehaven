<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends CI_Controller {

	function __construct() 
	{
		parent::__construct();
		$this->load->model('Model_group','',TRUE);
		if ($this->session->userdata('login')!=TRUE) {
    		redirect('login');
    	} 
	}
   private function status()
	{
		$status = array('0'=>'Disable','1'=>'Active');
		return $status;
	}

	public function index()
	{
		$data['judul']		= "Gracehaven-Group";	
		if ($this->acl->has_permission('groups')) {
			$this->template->display('backend/group/index', $data);
		} else {
			$this->load->view('errors/html/error_access', $data);
		}
	}
	public function getData()
    {
		$query = $this->Model_group->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($query as $key=> $group) {
			$enc_id = $this->Model_group->safe_encode($this->encryption->encrypt($group->group_id));

			$action = "";
			$action .="<p class='nomargin'>";
			if ($this->acl->has_permission('group-detail')) {
				$action .='<a href="group/detail/'.$enc_id.'" class="btn btn-success btn-xs" data-toggle="tooltip" title="Detail"><i class="fa fa-eye"></i></a>&nbsp;';
			} 
			if ($this->acl->has_permission('group-edit')) {
		        $action .='<a href="group/edit/'.$enc_id.'" class="btn btn-primary btn-xs" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>&nbsp;';  
			}
			if ($this->acl->has_permission('group-delete')) { 
	        	$action .='<a href="javascript:void(0)" class="btn btn-danger btn-xs"  onclick="DeleteFunction(this,\''.$group->group_id.'\')" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></a>';
			} 
			$action .="</p'>";

			$no++;
			$row = array();
			$row['no'] 					= $no;
			$row['name'] 				= $group->group_name;
			$row['action'] 				= $action;
			$data[] 					= $row;

		}
		if ($this->acl->has_permission('groups')) {
			$output = array(
					"draw" 				=> $_POST['draw'],
					"recordsTotal" 		=> $this->Model_group->count_all(),
					"recordsFiltered" 	=> $this->Model_group->count_filtered(),
					"data" 				=> $data,
			);
		} else {
			$output = array(
				"draw" 				=> 0,
				"recordsTotal" 		=> 0,
				"recordsFiltered" 	=> 0,
				"data" 				=> null,
			);
		}
		
		echo json_encode($output);
    }
    public function form($enc_id=null)
	{
		$module 			= $this->Model_group->getModule();
		$id = $this->encryption->decrypt($this->Model_group->safe_decode($enc_id));
		$data['status'] = $this->status();
		if($id!=FALSE) {
			  
				$data['group'] 			= $this->Model_group->get_role($id)->row();
				$data['judul'] 			= 'Gracehaven-Group Edit';
				$data['enc_id'] 		= $enc_id;
				$data['checkbox_loop'] 	= $this->checkbox_loop_checked($module,$id);
				if ($this->acl->has_permission('group-edit')) {
					$this->template->display('backend/group/form', $data);
				} else {
					$this->load->view('errors/html/error_access', $data);
				}
				
		} else {
				$data['judul']			= "Gracehaven-Group Add";
				$data['group'] 			= null;
				$data['enc_id'] 		= null;
				$data['checkbox_loop'] 	= $this->checkbox_loop_checked($module,$id=null);
				if ($this->acl->has_permission('group-add')) {
					$this->template->display('backend/group/form', $data);
				} else {
					$this->load->view('errors/html/error_access', $data);
				}
		}
	}
	 private function checkbox_loop_checked($data,$id=nulll)
    {
        $return = '<ul class="list-unstyled m-l-35">'."\n";
        foreach ($data as $row) {
        	$access = $this->Model_group->cekexistrole($id,$row->id);
           if($access) {
                $row->access = 1;
           }else{
                $row->access = 0;
           }
        	 $return .="<div class='ckbox ckbox-success'>";

            $return .= '<li data-id="'.$row->id.'">'."\n";
            $return .= '<input class="filled-in check_tree" type="checkbox" id="'.$row->name.'" data-id="'.$row->id.'" name="permission['.$row->id.']"'.($row->access ? "checked" : "").'/>'."\n";
            $return .= '<label for="'.$row->name.'">'.$row->name.'</label>'."\n";
            $return .= '</li>'."\n";
            $return .= '</div>'."\n";
        	
        }
        $return .= '</ul>'."\n";
        return $return;
    }
	public function save($enc_id=null)
	{
		if ($enc_id) {
			$module = $this->input->post('permission');
			$id = $this->encryption->decrypt($this->Model_group->safe_decode($enc_id));
			$process = $this->Model_group->processupdate($id);
			if($process==FALSE) {
				$this->session->set_flashdata('error',array('warning','Failed. Try Again.','warning'));
				redirect('group/form/'.$enc_id.'');
			} else {
				$this->session->set_flashdata('error',array('success','Success Updated data','success'));
				redirect('groups');
			}

		} else {
			$insert = $this->Model_group->insert();
			if($insert==FALSE) {
				$this->session->set_flashdata('error',array('warning','Failed. Try Again.','warning'));
				redirect('group/form');
			} else {
				$this->session->set_flashdata('error',array('success','Success Add Data','success'));
				redirect('groups');
			}				
		}
	}
	public function Detail($enc_id)
	{
		$id = $this->encryption->decrypt($this->Model_group->safe_decode($enc_id));
		if($id!=FALSE) {
				$data['group'] = $this->Model_group->get_role($id)->row();
				$data['judul']= 'Gracehaven | Detail Group';
				$data['enc_id'] = $enc_id;
				$this->template->display('backend/group/detail', $data);
		} else {
			$this->session->set_flashdata('error',array('warning','Data tidak ditemukan','warning'));
			redirect('roles');
		}
		
	}
	public function getDataPermission($enc_id)
    {
		$id = $this->encryption->decrypt($this->Model_group->safe_decode($enc_id));
		$query = $this->Model_group->get_datatables_role_permissions($id);

		// var_dump($query);
		$data = array();
		$no = $_POST['start'];
		foreach ($query as $key=> $rolepermission) {
			$action = "";
			$no++;
			$row = array();
			$row['no'] 				= $no;
			$row['key'] 			= $rolepermission->key;
			$row['name'] 			= $rolepermission->name;
			$row['description']     = $rolepermission->description;
			$row['group']			= $rolepermission->group;
			$row['status']			= $rolepermission->statuspermission=='1' ? 'Active' : 'Disable' ;
			$row['action'] 			= $action;
			$data[] 				= $row;
		}
		
		$output = array(
					"draw" 				=> $_POST['draw'],
					"recordsTotal" 		=> $this->Model_group->count_all_role_permissions($id),
					"recordsFiltered" 	=> $this->Model_group->count_filtered_role_permissions($id),
					"data" 				=> $data,
				);
		echo json_encode($output);
    }
    public function getDataUser($enc_id)
    {
		$id = $this->encryption->decrypt($this->Model_group->safe_decode($enc_id));
		$query = $this->Model_group->get_datatables_role_user($id);

		// var_dump($query);
		$data = array();
		$no = $_POST['start'];
		foreach ($query as $key=> $roleuser) {
			$action = "";
			$no++;
			$row = array();
			$row['no'] 				= $no;
			$row['name'] 			= $roleuser->name;
			$data[] 				= $row;
		}
		
		$output = array(
					"draw" 				=> $_POST['draw'],
					"recordsTotal" 		=> $this->Model_group->count_all_role_user($id),
					"recordsFiltered" 	=> $this->Model_group->count_filtered_role_user($id),
					"data" 				=> $data,
				);
		echo json_encode($output);
    }
    
    public function deleteRole()
    {
    	$id= $this->input->post('id');
		$query = $this->Model_group->cekRole($id);
		if ($query > 0) {
			$data = array(
                    "success"    => FALSE,  
                    "message"    => "You can't remove because this role used by other user. Please disable if you not used."
                    );
		} else {
			$delete = $this->Model_group->deleteRole($id);
			if ($delete) {
				$data = array(
                    "success"    => TRUE,  
                    "message"    => 'Success Deleted.'
                );
			} else {
				$data = array(
                    "success"    => FALSE,  
                    "message"    => 'Failed Deleted.'
                );
			}	
		}
	    echo json_encode($data);
    }
}

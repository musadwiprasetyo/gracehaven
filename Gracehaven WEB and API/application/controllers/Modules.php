<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modules extends CI_Controller {

	function __construct() 
	{
		parent::__construct();
		
		$this->load->model('Model_modules','',TRUE);
		$this->load->model('Model_group','',TRUE);
		if ($this->session->userdata('login')!=TRUE) {
    		redirect('login');
    	} 
	}
   private function status()
	{
		$status = array('0'=>'Disable','1'=>'Active');
		return $status;
	}

	public function index()
	{
		$data['judul']		= "Gracehaven-Modules";	
		if ($this->acl->has_permission('modules')) {
			$this->template->display('backend/modules/index', $data);
		} else {
			$this->load->view('errors/html/error_access', $data);
		}
	}
	public function getData()
    {
		$query = $this->Model_modules->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($query as $key=> $module) {
			$enc_id = $this->Model_modules->safe_encode($this->encryption->encrypt($module->id));

			$action = "";
			$action .="<p class='nomargin'>";
			if ($this->acl->has_permission('module-detail')) {
				$action .='<a href="module/detail/'.$enc_id.'" class="btn btn-success btn-xs" data-toggle="tooltip" title="Detail"><i class="fa fa-eye"></i></a>&nbsp;';
			} 
			if ($this->acl->has_permission('module-edit')) {
		        $action .='<a href="module/edit/'.$enc_id.'" class="btn btn-primary btn-xs" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>&nbsp;';  
			}
			if ($this->acl->has_permission('module-delete')) { 
	        	$action .='<a href="javascript:void(0)" class="btn btn-danger btn-xs"  onclick="DeleteFunction(this,\''.$module->id.'\')" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></a>';
			} 
			$action .="</p'>";
			$no++;
			$row = array();
			$row['no'] 				= $no;
			$row['name'] 			= $module->name;
			$row['description']     = $module->description;         
			         
			$row['status']			= $module->statuspermission=='1' ? '<div class="ckbox ckbox-success"><input type="checkbox" id="status'.$module->id.'" required="" name="int['.$module->id.']" checked onclick="statusFunction(this,\''.$module->id.'\',0)""><label for="status'.$module->id.'"></label></div>' : '<div class="ckbox ckbox-success"><input type="checkbox" id="statusx'.$module->id.'" name="int[]" onclick="statusFunction(this,\''.$module->id.'\',1)"><label for="statusx'.$module->id.'"></label></div>' ;
			$row['action'] 			= $action;
			$data[] 				= $row;

		}
		if ($this->acl->has_permission('modules')) {
			$output = array(
					"draw" 				=> $_POST['draw'],
					"recordsTotal" 		=> $this->Model_modules->count_all(),
					"recordsFiltered" 	=> $this->Model_modules->count_filtered(),
					"data" 				=> $data,
			);
		} else {
			$output = array(
				"draw" 				=> 0,
				"recordsTotal" 		=> 0,
				"recordsFiltered" 	=> 0,
				"data" 				=> null,
			);
		}
		
		echo json_encode($output);
    }
    public function form($enc_id=null)
	{
		
		$id = $this->encryption->decrypt($this->Model_modules->safe_decode($enc_id));
		$data['status'] = $this->status();
		if($id!=FALSE) {
			    $module = $this->Model_modules->get_module($id)->row();
				$data['module'] 	  	= $this->Model_modules->get_module($id)->row();
				
				$data['judul'] 			= 'Edit Data';
				$data['enc_id'] 		= $enc_id;
				$data['selected']		= $module->statuspermission;
				$data['selectedgroup']	= $module->group;
				
				if ($this->acl->has_permission('module-edit')) {
					$this->template->display('backend/modules/form', $data);
				} else {
					$this->load->view('errors/html/error_access', $data);
				}
				
		} else {
				$data['judul']			= "Gracehaven-Modules";
				$data['module'] 		= null;
				$data['enc_id'] 		= null;
				$data['selected']		= 1;
				
				$data['selectedgroup']	= null;
				if ($this->acl->has_permission('module-add')) {
					$this->template->display('backend/modules/form', $data);
				} else {
					$this->load->view('errors/html/error_access', $data);
				}
		}
	}
	 
	public function save($enc_id=null)
	{
		if ($enc_id) {
			
			$id = $this->encryption->decrypt($this->Model_modules->safe_decode($enc_id));
			$process = $this->Model_modules->processupdate($id);
			if($process==FALSE) {
				$this->session->set_flashdata('error',array('warning','Failed. Try Again.','warning'));
				redirect('module/edit/'.$enc_id.'');
			} else {
				$this->session->set_flashdata('error',array('success','Success Updated data','success'));
				redirect('modules');
			}

		} else {
			$insert = $this->Model_modules->insert();
			if($insert==FALSE) {
				$this->session->set_flashdata('error',array('warning','Failed. Try Again.','warning'));
				redirect('module/add');
			} else {
				$this->session->set_flashdata('error',array('success','Success Add Data','success'));
				redirect('modules');
			}				
		}
	}
	public function Detail($enc_id)
	{
		$id = $this->encryption->decrypt($this->Model_modules->safe_decode($enc_id));
		if($id!=FALSE) {
				$data['module'] = $this->Model_modules->get_module($id)->row();
				$data['judul']= 'Detail';
				$data['enc_id'] = $enc_id;
				$this->template->display('backend/modules/detail', $data);
		} else {
			$this->session->set_flashdata('error',array('warning','Data not found','warning'));
			redirect('modules');
		}
		
	}
	
    public function statusModule()
    {
    	$id= $this->input->post('id');
    	$value= $this->input->post('value');
		$query = $this->Model_modules->updateStatusModule($id,$value);
    	if($query==FALSE) {
    		 $data = array(
                    "success"    => FALSE,  
                    "message"    => 'Failed Updated.'
                    );
			
		} else {
			 $data = array(
                    "success"    => TRUE,  
                    "message"    => 'Success Updated.'
                    );
		}	
	    echo json_encode($data);
    }
    public function deleteModule()
    {
    	$id= $this->input->post('id');
		$query = $this->Model_modules->cekModule($id);
		if ($query > 0) {
			$data = array(
                    "success"    => FALSE,  
                    "message"    => "You can't remove because this data used by other user. Please disable if you not used."
                    );
		} else {
			$delete = $this->Model_modules->deleteModule($id);
			if ($delete) {
				$data = array(
                    "success"    => TRUE,  
                    "message"    => 'Success Deleted.'
                );
			} else {
				$data = array(
                    "success"    => FALSE,  
                    "message"    => 'Failed Deleted.'
                );
			}	
		}
	    echo json_encode($data);
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct() 
	{
		parent::__construct();
		
		if ($this->session->userdata('login')!=TRUE) {
    		redirect('login');
    	} 
	}
	public function index()
	{
		$data['judul']		= "Gracehaven | Home";	
		$this->template->display('backend/beranda/index', $data);
	}
	

}

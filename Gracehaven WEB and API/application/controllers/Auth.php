<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct() 
	{
		parent::__construct();
		$this->load->model('Model_auth','',TRUE);
	}
	public function index()
    {
    	if ($this->session->userdata('login')==TRUE) {
    		redirect('home');
    	} else {
    		 $this->load->view('backend/login');
    	}
    }
    public function cekAuth()
    {
    	$username = $this->input->post('username');
    	$password = $this->input->post('password');

    	if ($this->Model_auth->cekAuth()) {
			$this->Model_auth->logger(1);					
			redirect('home');
		}else{
			$this->Model_auth->logger();
			$this->session->set_flashdata('error',array('warning','Login failed. Please try again or contact administrator','warning'));
    		$this->load->view('backend/login');
        }
    }
    public function logout()
	{
        $this->Model_auth->logout();
		redirect('login');
	}
	
}

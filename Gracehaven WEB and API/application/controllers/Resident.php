<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resident extends CI_Controller {

	function __construct() 
	{
		parent::__construct();
		
		$this->load->model('Model_resident','',TRUE);
		$this->load->model('Model_group','',TRUE);

		if ($this->session->userdata('login')!=TRUE) {
    		redirect('login');
    	} 
	}
  

	public function index()
	{
		$data['judul']		= "Gracehaven-resident";	
		if ($this->acl->has_permission('resident')) {
			$this->template->display('backend/resident/index', $data);
		} else {
			$this->load->view('errors/html/error_access', $data);
		}
	}
	public function getData()
    {
		$query = $this->Model_resident->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($query as $key=> $resident) {
			$enc_id = $this->Model_resident->safe_encode($this->encryption->encrypt($resident->id));

			$action = "";
			$action .="<p class='nomargin'>";
			
			if ($this->acl->has_permission('resident-edit')) {
		        $action .='<a href="resident/edit/'.$enc_id.'" class="btn btn-primary btn-xs" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>&nbsp;';  
			}
			if ($this->acl->has_permission('resident-delete')) { 
				
				$action .='<a href="javascript:void(0)" class="btn btn-danger btn-xs"  onclick="DeleteFunction(this,\''.$resident->id.'\')" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></a>';	        	
			} 
			$action .="</p'>";
			$no++;
			$row = array();
			$row['no'] 				= $no;
			$row['name'] 			= $resident->name;       
			$row['username']        = $resident->username;          
			$row['action'] 			= $action;
			$data[] 				= $row;

		}
		if ($this->acl->has_permission('resident')) {
			$output = array(
					"draw" 				=> $_POST['draw'],
					"recordsTotal" 		=> $this->Model_resident->count_all(),
					"recordsFiltered" 	=> $this->Model_resident->count_filtered(),
					"data" 				=> $data,
			);
		} else {
			$output = array(
				"draw" 				=> 0,
				"recordsTotal" 		=> 0,
				"recordsFiltered" 	=> 0,
				"data" 				=> null,
			);
		}
		
		echo json_encode($output);
    }
    public function form($enc_id=null)
	{
		
		$id = $this->encryption->decrypt($this->Model_resident->safe_decode($enc_id));
		if($id!=FALSE) {
		
				$data['resident'] 	  	= $this->Model_resident->get_resident($id)->row();
				$data['role'] 			= $this->Model_group->getrole();
				$data['judul'] 			= 'Gracehaven | Edit Data';
				$data['enc_id'] 		= $enc_id;
				
				
				if ($this->acl->has_permission('resident-edit')) {
					$this->template->display('backend/resident/form', $data);
				} else {
					$this->load->view('errors/html/error_access', $data);
				}
				
		} else {
				$data['judul']			= "Gracehaven-Resident";
				$data['resident'] 			= null;
				$data['enc_id'] 		= null;
				if ($this->acl->has_permission('resident-add')) {
					$this->template->display('backend/resident/form', $data);
				} else {
					$this->load->view('errors/html/error_access', $data);
				}
		}
	}
	 
	public function save($enc_id=null)
	{
		if ($enc_id) {
			
			$id = $this->encryption->decrypt($this->Model_resident->safe_decode($enc_id));
			$process = $this->Model_resident->processupdate($id);
			if($process==FALSE) {
				$this->session->set_flashdata('error',array('warning','Failed or no data updated. Try Again.','warning'));
				redirect('resident/edit/'.$enc_id.'');
			} else {
				$this->session->set_flashdata('error',array('success','Success Updated data','success'));
				redirect('resident');
			}

		} else {
			$insert = $this->Model_resident->insert();
			if($insert==FALSE) {
				$this->session->set_flashdata('error',array('warning','Failed. Try Again.','warning'));
				redirect('resident/add');
			} else {
				$this->session->set_flashdata('error',array('success','Success Add Data','success'));
				redirect('resident');
			}				
		}
	}
	public function Detail($enc_id)
	{
		$id = $this->encryption->decrypt($this->Model_resident->safe_decode($enc_id));
		if($id!=FALSE) {
				$data['staff'] = $this->Model_resident->get_staff($id)->row();
				$data['judul']= 'Detail';
				$data['enc_id'] = $enc_id;
				$this->template->display('backend/resident/detail', $data);
		} else {
			$this->session->set_flashdata('error',array('warning','Data not found','warning'));
			redirect('resident');
		}
		
	}
	
    public function statusresident()
    {
    	$id= $this->input->post('id');
    	$value= $this->input->post('value');
		$query = $this->Model_resident->updateStatusresident($id,$value);
    	if($query==FALSE) {
    		 $data = array(
                    "success"    => FALSE,  
                    "message"    => 'Failed Updated.'
                    );
			
		} else {
			 $data = array(
                    "success"    => TRUE,  
                    "message"    => 'Success Updated.'
                    );
		}	
	    echo json_encode($data);
    }
    public function deleteResident()
    {
    	$id= $this->input->post('id');
		
			$delete = $this->Model_resident->deleteResident($id);
			if ($delete) {
				$data = array(
                    "success"    => TRUE,  
                    "message"    => 'Success Deleted.'
                );
			} else {
				$data = array(
                    "success"    => FALSE,  
                    "message"    => 'Failed Deleted.'
                );
			}	
		
	    echo json_encode($data);
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiController extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        header("Content-Type: application/json");
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        header("Access-Control-Allow-Headers: Content-Type,Access-Control-Allow-Headers,Authorization,X-Requested-With");
        $this->load->model('UserModel');    
        $this->load->model('ApiModel');   
    }
	public function index()
	{
     
      $post_data = file_get_contents("php://input");
      $request = json_decode($post_data,true);
  
      $username = $request['username'];
      $password = $request['password']; 

      // $username = $this->input->post('username');
      // $password = $this->input->post('password');

	    $cek = $this->UserModel->getLogin($username,$password);
      
        if (!empty($cek->num_rows())) {
            $get = $cek->row();
            if ($get->token==null) {
                    $token = date("YmdHis").$get->password;
                    $newdata = Array (          
                        'token'             => $token
                    );
                    $this->db->where('id',$get->id);
                    $this->db->update('users', $newdata);
                    $response = array(
                          'success' =>'TRUE',
                          'id'      =>$get->user_id,
                          'username'=>$get->username,
                          'name'    =>$get->name,
                          'token'   =>$token,
                          'type'    =>$get->type,
                          'info'    =>'Success Login'
                    );
            } else {
                 $response = array(
                          'success' =>'FALSE',
                          'token'   =>$get->token,
                          'info'    =>'Login Duplicate'
                );
            }        
        }else{ 
            $response = array(
                      'success' => 'FALSE',
                      'token'   => null,
                      'info'    =>'Username or Password incorrect'

                      );
        }
        echo json_encode($response);
	}
  public function testindex()
  {
     
      $post_data = file_get_contents("php://input");
      $request = json_decode($post_data,true);
  
      // $password = sha1($request['password']); 
      // $username = $request['username'];
      // $password = $request['password']; 

      $username = $this->input->post('username');
      $password = $this->input->post('password');
      $cek = $this->UserModel->getLogin($username,$password);

        if (!empty($cek->num_rows())) {
            $get = $cek->row();
            if ($get->token==null) {
                    $token = date("YmdHis").$get->password;
                    $newdata = Array (          
                        'token'             => $token
                    );
                    $this->db->where('id',$get->id);
                    $this->db->update('staff', $newdata);
                    $response = array(
                          'success' =>'TRUE',
                          'id'      =>$get->id,
                          'username'=>$get->username,
                          'name'    =>$get->name,
                          'token'   =>$token,
                          'info'    =>'Success Login'
                    );
            } else {
                 $response = array(
                          'success' =>'FALSE',
                          'token'   =>$get->token,
                          'info'    =>'Login Duplicate'
                );
            }        
        }else{ 
            $response = array(
                      'success' => 'FALSE',
                      'token'   => null,
                      'info'    =>'Username or Password incorrect'

                      );
        }
        echo json_encode($response);
  }
  public function getCategoryRoutine()
  {
      
      $category = $this->ApiModel->getCategoryRoutine();
      if ($category->num_rows() > 0) {
          $response = array(
                      'success'        =>'TRUE',
                      'categoryroutine'=>$category->result()
          );
      } else {
          $response = array(
                      'success'        =>'FALSE',
                      'categoryroutine'=>[]
          );
      }
      echo json_encode($response);
  }
  public function getRoutine($id)
  {
      $routine = $this->ApiModel->getRoutine($id);
      if ($routine->num_rows() > 0) {
          $response = array(
                      'success'   =>'TRUE',
                      'routine'   =>$routine->result()
          );
      } else {
          $response = array(
                      'success'    =>'FALSE',
                      'routine'    =>[]
          );
      }
      echo json_encode($response);
  }
  public function getResident()
  {
      $resident = $this->ApiModel->getResident();
      if ($resident->num_rows() > 0) {
          $response = array(
                      'resident'      =>$resident->result()
          );
      } else {
          $response = array(
                      'resident'=>[]
          );
      }
      echo json_encode($response);
  }
  public function getClass()
  {
      $class = $this->ApiModel->getClass();
      if ($class->num_rows() > 0) {
          $response = array(
                      'class'      =>$class->result()
          );
      } else {
          $response = array(
                      'class'=>[]
          );
      }
      echo json_encode($response);
  }
  public function processClass()
  {
        $post_data = file_get_contents("php://input");
        $request = json_decode($post_data,true);

        $classname    = $request['classname'];
        $classid      = $request['classid'];
        if ($classid == NULL) {
            $newdata = Array (         
                'class_name'  => $classname
            );
            $process=$this->db->insert('class', $newdata);    
        } else {
            $data = array(
                'class_name' => $classname
            );
            $this->db->where('class_id', $classid);
            $process= $this->db->update('class', $data);
            
        }
        if ($process) {
            $response = array(
                'success' =>'TRUE',
                'info'    =>'Success Process'
            );
        } else {
            $response = array(
                'success' =>'FALSE',
                'info'    =>'Ups, Failed'
            );
        }
       echo json_encode($response);
  }
  public function deleteClass()
  {
        $post_data = file_get_contents("php://input");
        $request = json_decode($post_data,true);
        $classid      = $request['classid'];
        // $classid      = $this->input->post('classid');

       
        $check = $this->ApiModel->checkClassStaff($classid);
        if ($check->num_rows() > 0) {
            $response = array(
                        'success'        =>'FALSE',
                        'info'        =>"Can't delete because this class linking to class staff"
            );
        } else {
            $this->db->where('class_id', $classid);
            $process= $this->db->delete('class');
            
            if ($process) {
                $response = array(
                    'success' =>'TRUE',
                    'info'    =>'Success Deleted'
                );
            } else {
                $response = array(
                    'success' =>'FALSE',
                    'info'    =>'Ups, Failed'
                );
            }
        }  
       echo json_encode($response);
  }
  public function getClassStaff()
  {
      $class = $this->ApiModel->getClassStaff();
      if ($class->num_rows() > 0) {
          $response = array(
                      'class'      =>$class->result()
          );
      } else {
          $response = array(
                      'class'=>[]
          );
      }
      echo json_encode($response);
  }
  public function getClassStaffToday($id)
  {
      $dy=date('w');
      $class = $this->ApiModel->getClassStaffToday($dy,$id);
      if ($class->num_rows() > 0) {
          $response = array(
                      'class'      =>$class->result()
          );
      } else {
          $response = array(
                      'class'=>[]
          );
      }
      echo json_encode($response);
  }
  public function getClassResidentToday($id)
  {
      $dy=date('w');
      $class = $this->ApiModel->getClassResidentToday($dy,$id);
      if ($class->num_rows() > 0) {
          $response = array(
                      'class'      =>$class->result()
          );
      } else {
          $response = array(
                      'class'=>[]
          );
      }
      echo json_encode($response);
  }
  public function getClassStaffByUser($id)
  {
      $class = $this->ApiModel->getClassStaffByUser($id);
      if ($class->num_rows() > 0) {
          $response = array(
                      'class'      =>$class->result()
          );
      } else {
          $response = array(
                      'class'=>[]
          );
      }
      echo json_encode($response);
  }
  public function getClassStaffSchedule($id)
  {
      $schedule = $this->ApiModel->getClassStaffSchedule($id);
      if ($schedule->num_rows() > 0) {
          $response = array(
                      'schedule'      =>$schedule->result()
          );
      } else {
          $response = array(
                      'schedule'=>[]
          );
      }
      echo json_encode($response);
  }
  public function getDataAttendance($id)
  {
    $attendance = $this->ApiModel->getDataAttendance($id);
    if ($attendance->num_rows() > 0) {
        $response = array(
                    'attendance'      =>$attendance->result()
        );
    } else {
        $response = array(
                    'attendance'=>[]
        );
    }
    echo json_encode($response);
  }
  
  public function getRoutineInput()
  {

      $post_data = file_get_contents("php://input");
      $request = json_decode($post_data,true);

      $catid    = $this->input->post('catid');

      $routine = $this->ApiModel->getRoutineInput($catid);
      if ($routine->num_rows() > 0) {
          $response = array(
                      'success'        =>'TRUE',
                      'routine'      =>$routine->result()
          );
      } else {
          $response = array(
                      'success'        =>'FALSE',
                      'routine'=>[]
          );
      }
      echo json_encode($response);
  }
  public function getRoutineRecord()
  {
    
     $record = $this->ApiModel->getRoutineRecord();
      if ($record->num_rows() > 0) {
          $response = array(
                      'success'        =>'TRUE',
                      'record'      =>$record->result()
          );
      } else {
          $response = array(
                      'success'        =>'FALSE',
                      'record'=>[]
          );
      }
      echo json_encode($response);
  }
  public function AttendanceTaking()
  {
    $post_data = file_get_contents("php://input");
    $request = json_decode($post_data,true);

    $userid         = $request['userid'];
    $idclasstaff    = $request['idclasstaff'];
    $idscs          = $request['idscs'];
    $date           = date('Y-m-d');
    
    // $userid         = $this->input->post('userid');
    // $idclasstaff    = $this->input->post('idclassstaff');
    // $idscs          = $this->input->post('idscs');
    // $date           = date('Y-m-d');
   
   
    $getResident        = $this->ApiModel->getRes($userid);
   
    if ($getResident->num_rows() > 0) {
        $checkAttendance = $this->ApiModel->checkAttentanceExist($date,$userid,$idclasstaff,$idscs)->num_rows();
        if ($checkAttendance) {
            $response = array(
                'success' =>'FALSE',
                'info'    =>'Ups. Resident Already Attendance'
            );
        }else{
              $newdata = Array (         
                  'id_class_staff'       => $idclasstaff,
                  'id_scs'               => $idscs,
                  'id_resident'          => $userid,
                  'date'                 => $date,
                  'time'                 => date('H:i:s'),
                  'status'               => 'present',
              );
           $this->db->insert('attendance_resident', $newdata);
           $response = array(
            'success' =>'TRUE',
            'info'    =>'Success Attendance Takings'
            );
        }
    }else{
        $response = array(
            'success' =>'FALSE',
            'info'    =>'Resident not found.'
        );
    }    
    echo json_encode($response); 
  }
  public function logout(){
    $post_data = file_get_contents("php://input");
    $request = json_decode($post_data,true);
    $userid = $request['userid'];
    $newdata = Array (          
                'token'        => NULL
            );
    $this->db->where('user_id',$userid);
    $this->db->update('users', $newdata);
    if ($this->db->affected_rows() > 0) {
      $response = array(
          'success' =>'TRUE',
          'info'    =>'Logout Success'
      );
      
    } else {
       $response = array(
          'success' =>'FALSE',
          'info'    =>'Please. Try Again to logout'
      );
    }                                                                                                              
      echo json_encode($response);
  }
  public function getAttendanceSetting()
  {
    $query = $this->ApiModel->getAttendanceSetting()->row();
    return $query;
  }
  public function getLateCharge($start,$end)
  {
    $start_time  = strtotime($start);
    $end_time = strtotime($end);
    $minutes  = ($end_time - $start_time) / 60;
    return $minutes;
  }

  //Resident
  public function profilresident()
  {
    $post_data = file_get_contents("php://input");
    $request = json_decode($post_data,true);


    $residentid   = $request['userid'];
    // $residentid = $this->input->get('userid');
    // $residentid=1;
    $profil = $this->ApiModel->profilResident($residentid)->row_array();
    if ($profil) {
      $response = array(
            'success' =>'TRUE',
            'info'    =>'Data Found',
            'data'    =>$profil
      );
    } else {
      $response = array(
            'success' =>'FALSE',
            'info'    =>'Not Found',
            'data'    =>[] 
      );
    }
    
    echo json_encode($response); 
  }
  //staff or admin
  public function profilstaff()
  {
    $post_data = file_get_contents("php://input");
    $request = json_decode($post_data,true);


    $staffid   = $request['userid'];
    $profil = $this->ApiModel->profilStaff($staffid)->row_array();
    if ($profil) {
      $response = array(
            'success' =>'TRUE',
            'info'    =>'Data Found',
            'data'    =>$profil
      );
    } else {
      $response = array(
            'success' =>'FALSE',
            'info'    =>'Not Found',
            'data'    =>[] 
      );
    }
    
    echo json_encode($response); 
  }
  public function masterstaff()
  {
    $staff = $this->ApiModel->masterstaff();
    $this->db->select_min('id');
    $query = $this->db->get('staff');
    if ($staff) {
      $response = array(
            'default' =>$query->row('id'), 
            'data'    =>$staff->result()
      );
    } else {
      $response = array(
            'default' =>[],
            'data'    =>[] 
      );
    }
    echo json_encode($response); 
  }
  public function masterclass()
  {
    $class = $this->ApiModel->getClass();
    $this->db->select_min('class_id');
    $query = $this->db->get('class');
    if ($class) {
      $response = array(
            'default' =>$query->row('class_id'), 
            'data'    =>$class->result()
      );
    } else {
      $response = array(
            'default' =>[],
            'data'    =>[] 
      );
    }
    echo json_encode($response);

  }
  public function masterday()
  {
    $day = $this->ApiModel->getDay();
    $this->db->select_min('id');
    $query = $this->db->get('day');
    if ($day) {
      $response = array(
            'default' =>$query->row('id'), 
            'data'    =>$day->result()
      );
    } else {
      $response = array(
            'default' =>[],
            'data'    =>[] 
      );
    }
    echo json_encode($response);
  }
  public function masterresident()
  {
    $resident = $this->ApiModel->masterresident();
    $this->db->select_min('id');
    $query = $this->db->get('resident');
    if ($resident) {
      $response = array(
            'default' =>$query->row('id'), 
            'data'    =>$resident->result()
      );
    } else {
      $response = array(
            'default' =>[],
            'data'    =>[] 
      );
    }
    echo json_encode($response); 
  }
  //process class staff
  public function processClassStaffx()
  {
        $post_data = file_get_contents("php://input");
        $request = json_decode($post_data,true);
        $response = array(
                'success' =>'TRUE',
                'info'    =>$request
            );
       echo json_encode($response);

        // $classstaffid=NULL;
        // $dayid ='1';
        // $classid='1';
        // $staffid='1';
        // $clock  ='10:00-12:00';

        // $classname    = $request['classname'];
        // $classid      = $request['classid'];
       //  if ($classstaffid == NULL) {
       //      $newdata = Array (         
       //          'id_class '  =>$classid,
       //          'id_staff'   =>$staffid,
       //          'day'        =>$dayid,
       //          'clock'      =>$clock
       //      );
       //      $process=$this->db->insert('class_staff', $newdata);    
       //  } else {
       //      $newdata = Array (         
       //          'id_class '  =>$classid,
       //          'id_staff'   =>$staffid,
       //          'day'        =>$dayid,
       //          'clock'      =>$clock
       //      );
       //      $this->db->where('id', $classstaffid);
       //      $process= $this->db->update('class_staff', $data);
            
       //  }
       //  if ($process) {
       //      $response = array(
       //          'success' =>'TRUE',
       //          'info'    =>'Success Process'
       //      );
       //  } else {
       //      $response = array(
       //          'success' =>'FALSE',
       //          'info'    =>'Ups, Failed'
       //      );
       //  }
       // echo json_encode($response);
  }
  public function processClassStaff()
  {
        
        $post_data = file_get_contents("php://input");
        $request = json_decode($post_data,true);

        $clock             = $request['clock'];
        $staffid           = $request['staffid'];
        $classid           = $request['classid'];
        $dayid             = $request['dayid'];
        $classstaffid      = $request['classstaffid'];

         if ($classstaffid == NULL) {
            $newdata = Array (         
                'id_class '  =>$classid,
                'id_staff'   =>$staffid,
                'day'        =>$dayid,
                'clock'      =>$clock
            );
            $process=$this->db->insert('class_staff', $newdata);    
        } else {
            $newdata = Array (         
                'id_class '  =>$classid,
                'id_staff'   =>$staffid,
                'day'        =>$dayid,
                'clock'      =>$clock
            );
            $this->db->where('id', $classstaffid);
            $process= $this->db->update('class_staff', $data);
            
        }
        if ($process) {
            $response = array(
                'success' =>'TRUE',
                'info'    =>'Success Process'
            );
        } else {
            $response = array(
                'success' =>'FALSE',
                'info'    =>'Ups, Failed'
            );
        }
       echo json_encode($response);
  }
  public function processSchedule()
  {
        
        $post_data = file_get_contents("php://input");
        $request = json_decode($post_data,true);

        $schedule          = $request['schedule'];
        $scheduledate      = $request['scheduledate'];
        // $newdate           = date('Y-m-d',strtotime($scheduledate));
        $note              = $request['note'];
        $idschedule        = $request['idschedule'];
        $idclassstaff      = $request['idclassstaff'];
        


        if ($idschedule == NULL) {
            $newdata = Array (         
                'id_class_staff'=>$idclassstaff,
                'schedule'      =>$schedule,
                'date'          =>$scheduledate,
                'note'          =>$note
            );
            $process=$this->db->insert('schedule_class_staff', $newdata);    
        } else {
            $newdata = Array (         
                'id_class_staff'=>$idclassstaff,
                'schedule'      =>$schedule,
                'date'          =>$scheduledate,
                'note'          =>$note
            );
            $this->db->where('id',$idschedule);
            $process= $this->db->update('schedule_class_staff',$newdata);
       }
        if ($process) {
            $response = array(
                'success' =>'TRUE',
                'info'    =>'Success Process'
            );
        } else {
            $response = array(
                'success' =>'FALSE',
                'info'    =>'Ups, Failed'
            );
        }
       echo json_encode($response);
  }
  //PBIS
  public function processPBIS()
  {
           
        $post_data = file_get_contents("php://input");
        $request = json_decode($post_data,true);
        
        
        $routine_id   = $request['routine_id'];
        $resident     = $request['resident'];
        $date         = $request['date'];
        $description  = $request['description'];
        $star         = $request['star'];
        $staff_id     = $request['staffid'];
        // $routine_id   = $this->input->post('routine_id');
        // $resident_id     = $this->input->post('resident');
        // $date         = $this->input->post('date');
        // $description  = $this->input->post('description');
        // $star         = $this->input->post('star');
        // $staff_id     = $this->input->post('staffid');
        $day          = date('w',strtotime($date));
       
        //check if date, routine_id,resident exist 
        $cekpbis=  $this->ApiModel->checkPBISDate($date,$routine_id,$resident);
         if ($cekpbis->num_rows() > 0) {
              $response = array(
                          'success' =>'FALSE',
                          'info'    =>'Record exist!'
              );
          } else {
              $data = array(
                       'routine_id' => $routine_id,
                       'date'       => $date,
                       'description'=> $description,
                       'resident_id'=> $resident,
                       'star'       => $star,
                       'day'        => $day,
                       'staff_id'   => $staff_id
              );
              $process= $this->db->insert('routine_record', $data);
              $response = array(
                          'success'        =>'TRUE',
                          'info'           =>'Success Inserted'
              );
          }
      
       echo json_encode($response);
  }
  
  //record PBIS
   public function recordPBIS()
  {
    $record = $this->ApiModel->recordPBIS();
    if ($record) {
      $response = array(
            'data'    =>$record->result()
      );
    } else {
      $response = array(
            'data'    =>[] 
      );
    }
    echo json_encode($response); 
  }
  public function detailrecordPBIS($date){
    $record = $this->ApiModel->detailrecordPBIS($date);
    if ($record) {
      $response = array(
            'data'    =>$record->result()
      );
    } else {
      $response = array(
            'data'    =>[] 
      );
    }
    echo json_encode($response); 
  }
   public function detailrecordPBISperResident($date,$resident){
    $record = $this->ApiModel->detailrecordPBISperResident($date,$resident);
    if ($record) {
        $response = array(
            'data'    =>$record->result()
        );
    } else {
        $response = array(
            'data'    =>[] 
        );
    }
    echo json_encode($response); 
  }
}
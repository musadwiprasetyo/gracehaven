<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model {

	public function getLogin($username,$password)
	{
		return $this->db->where('username', $username)
				 		->where('password', $password)
				 		->limit(1)
				 		->get('users');
	}
	public function getStaff($userid)
	{
		return $this->db->where('id', $userid)
				 		->limit(1)
				 		->get('staff');
	}
	public function getResident($residentid)
	{
		return $this->db->where('resident_id', $residentid)
				 		->limit(1)
				 		->get('resident');
	}
	
}

/* End of file UserModel.php */
/* Location: ./application/models/UserModel.php */
<?php
class Model_modules extends CI_Model
{
	var $table = 'permissions';
	var $column_order = array(null, 'name','description','statuspermission');  
	// column search
	var $column_search = array('name'); //set column field database for datatable searchable 
	// default sort by
	var $order = array('id' => 'asc','name'=>'asc'); 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
    private function _get_datatables_query()
	{
		
		$this->db->from($this->table);
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
		// fungsi pencarian
		$i = 0;
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->_get_datatables_query();
		return $this->db->count_all_results();
	}
	public function insert() {
		$newdata = Array (			
			'name'	 		    => $this->input->post('name'),
			'key'	 		    => $this->input->post('key'),
			'group'	 		    => $this->input->post('group'),
			'description' 		=> $this->input->post('description'),
			'order' 			=> $this->input->post('order'),
			'statuspermission' 	=> $this->input->post('status')
		);
		$this->db->insert('permissions', $newdata);
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return FALSE; 
		}
	}
	public function processupdate($id)
	{	
		$newdata = Array (			
			'name'	 		    => $this->input->post('name'),
			'key'	 		    => $this->input->post('key'),
			'group'	 		    => $this->input->post('group'),
			'description' 		=> $this->input->post('description'),
			'order' 			=> $this->input->post('order'),
			'statuspermission' 	=> $this->input->post('status')
		);
		$this->db->where('id', $id);
		$this->db->update('permissions', $newdata);
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return FALSE; 
		}

		

	}
	function safe_encode($string) {
	
        $data = str_replace(array('/'),array('_'),$string);
        return $data;
    }
 
	function safe_decode($string,$mode=null) {
		
		$data = str_replace(array('_'),array('/'),$string);
        return $data;
    }
	
	function get_module($id)
	{		
		return $this->db->where('id',$id)
						->get($this->table);
		
	}
	
	public function updateStatusModule($id,$value)
	{
		$nilaistatus = Array (
			'statuspermission' => $value
		);
		$this->db->where('id', $id)
				 ->update('permissions', $nilaistatus);
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return TRUE; 
		}
	}
	public function cekModule($id)
	{
	    $this->db->select("permissions.id,permissions.name");
		$this->db->join('role_permissions','role_permissions.permission_id = permissions.id','INNER');
		$this->db->where("permissions.id",$id);
		$this->db->from("permissions");
		$query = $this->db->get();
		return $query->num_rows();
	}
	public function cekexistmodule($id,$permissionsid)
	{
		$this->db->where("role_permissions.role_id",$id);
		$this->db->where("role_permissions.permission_id",$permissionsid);
		$this->db->from('role_permissions');	
		$query = $this->db->get();
		return $query->row();
	}
	public function deleteModule($id)
	{
	    $this->db->where('id', $id);
		$this->db->delete('permissions');
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return TRUE; 
		}
	}
	
}
?>
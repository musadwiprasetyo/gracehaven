<?php
class Model_pbis extends CI_Model
{
	var $table ='routine_record';
	var $column_order = array(null,'routine_record.id','resident.name','staff.name');  
	// column search
	var $column_search = array('resident.name','staff.name','day.day_name','category_routine.name'); //set column field database for datatable searchable 
	// default sort by
	var $order = array('routine_record.id' => 'asc','category_routine.id'=>'asc'); 

	public function __construct()
	{
		parent::__construct();
	}
    private function _get_datatables_query()
	{

		$this->db->select('routine_record.id,resident.name,
			category_routine.name as catname,day.day_name,routine_record.date,routine_record.description,routine_record.star,routine.default_star,staff.name as staffname,staff.id as staffid,routine.routine_name');
		$this->db->join('routine','routine.id = routine_record.routine_id','INNER');
		$this->db->join('category_routine','category_routine.id = routine.category_routine_id','INNER');
		$this->db->join('resident','resident.resident_id = routine_record.resident_id','INNER');
		$this->db->join('day','day.id = routine_record.day','INNER');
		$this->db->join('staff','staff.id = routine_record.staff_id','INNER');
		$this->db->from('routine_record');
	

		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
		// fungsi pencarian
		$i = 0;
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->_get_datatables_query();
		return $this->db->count_all_results();
	}
	
	function safe_encode($string) {
	
        $data = str_replace(array('/'),array('_'),$string);
        return $data;
    }
 
	function safe_decode($string,$mode=null) {
		
		$data = str_replace(array('_'),array('/'),$string);
        return $data;
    }
    public function categoryroutine()
    {

		return $this->db->get('category_routine');
    }
    public function subroutine()
    {

		return $this->db->get('routine');
    }
    
    public function residents()
    {

		return $this->db->get('resident');
    }
    public function insertUpdateSubmit() {

		$id 	= $this->input->post('id');
		$date 	= $this->input->post('date');
    	$day    = date('w',strtotime($date));
    	if ($id=='') {
    		$newdata = Array (			
					'routine_id'  => $this->input->post('routine_id'),
					'resident_id' => $this->input->post('resident_id'),
					'date' 		  => $this->input->post('date'),
					'day' 		  => $day,
					'description' => $this->input->post('description'),
					'star' 		  => $this->input->post('star'),
					'staff_id' 	  => $this->session->userdata('user_id')
			);
			$this->db->insert($this->table, $newdata);
    	} else {
    		$newdata = Array (			
					'routine_id'  => $this->input->post('routine_id'),
					'resident_id' => $this->input->post('resident_id'),
					'date' 		  => $this->input->post('date'),
					'day' 		  => $day,
					'description' => $this->input->post('description'),
					'star' 		  => $this->input->post('star'),
					'staff_id' 	  => $this->session->userdata('user_id')
			);
			$this->db->where('id', $id);
			$this->db->update($this->table, $newdata);
    	}
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return FALSE; 
		}
	}
	function getDataJson($id)
	{		
		
		$this->db->select('category_routine.id as catid,category_routine.name as catname,routine_record.id,routine_record.resident_id,routine_record.date,routine_record.description,routine_record.star,routine_record.routine_id');
		$this->db->from('routine_record');
		
		$this->db->join('routine','routine.id = routine_record.routine_id','INNER');
		$this->db->join('category_routine','category_routine.id= routine.category_routine_id','INNER');
		$this->db->where('routine_record.id',$id);
		$query = $this->db->get();
		return $query;
	}
	public function deleteData($id)
	{
	    $this->db->where('id', $id);
		$this->db->delete($this->table);
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return TRUE; 
		}
	}
}
?>
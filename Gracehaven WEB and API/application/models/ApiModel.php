<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiModel extends CI_Model {

	public function getCategoryRoutine()
	{
		return $this->db->from('category_routine')->get();
	}
	public function getRoutine($id)
	{
		return $this->db->where('category_routine_id',$id)
						->get('routine');
	}
	public function getResident()
	{
		return $this->db->from('resident')->get();
	}
	public function masterstaff()
	{
		return $this->db->from('staff')->get();
	}
	public function masterresident()
	{
		return $this->db->from('resident')->get();
	}
	public function getClass()
	{
		return $this->db->from('class')->get();
	}
	public function getDay()
	{
		return $this->db->from('day')->get();
	}
	
	public function getClassStaff()
	{
		$this->db->select('class_staff.id,class.class_name, staff.id as staffid, staff.name as staffname,
		day.day_name,clock');
		$this->db->from('class_staff');
		$this->db->join('class', 'class.class_id=class_staff.id_class');
		$this->db->join('day', 'day.id = class_staff.day');
		$this->db->join('staff', 'staff.id = class_staff.id_staff');
		$this->db->order_by('day.id');
		return $this->db->get();
	}
	public function getClassStaffToday($dy,$id)
	{
		$this->db->select('class_staff.id,class.class_name, staff.id as staffid, staff.name as staffname,
		day.day_name,clock');
		$this->db->from('class_staff');
		$this->db->join('class', 'class.class_id=class_staff.id_class');
		$this->db->join('day', 'day.id = class_staff.day');
		$this->db->join('staff', 'staff.id = class_staff.id_staff');
		$this->db->where('day.id',$dy);
		$this->db->where('class_staff.id_staff',$id);
		$this->db->order_by('day.id');
		return $this->db->get();
	}
	public function checkPBISDate($date,$routine_id,$resident)
	{
		return $this->db->where('routine_id',$routine_id)
						->where('resident_id',$resident)
						->where('date',$date)
						->get('routine_record');
	}
	public function getClassResidentToday($dy,$id)
	{
		$this->db->select('class_staff.id,class.class_name, staff.id as staffid, staff.name as staffname,
		day.day_name,clock');
		$this->db->from('class_resident');
		$this->db->join('resident', 'class_resident.id_resident=resident.resident_id');
		$this->db->join('class_staff', 'class_staff.id = class_resident.id_class_staff');

		$this->db->join('class', 'class.class_id = class_staff.id_class');
		$this->db->join('staff', 'staff.id = class_staff.id_staff');
		$this->db->join('day', 'day.id = class_staff.day');
		$this->db->where('day.id',$dy);
		$this->db->where('resident.resident_id',$id);
		$this->db->order_by('day.id');
		return $this->db->get();
	}
	public function getClassStaffByUser($id)
	{
		$this->db->select('class_staff.id,class.class_name, staff.id as staffid, staff.name as staffname,
		day.day_name,clock');
		$this->db->from('class_staff');
		$this->db->join('class', 'class.class_id=class_staff.id_class');
		$this->db->join('day', 'day.id = class_staff.id_staff');
		$this->db->join('staff', 'staff.id = class_staff.id_staff');
		$this->db->where('class_staff.id_staff',$id);
		$this->db->order_by('day.id');
		return $this->db->get();
	}
	
	public function checkClassStaff($classid)
	{
		return $this->db->where('id_class',$classid)
						->get('class_staff');
	}
	public function getClassStaffSchedule($id)
	{
		return $this->db->where('id_class_staff',$id)
						->get('schedule_class_staff');
	}
	
	public function getDataAttendance($id)
	{
		$this->db->select('attendance_resident.id_class_staff,attendance_resident.id_resident, attendance_resident.id_scs,attendance_resident.date,`attendance_resident.time,attendance_resident.status,resident.name');
		$this->db->from('attendance_resident');
		$this->db->join('resident', 'resident.resident_id = attendance_resident.id_resident');
		$this->db->where('attendance_resident.id_scs',$id);
		return $this->db->get();
	}
	public function getRoutineInput($catid)
	{
		return $this->db->where('category_routine_id',$catid)
						->get('routine');
	}

	public function getRoutineRecord()
	{
// 		select resident.resident_id, resident.name,category_routine.name as categoryname,`routine`.routine_name,routine_record.day,routine_record.date,
// routine_record.description,routine_record.star,routine_record.staff_id

		$this->db->select('resident.resident_id, resident.name,category_routine.name as categoryname,`routine`.routine_name,routine_record.day,routine_record.date,routine_record.description,routine_record.star,routine_record.staff_id');
		$this->db->from('routine_record');
		$this->db->join('resident', 'resident.resident_id = routine_record.resident_id');
		$this->db->join('routine', 'routine.id = routine_record.routine_id');
		$this->db->join('category_routine', 'category_routine.id = routine.category_routine_id');
		return $this->db->get();
	}
	public function checkAttentanceExist($date,$userid,$idclasstaff,$idscs)
	{
		return $this->db->where('id_resident',$userid)
						->where('id_class_staff',$idclasstaff)
						->where('id_scs',$idscs)
						->like('date', $date)
				 		->limit(1)
				 		->get('attendance_resident');
	}
	public function getAttendanceSetting()
	{
		return $this->db->limit(1)
				 		->get('attendances_setting');
	}
	public function getRes($userid){
		return $this->db->where('resident_id',$userid)
				 		->limit(1)
				 		->get('resident');
	}
	
	public function AttendanceType($id)
	{
		return $this->db->where('id',$id)
				 		->limit(1)
				 		->get('attendances_type');
	}
	//resident
	public function profilResident($residentid)
	{
		return $this->db->where('resident_id',$residentid)
				 		->limit(1)
				 		->get('resident');
	}
	public function profilStaff($staffid)
	{
		return $this->db->where('id',$staffid)
				 		->limit(1)
				 		->get('staff');
	}
	
	public function recordPBIS()
	{
		$this->db->select('day_name,date');
		$this->db->from('routine_record');
		$this->db->join('day', 'day.id = routine_record.day');
		$this->db->group_by('date');
		$this->db->order_by('date','desc');
		return $this->db->get();
	}
	public function detailrecordPBIS($date)
	{
		$this->db->select('day_name,date,resident.resident_id,resident.name');
		$this->db->from('routine_record');
		$this->db->join('day', 'day.id = routine_record.day');
		$this->db->join('resident', 'resident.resident_id = routine_record.resident_id');
		$this->db->where('date',$date);
		$this->db->group_by('routine_record.resident_id');
		return $this->db->get();
	}
	public function detailrecordPBISperResident($date,$resident)
	{
		$this->db->select('routine_record.resident_id,resident.name,routine_record.date,routine_record.description,routine_record.star,staff.name as staffname,routine_name,default_star');
		$this->db->from('routine_record');
		$this->db->join('day', 'day.id = routine_record.day');
		$this->db->join('resident', 'resident.resident_id = routine_record.resident_id');
		$this->db->join('routine', 'routine.id = routine_record.routine_id');
		$this->db->join('staff', 'staff.id = routine_record.staff_id');
		$this->db->where('date',$date);
		$this->db->where('routine_record.resident_id',$resident);
		$this->db->order_by('routine_record.routine_id','asc');
		return $this->db->get();
	}
	public function PBISResident($date,$resident)
	{
		return $this->db->where('date',$date)
						->where('resident_id',$resident)
				 		->limit(1)
				 		->get('routine_record');
	}
	
}
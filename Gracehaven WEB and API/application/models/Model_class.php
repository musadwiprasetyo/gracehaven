<?php
class Model_class extends CI_Model
{
	var $table = 'class';
	var $column_order = array(null,'class_id','class_name');  
	// column search
	var $column_search = array('class_name'); //set column field database for datatable searchable 
	// default sort by
	var $order = array('class_id' => 'asc','class_name'=>'asc'); 

	public function __construct()
	{
		parent::__construct();
	}
    private function _get_datatables_query()
	{
		$this->db->from($this->table);
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
		// fungsi pencarian
		$i = 0;
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->_get_datatables_query();
		return $this->db->count_all_results();
	}
	public function insertUpdateSubmit() {

		$classid 	= $this->input->post('classid');
    	if ($classid=='') {
    		$newdata = Array (			
					'class_name'	 		    => $this->input->post('name')
			);
			$this->db->insert($this->table, $newdata);
    	} else {
    		$newdata = Array (			
					'class_name'	 		    => $this->input->post('name')
			);
			$this->db->where('class_id', $classid);
			$this->db->update($this->table, $newdata);
    	}
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return FALSE; 
		}
	}
	
	function getDataJson($id)
	{		
		return $this->db->where('class_id',$id)
						->get($this->table);
		
	}
	function safe_encode($string) {
	
        $data = str_replace(array('/'),array('_'),$string);
        return $data;
    }
 
	function safe_decode($string,$mode=null) {
		
		$data = str_replace(array('_'),array('/'),$string);
        return $data;
    }
	
	function getdata($id)
	{		
		return $this->db->where('id',$id)
						->get($this->table);
		
	}
	public function cekExist($id)
	{
	    $this->db->select("*");
		$this->db->where("class_staff.id_class",$id);
		$this->db->from("class_staff");
		$query = $this->db->get();
		return $query->num_rows();
	}
	public function updateStatusData($id,$value)
	{
		$nilaistatus = Array (
			'rak_status' => $value
		);
		$this->db->where('id', $id)
				 ->update($this->table, $nilaistatus);
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return TRUE; 
		}
	}
	public function deleteData($id)
	{
	    $this->db->where('class_id', $id);
		$this->db->delete($this->table);
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return TRUE; 
		}
	}
	
	
}
?>
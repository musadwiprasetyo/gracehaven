<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_auth extends CI_Model {
    // cek status admin, login atau tidak?
    public function cekAuth()
    {		
        $username = $this->input->post('username');
        $password = $this->input->post('password');
  
		$query = $this->db->where('username', $username)
						  ->where_in('type', array('1','2'))
						  ->limit(1)
						  ->get('users');
			if ($query->num_rows() == 1)
			{
				if($query->row('password')==$password)
				{

					$isstaff = $this->db->where('id', $query->row('user_id'))
						  				->limit(1)
						  				->get('staff')->row();

					$data = array(
								'name'			=> $query->row('name'),
								'type'			=> $query->row('type'),
								'username' 		=> $username,
								'isstaff'		=> $isstaff->staff,
								'user_id' 		=> $query->row('user_id'),
								'login' 		=> TRUE,
							);
					$this->session->set_userdata($data);
					return TRUE;
				} else {
					return FALSE;
				}
			} else {
				return FALSE;
			}
            
    }

    public function logout()
    {
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
        $this->session->sess_destroy();
    }
	
	public function logger($success=0)
	{
		$user = array (
					'username' 		=> $this->input->post('username'),
					'ip_address' 	=> $this->input->ip_address(),
					'time_stamp' 	=> date("Y-m-d H:i:s"),
					'success' 		=> $success
		);
		$this->db->insert('login_attempts', $user);
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return FALSE; 
		}
	}
}

<?php
class Model_group extends CI_Model
{
	var $table = 'group';
	var $column_order = array(null, 'group_name');  
	var $column_search = array('group_name'); 
	var $order = array('group_id' => 'asc','group_name'=>'asc'); 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
    private function _get_datatables_query()
	{
		$this->db->from($this->table);
		
		$i = 0;
		foreach ($this->column_search as $item) 
		{
			if($_POST['search']['value'])
			{
				if($i===0) 
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);

				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}
				if(count($this->column_search) - 1 == $i) 
					$this->db->group_end();
			}
			$i++;
		}
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		// $this->_get_datatables_query();
		return $this->db->count_all_results();
	}
	public function insert() {
		$newrole = Array (			
			'group_name'	 		    => $this->input->post('name')
		);
		$this->db->insert('group', $newrole);
		if ($this->db->affected_rows() > 0) {
			$id = $this->db->insert_id();
			$module = $this->input->post('permission');
			foreach ($module as $key => $value) {
				$data = array(
				        'grup_id' 		=> $id,
				        'permission_id' => $key
				);
				$this->db->insert('role_permissions', $data);
			}
			return TRUE;
		} else {
			return FALSE; 
		}
	}
	public function processupdate($id)
	{	
		$role = Array (			
			'group_name'	 		    => $this->input->post('name')
		);
		$this->db->where('group_id', $id);
		$this->db->update('group', $role);
		//delete role permissions
		$this->db->where('grup_id',$id);
		$this->db->delete('role_permissions');
		if ($this->db->affected_rows() > 0) {
			$module = $this->input->post('permission');
			//add role permissions
			foreach ($module as $key => $value) {
				$data = array(
				        'grup_id' 		=> $id,
				        'permission_id' => $key
				);
				$this->db->insert('role_permissions', $data);
			}
			return TRUE;
		} else {
			return FALSE; 
		}

		

	}
	function safe_encode($string) {
	
        $data = str_replace(array('/'),array('_'),$string);
        return $data;
    }
 
	function safe_decode($string,$mode=null) {
		
		$data = str_replace(array('_'),array('/'),$string);
        return $data;
    }
	
	function get_role($id)
	{		
		return $this->db->where('group_id',$id)
						->get($this->table);
		
	}
	
	var $column_order_permissions = array(null, 'permissions.name','permissions.key',null,'permissions.group','permissions.statuspermission');
	var $column_search_permisions = array('permissions.name','permissions.key','permissions.group'); 
	var $order_permissions = array('permissions.order' => 'asc','permissions.name'=>'asc'); 

	public function _get_datatables_query_role_permissions($id)
	{
		
		    $this->db->select("permissions.id,permissions.key, permissions.name, permissions.description, permissions.group, permissions.statuspermission");
			$this->db->join('permissions','permissions.id = role_permissions.permission_id','INNER');
			$this->db->join('group','group.group_id = role_permissions.grup_id','INNER');
			$this->db->where("group.group_id",$id);
			$this->db->order_by("permissions.order",'asc');
			$this->db->from("role_permissions");
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order_permissions[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order_permissions))
		{
			$order = $this->order_permissions;
			$this->db->order_by(key($order), $order[key($order)]);
		}
		// fungsi pencarian
		$i = 0;
		foreach ($this->column_search_permisions as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_permisions) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
	}

	function get_datatables_role_permissions($id)
	{
		$this->_get_datatables_query_role_permissions($id);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered_role_permissions($id)
	{
		$this->_get_datatables_query_role_permissions($id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all_role_permissions($id)
	{
		$this->_get_datatables_query_role_permissions($id);
		return $this->db->count_all_results();
	}


	var $column_order_users = array(null, 'staff.name');
	var $column_search_users = array('staff.name'); 
	var $order_users = array('staff.id' => 'asc','staff.name'=>'asc'); 
	public function _get_datatables_query_role_user($id)
	{
		
		    $this->db->select("staff.id,staff.name");
			$this->db->join('group','group.group_id = staff.group_id','INNER');
			$this->db->where("group.group_id",$id);
			$this->db->from("staff");
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order_users[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order_users))
		{
			$order = $this->order_users;
			$this->db->order_by(key($order), $order[key($order)]);
		}
		// fungsi pencarian
		$i = 0;
		foreach ($this->column_search_users as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_users) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
	}

	function get_datatables_role_user($id)
	{
		$this->_get_datatables_query_role_user($id);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered_role_user($id)
	{
		$this->_get_datatables_query_role_user($id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all_role_user($id)
	{
		$this->_get_datatables_query_role_user($id);
		return $this->db->count_all_results();
	}

	public function getModule()
	{
		$this->db->order_by('group ASC, id ASC');
		$this->db->from('permissions');
		$query = $this->db->get();
		return $query->result();
	}
	public function updateStatusRole($id,$value)
	{
		$nilaistatus = Array (
			'status' => $value
		);
		$this->db->where('id', $id)
				 ->update('roles', $nilaistatus);
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return TRUE; 
		}
	}
	public function cekRole($id)
	{
	    $this->db->select("group.group_id,group.group_name");
		$this->db->join('staff','staff.group_id = group.group_id','INNER');
		$this->db->where("group.group_id",$id);
		$this->db->from("group");
		$query = $this->db->get();
		return $query->num_rows();
	}
	public function cekexistrole($id,$permissionsid)
	{
		$this->db->where("role_permissions.grup_id",$id);
		$this->db->where("role_permissions.permission_id",$permissionsid);
		$this->db->from('role_permissions');	
		$query = $this->db->get();
		return $query->row();
	}
	public function deleteRole($id)
	{
	    $this->db->where('group_id', $id);
		$this->db->delete('group');
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return TRUE; 
		}
	}
	public function getroleactive()
	{		
		$this->db->where_in('group_id', array('1','2'));
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->result();
		
	}
	public function getrole()
	{		
		$this->db->where_in('group_id', array('1','2'));
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->result();
		
	}
	
}
?>
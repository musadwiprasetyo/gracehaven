<?php
class Model_resident extends CI_Model
{
	var $table = 'resident';
	var $column_order = array(null, 'resident.name','users.username','group.group_name'); 
	var $column_search = array('resident.name','users.username','group.group_name');   //
	var $order = array('resident.id' => 'asc','resident.name'=>'asc'); 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
    private function _get_datatables_query()
	{
		$this->db->select('resident.id,resident.resident_id,resident.name,users.username,group.group_name');
		$this->db->join('users','users.user_id = resident.resident_id','INNER');
		$this->db->join('group','group.group_id = users.type','INNER');
		$this->db->where_in('users.type', array('3'));
		$this->db->from($this->table);
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
		// fungsi pencarian
		$i = 0;
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->_get_datatables_query();
		return $this->db->count_all_results();
	}
	public function insert() {
		$newdata = Array (			
				'resident_id'	 	=> $this->input->post('residentid'),
				'name'	 		    => $this->input->post('name'),
		);
		
		$this->db->insert('resident', $newdata);
		if ($this->db->affected_rows() > 0) {
			$id = $this->db->insert_id();
			$datauser = Array (		
				'user_id'	 		=> $this->input->post('residentid'),	
				'name'	 		    => $this->input->post('name'),
				'username'	 		=> $this->input->post('username'),
				'password' 			=> $this->input->post('password'),
				'type' 				=> 3
			);
			$this->db->insert('users', $datauser);
			return TRUE;
		} else {
			return FALSE; 
		}
	}
	public function processupdate($id)
	{	
		$password=$this->input->post('password');
		if ($password != null) {
			$newdata = Array (		
				'resident_id'	 	=> $this->input->post('residentid'),	
				'name'	 		    => $this->input->post('name'),
				
			);
		    $datauser = Array (			
				'name'	 		    => $this->input->post('name'),
				'username'	 		=> $this->input->post('username'),
				'password' 			=> $this->input->post('password'),
				'type' 				=> 3
			);
		} else {
			$newdata = Array (	
				'resident_id'	 	=> $this->input->post('residentid'),		
				'name'	 		    => $this->input->post('name'),
			);
		    $datauser = Array (			
				'name'	 		    => $this->input->post('name'),
				'username'	 		=> $this->input->post('username'),
				'type' 				=> 3
			);
		}
			   $this->db->where('id', $id);
		$resident=$this->db->update('resident', $newdata);
		if ($resident) {
			$this->db->where('user_id',$this->input->post('residentid'));
			$this->db->where('type',3);
			$this->db->update('users', $datauser);
			return TRUE;
		}else{
			return FALSE; 
		}
	}
	function safe_encode($string) {
	
        $data = str_replace(array('/'),array('_'),$string);
        return $data;
    }
 
	function safe_decode($string,$mode=null) {
		
		$data = str_replace(array('_'),array('/'),$string);
        return $data;
    }
	
	function get_resident($id)
	{		
		$this->db->select('resident.id,resident.resident_id,resident.name,users.username');
		$this->db->join('users','users.user_id = resident.resident_id','INNER');
		$this->db->where('resident.id',$id);
		$this->db->where('type',3);
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query;		
	}
	public function deleteResident($id)
	{
	    $this->db->where('id', $id);
		$this->db->delete('resident');
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return TRUE; 
		}
	}
	
}
?>
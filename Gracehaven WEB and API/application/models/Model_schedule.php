<?php
class Model_schedule extends CI_Model
{
	var $table = 'class_staff';
	var $column_order = array(null,'class_staff.id','class.class_name','staff.name');  
	// column search
	var $column_search = array('class.class_name','staff.name','day.day_name'); //set column field database for datatable searchable 
	// default sort by
	var $order = array('class_staff.id' => 'asc','class.class_name'=>'asc'); 

	public function __construct()
	{
		parent::__construct();
	}
    private function _get_datatables_query()
	{

		$this->db->select('class_staff.id,class.class_name,staff.name,day.day_name,class_staff.clock,class_staff.id_staff,class_staff.status');
		$this->db->join('staff','staff.id = class_staff.id_staff','INNER');
		$this->db->join('class','class.class_id = class_staff.id_class','INNER');
		$this->db->join('day','day.id = class_staff.day','INNER');
		$this->db->where('staff.id',$this->session->userdata('user_id'));
		$this->db->from($this->table);
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
		// fungsi pencarian
		$i = 0;
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->_get_datatables_query();
		return $this->db->count_all_results();
	}
	public function insertUpdateSubmit() {

		$classstaffid 	= $this->input->post('classstaffid');
    	if ($classstaffid=='') {
    		$newdata = Array (			
					'id_class'	 		    => $this->input->post('programme'),
					'id_staff'	 		    => $this->input->post('staff'),
					'day'	 		    	=> $this->input->post('day'),
					'clock'	 		    	=> $this->input->post('clock'),
					'status'	 		    => 1,
			);
			$this->db->insert($this->table, $newdata);
    	} else {
    		$newdata = Array (			
					'id_class'	 		    => $this->input->post('programme'),
					'id_staff'	 		    => $this->input->post('staff'),
					'day'	 		    	=> $this->input->post('day'),
					'clock'	 		    	=> $this->input->post('clock'),
					'status'	 		    => 1,
			);
			$this->db->where('id', $classstaffid);
			$this->db->update($this->table, $newdata);
    	}
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return FALSE; 
		}
	}
	
	function getDataJson($id)
	{		
		return $this->db->where('id',$id)
						->get($this->table);
		
	}
	function getDataJsonSchedule($id)
	{		
		return $this->db->where('id',$id)
						->get('schedule_class_staff');
		
	}
	function safe_encode($string) {
	
        $data = str_replace(array('/'),array('_'),$string);
        return $data;
    }
 
	function safe_decode($string,$mode=null) {
		
		$data = str_replace(array('_'),array('/'),$string);
        return $data;
    }
	
	function getdata($id)
	{		
		return $this->db->where('id',$id)
						->get($this->table);
		
	}
	public function getClass()
	{
		return $this->db->get('class');
	}
	public function getStaff()
	{
		return $this->db->get('staff');
	}
	public function getDay()
	{
		return $this->db->get('day');
	}
	public function countResident($id)
	{
		return $this->db->where('id_class_staff',$id)
						->get('class_resident');
	}
	public function countResidentPresence($id_scs)
	{
		return $this->db->where('id_scs',$id_scs)
						->get('attendance_resident');
	}
	public function cekExist($id)
	{
	    $this->db->select("*");
		$this->db->join('class_resident','class_resident.id_class_staff = class_staff.id','INNER');
		$this->db->where("class_staff.id",$id);
		$this->db->from("class_staff");
		$query = $this->db->get();
		return $query->num_rows();
	}
	public function updateStatusData($id,$value)
	{
		$nilaistatus = Array (
			'rak_status' => $value
		);
		$this->db->where('id', $id)
				 ->update($this->table, $nilaistatus);
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return TRUE; 
		}
	}
	public function deleteData($id)
	{
	    $this->db->where('id', $id);
		$this->db->delete($this->table);
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return TRUE; 
		}
	}

	public function getdataclassstaff($id)
	{
		$this->db->select("class_staff.id as classstaffid,class.class_name,class.class_id,staff.id,staff.name");
		$this->db->join('class','class.class_id = class_staff.id_class','INNER');
		$this->db->join('staff','staff.id = class_staff.id_staff','INNER');
		$this->db->where("class_staff.id",$id);
		$this->db->limit(1);
		$this->db->from("class_staff");
		$query = $this->db->get();
		return $query->row();
	}
	var $tableschedule = 'schedule_class_staff';
	var $column_order_schedule = array(null,'schedule_class_staff.id','schedule_class_staff.schedule','schedule_class_staff.date','schedule_class_staff.note','schedule_class_staff.status');  
	// column search
	var $column_search_schedule = array('schedule_class_staff.schedule','schedule_class_staff.date'); //set column field database for datatable searchable 
	// default sort by
	var $order_schedule = array('schedule_class_staff.id' => 'asc','schedule_class_staff.schedule'=>'asc'); 

	
    private function _get_datatables_query_schedule($id)
	{

		$this->db->select('schedule_class_staff.id,schedule,schedule_class_staff.date,schedule_class_staff.note,schedule_class_staff.status,schedule_class_staff.id_class_staff');
		$this->db->where('id_class_staff',$id);
		$this->db->from($this->tableschedule);
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order_schedule))
		{
			$order = $this->order_schedule;
			$this->db->order_by(key($order), $order[key($order)]);
		}
		// fungsi pencarian
		$i = 0;
		foreach ($this->column_search_schedule as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_schedule) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
	}

	function get_datatables_schedule($id)
	{
		$this->_get_datatables_query_schedule($id);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered_schedule($id)
	{
		$this->_get_datatables_query_schedule($id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all_schedule($id)
	{
		$this->_get_datatables_query_schedule($id);
		return $this->db->count_all_results();
	}
	public function Attendance($id)
	{
		return $this->db->where('id',$id)
						->get('schedule_class_staff');
	}

	public function ResidentAttendance($id,$id_scs)
	{
		
		return $this->db->query("SELECT a.id_resident,b.`status`,r.name FROM class_resident a
			JOIN resident r ON r.resident_id=a.id_resident  
			left join attendance_resident b
			ON a.id_resident=b.id_resident
			AND b.id_scs='$id_scs'
			AND b.id_class_staff='$id'
			WHERE a.id_class_staff='$id'
			group BY a.id_resident")->result();
	}
	public function AttendanceResident($id_scs)
	{
		$this->db->select("resident.resident_id,resident.name,attendance_resident.date,attendance_resident.time,attendance_resident.status,attendance_resident.id_scs,attendance_resident.id_class_staff");
		$this->db->join('resident','resident.resident_id = attendance_resident.id_resident','INNER');
		$this->db->where("id_scs",$id_scs);
		$this->db->from("attendance_resident");
		$query = $this->db->get();
		return $query;
	}

	public function insertUpdateSubmitSchedule() {

		$scheduleid 	= $this->input->post('scheduleid');
    	if ($scheduleid=='') {
    		$newdata = Array (			
					'id_class_staff'	 	=> $this->input->post('classstaffid'),
					'schedule'	 		    => $this->input->post('schedule'),
					'date'	 		    	=> $this->input->post('date'),
					'note'	 		   		=> $this->input->post('note'),
					'status'	 		    => 1,
			);
			$this->db->insert('schedule_class_staff', $newdata);
    	} else {
    		$newdata = Array (			
					'id_class_staff'	 	=> $this->input->post('classstaffid'),
					'schedule'	 		    => $this->input->post('schedule'),
					'date'	 		    	=> $this->input->post('date'),
					'note'	 		    	=> $this->input->post('note'),
					'status'	 		    => 1,
			);
			$this->db->where('id', $scheduleid);
			$this->db->update('schedule_class_staff', $newdata);
    	}
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return FALSE; 
		}
	}
	public function cekExistAttendance($id)
	{
	    $this->db->select("*");
		$this->db->join('schedule_class_staff','schedule_class_staff.id = attendance_resident.id_scs','INNER');
		$this->db->where("schedule_class_staff.id",$id);
		$this->db->from("attendance_resident");
		$query = $this->db->get();
		return $query->num_rows();
	}
	public function deleteDataSchedule($id)
	{
	    $this->db->where('id', $id);
		$this->db->delete('schedule_class_staff');
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return TRUE; 
		}
	}
	public function deleteResident($id,$classstaff)
	{
		
		$this->db->where('id',$id);
	    $this->db->where('id_class_staff',$classstaff);
		$this->db->delete('class_resident');
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return TRUE; 
		}
	}
	public function InsertResident($id,$classstaff)
	{
		
		
		$newdata = Array (			
					'id_class_staff'	 	=> $classstaff,
					'id_resident'	 		=> $id
		);
		$this->db->insert('class_resident', $newdata);
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return TRUE; 
		}
	}
	
	public function DetailResident($id)
	{
		
		return $this->db->query("SELECT * FROM resident r LEFT JOIN class_resident cr 
		ON cr.id_resident=r.resident_id
		AND cr.id_class_staff='$id'")->result();
	}
}
?>